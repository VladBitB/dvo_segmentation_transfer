/*
 * ply_module.cpp
 *
 *  Created on: 	04.06.2014
 *  Last modified on: 	27.01.2015
 *      Author: Golyanik
 */

#include "ply_module.hpp"

vector<double> array_x, array_y, array_z;

//in order to save color
vector<uint> array_red, array_green, array_blue, array_alpha;

static int callback_x(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_x.push_back(ply_get_argument_value(argument));
  return 1;
}

static int callback_y(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_y.push_back(ply_get_argument_value(argument));
  return 1;
}

static int callback_z(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_z.push_back(ply_get_argument_value(argument));
  return 1;
}


static int callback_red(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_red.push_back(ply_get_argument_value(argument));
  return 1;
}

static int callback_green(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_green.push_back(ply_get_argument_value(argument));
  return 1;
}

static int callback_blue(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_blue.push_back(ply_get_argument_value(argument));
  return 1;
}

static int callback_alpha(p_ply_argument argument){

  ply_get_argument_user_data(argument, NULL, NULL);
  array_alpha.push_back(ply_get_argument_value(argument));
  return 1;
}


/* read_ply_file
 * Description: reads a .ply file and returns the point cloud
 *
 * Last modified on 27.01.2015
 */
int read_ply_file(/*input*/
		  string f_name, 				//name of a .ply file
		  int free_vector,				//free internal memory after the last call
		  /*output*/
		  Eigen::MatrixXd &Mat, 				//point cloud, num of vertices x 3
		  Eigen::MatrixXi *Colors				//colors
		  ){

	p_ply ply 	= ply_open(f_name.c_str(), NULL, 0, NULL);

	if ((!ply) || (!ply_read_header(ply)) ){
			cout << "-> ply_module error: failed to open the file " << f_name << endl;
			exit(EXIT_FAILURE);

	}
	else
			cout << "-> reading " << f_name << "..." << endl;
	
	int p_red = 0, p_green = 0, p_blue = 0, p_alpha = 0;
	
	if (Colors != nullptr){
	  p_ply_element element = NULL;
	  element = ply_get_next_element(ply, element);
	  p_ply_property property = NULL;
	
	  while ((property = ply_get_next_property(element, property))){
            const char *property_name;
            e_ply_type type, length_type, value_type;
            ply_get_property_info(property, &property_name, &type, 
                    &length_type, &value_type);
	    string p_str = property_name;
	    
	    //check if the properties exist
	    if (p_str == "red")
	      p_red = 1;
	    else if (p_str == "green")
	      p_green = 1;
	    else if (p_str == "blue")
	      p_blue = 1;
	    else if (p_str == "alpha")
	      p_alpha = 1;
	  }
	}
	

	
	int num_elem = 	ply_set_read_cb(ply, "vertex", "x", callback_x, NULL, 0);
			ply_set_read_cb(ply, "vertex", "y", callback_y, NULL, 0);
			ply_set_read_cb(ply, "vertex", "z", callback_z, NULL, 0);
		
			
	if ((Colors != nullptr) && p_red && p_green && p_blue && !p_alpha)
	  *Colors = Eigen::MatrixXi::Zero(num_elem, 3);
	else if ((Colors != nullptr) && p_red && p_green && p_blue && p_alpha)
	  *Colors = Eigen::MatrixXi::Zero(num_elem, 4);
		
	
	//read color settings and save them
	if (Colors != nullptr){
	  if (p_red && p_green && p_blue && ((*Colors).rows() > 0)){
	    ply_set_read_cb(ply, "vertex", "red", callback_red, NULL, 0);
	    ply_set_read_cb(ply, "vertex", "green", callback_green, NULL, 0);
	    ply_set_read_cb(ply, "vertex", "blue", callback_blue, NULL, 0);
	    if ((*Colors).cols() == 4)
	      ply_set_read_cb(ply, "vertex", "alpha", callback_alpha, NULL, 0);
	  }
	}
	
	
	if (!ply_read(ply)){
		cout << "-> ply_module error: failed to read the file " << f_name << endl;
		exit(EXIT_FAILURE);
	}
	
	ply_close(ply);

	//save the data in the output matrix
	Mat = Eigen::MatrixXd(num_elem, 3);

	for (int i = 0; i < num_elem; i++){
	  Mat(i, 0) = array_x[i];
	  Mat(i, 1) = array_y[i];
	  Mat(i, 2) = array_z[i];
	}

	if (Colors != nullptr){
	  if (p_red && p_green && p_blue && ((*Colors).cols() == 3)){
	    //*Colors = MatrixXi(num_elem, 3);
	    for (int i = 0; i < num_elem; i ++){
	      (*Colors)(i, 0) = array_red[i];
	      (*Colors)(i, 1) = array_green[i];
	      (*Colors)(i, 2) = array_blue[i];
	    }
	  }
	  
	  else if (p_red && p_green && p_blue && p_alpha && ((*Colors).cols() == 4)){
	    //*Colors = MatrixXi(num_elem, 4);
	    for (int i = 0; i < num_elem; i ++){
	      (*Colors)(i, 0) = array_red[i];
	      (*Colors)(i, 1) = array_green[i];
	      (*Colors)(i, 2) = array_blue[i];
	      (*Colors)(i, 3) = array_alpha[i];
	    }
	  }
	  
	}
	
	//clean the associated point vectors
	array_x.resize(0);
	array_y.resize(0);
	array_z.resize(0);

	array_red.resize(0);
	array_green.resize(0);
	array_blue.resize(0);
	array_alpha.resize(0);
	
	//free memory allocated in associated vectors
	if (free_vector){
	  array_x.shrink_to_fit();
	  array_y.shrink_to_fit();
	  array_z.shrink_to_fit();
	  array_red.shrink_to_fit();
	  array_green.shrink_to_fit();
	  array_blue.shrink_to_fit();
	  array_alpha.shrink_to_fit();
	}
	

	return 0;

}



/* write_ply_file
 * Description: writes to a .ply file from a Mat(<number of points>, 3)
 *
 * Last modified on 27.01.2015
 */
int write_ply_file(/*input*/
					string f_name,				//name of an output .ply file
					Eigen::MatrixXd &Mat,				//point cloud data to save in a .ply file
					Eigen::MatrixXi *Colors,			//point colors of the point cloud
					ply_color color,
					int color_flag				//0 - no_color
										//1 - transfer colors (Colors matrix  is used)
										//2 - use the same color for all points
		  ){
	
       if (Colors == nullptr){
	 if ( (color_flag == 1) ){
	   cout << "-> ply_module error: no colors to transfer " << endl;
	   exit(EXIT_FAILURE);
	  }
	  else if ( (color_flag == 1) && ((*Colors).rows() == 0) ){
	    cout << "-> ply_module error: no colors to transfer " << endl;
	    exit(EXIT_FAILURE);
	  } 
        }
  
	p_ply oply = ply_create(f_name.c_str(), PLY_ASCII, nullptr, 0, nullptr);

	if (!oply){
	    cout << "-> ply_module error: failed to create an output file " << f_name << endl;

	}

	int ninstances = Mat.rows();
	ply_add_element(oply, "vertex", ninstances);

	//add properties; the function ignores two last parameters
	ply_add_property(oply, "x", PLY_DOUBLE, PLY_DOUBLE, PLY_DOUBLE);
	ply_add_property(oply, "y", PLY_DOUBLE, PLY_DOUBLE, PLY_DOUBLE);
	ply_add_property(oply, "z", PLY_DOUBLE, PLY_DOUBLE, PLY_DOUBLE);
	
	if (color_flag != 0){
	  ply_add_property(oply, "red", PLY_UCHAR, PLY_UCHAR, PLY_UCHAR);
	  ply_add_property(oply, "green", PLY_UCHAR, PLY_UCHAR, PLY_UCHAR);
	  ply_add_property(oply, "blue", PLY_UCHAR, PLY_UCHAR, PLY_UCHAR);
	  if (Colors != nullptr)
	    if ( (*Colors).cols() == 4)
	      ply_add_property(oply, "alpha", PLY_UCHAR, PLY_UCHAR, PLY_UCHAR);
	}
	
	//compose a comment string
	time_t t = time(nullptr);
	struct tm *aTime = localtime(&t);
	string ply_comment = ":: the file created by the coherent_point_drift.cpp, " + (string)asctime(aTime);
	//erase the last newline character
	ply_comment.pop_back();

	ply_add_comment(oply, 	ply_comment.c_str());
	ply_add_obj_info(oply, 	"content: two point clouds registered by the coherent point drift algorithm");

	//write header
	ply_write_header(oply);

	//write the values
	for (int i = 0; i < ninstances; i++){
	  
	  for (int j = 0; j < Mat.cols(); j++)
	    ply_write(oply, (double)Mat(i, j));
  
	  if (color_flag == 1){
	    ply_write(oply, (*Colors)(i, 0));
	    ply_write(oply, (*Colors)(i, 1));
	    ply_write(oply, (*Colors)(i, 2));
	    if ( (*Colors).cols() == 4)
	      ply_write(oply, (*Colors)(i, 3));
	    
	  }
	  else if (color_flag == 2){
	    ply_write(oply, color.red);
	    ply_write(oply, color.green);
	    ply_write(oply, color.blue);
	  }
	}

	ply_close(oply);

	return 0;

}