
#ifndef _DVOERRORPHOTOMETRIC_H
#define _DVOERRORPHOTOMETRIC_H

#define INVALID_RESIDUAL    0.0

// ### Brightness constancy

class DvoErrorPhotometric
{
public:
	DvoErrorPhotometric(const int w, const int h,
						const float* grayRef, const float* depthRef,
						const float* grayCur, const float* depthCur,
						const double* intrinics, const int x, const int y) :
		w_(w),
		h_(h),
		depthRef_(depthRef),
        depthCurrent_(depthCur),
        grayRef_(grayRef),
        grayCur_(grayCur),
		intrinics_(intrinics),
        x_(x),
        y_(y)
    {
    }
    

    virtual ~DvoErrorPhotometric()
    {
    }
    

    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
		// rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);

        // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
			T x0 = (T(x_) - cx) / fx;
			T y0 = (T(y_) - cy) / fy;
            
            // transform reference 3d point into current frame
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
			// rotate
			T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translate
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
				T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
				T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                {
                    // lookup interpolated intensity (automatic differentiation)
                    typedef ceres::Grid2D<float, 1> ImageDataType;
					ImageDataType array(grayCur_, 0, h_, 0, w_);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute photometric residual
						T valRef = T(grayRef_[y_*w_ + x_]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
            }
        }

		// invalid lookup
		residuals[0] = T(INVALID_RESIDUAL);
        return true;
    }
    
private:
	const int w_;
	const int h_;
	const float* depthRef_;
	const float* depthCurrent_;
    const float* grayRef_;
	const float* grayCur_;
	const double* intrinics_;
	const int x_;
	const int y_;
};



#endif