#ifndef _LOAD_H
#define _LOAD_H




cv::Mat loadDepth(const std::string &filename)
{
	// read 16 bit depth image
	cv::Mat depth16 = cv::imread(filename, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	// convert depth to float (1.0f = 1.0m)
	cv::Mat depth;
	depth16.convertTo(depth, CV_32FC1, (1.0 / 5000.0));
        
        /*
        // ### set zero values to 10.0f meters
        for (uint i = 0; i < depth16.rows; i++)
            for (uint j = 0; j < depth16.cols; j++){
                if (depth.at<float>(i, j) < 0.01f) 
                    depth.at<float>(i, j) = 10.0f; 
            }
        */
        
        
        return depth;
}


void loadData(cv::Mat &grayRef, cv::Mat &grayCur, cv::Mat &depthRef, cv::Mat &depthCur, Eigen::Matrix3d &K)
{
        
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgb/1305031102.275326.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "depth/1305031102.262886.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgb/1305031102.175304.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "depth/1305031102.160407.png");
        
        /*
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgbd_data7/rgb/48.802185.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "rgbd_data7/depth/48.809997.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgbd_data7/rgb/48.834565.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "rgbd_data7/depth/48.841358.png");
        */
        
        /*
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgbd_data5/rgb/115.024210.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "rgbd_data5/depth/115.024209.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgbd_data5/rgb/115.155453.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "rgbd_data5/depth/115.155451.png");
        */
        
    // initialize intrinsic matrix
    K <<                517.3, 0.0, 318.6,
			0.0, 516.5, 255.3,
			0.0, 0.0, 1.0; 
}


#endif