#pragma once

#include "stdafx.h"

#define GLOG_NO_ABBREVIATED_SEVERITIES

#include "CeresSolver.h"

#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "glog/logging.h"

using ceres::DynamicAutoDiffCostFunction;
using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;
using namespace std;



struct CorrespondenceCostFunc
{
	CorrespondenceCostFunc(const ICPCorr& _corr) : corr(_corr) {}

	template <typename T>
	bool operator()(const T* const camera, T* residuals) const {

		// camera[0,1,2] are the angle-axis rotation
		// camera[3,4,5] are the translation

		T pA[3] = { T(corr.p0.x), T(corr.p0.y), T(corr.p0.z) };
		T pAWorld[3];
		ceres::AngleAxisRotatePoint(camera, pA, pAWorld);	//rotation
		pAWorld[0] += camera[3]; pAWorld[1] += camera[4]; pAWorld[2] += camera[5];	//translation

		T pB[3] = { T(corr.p1.x), T(corr.p1.y), T(corr.p1.z) };

		T diff[3] = { pAWorld[0] - pB[0], pAWorld[1] - pB[1], pAWorld[2] - pB[2] };
		T n[3] = { T(corr.n.x), T(corr.n.y), T(corr.n.z) };

		//T weight[1] = { T(1.0) };	//todo weight
		residuals[0] = ceres::DotProduct(diff, n);// *weight[0];
		return true;
	}

	static ceres::CostFunction* Create(const ICPCorr& corr, double camera[6])	{
		auto cFunc = new CorrespondenceCostFunc(corr);

		//double camera[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		double residuals[1] = { 0.0 };
		(*cFunc)(camera, residuals);
		if (residuals[0] != residuals[0]) {
			cout << "invalid residual: " << residuals[0] << endl;
		}

		return (new ceres::AutoDiffCostFunction<CorrespondenceCostFunc, 1, 6>(cFunc));
	}

	ICPCorr corr;
};


struct IMUConstraintCostFunc
{
	IMUConstraintCostFunc(const IMUConstraint& _corr) : corr(_corr) {}

	template <typename T>
	bool operator()(const T* const camera, T* residuals) const {

		// camera[0,1,2] are the angle-axis rotation
		// camera[3,4,5] are the translation

		T rot[3] = { T(corr.rotation.x), T(corr.rotation.y), T(corr.rotation.z) };
		T tra[3] = { T(corr.translation.x), T(corr.translation.y), T(corr.translation.z) };

		T weight = { T(corr.weight) };

		residuals[0] = (camera[0] - rot[0]) * weight;
		residuals[1] = (camera[1] - rot[1]) * weight;
		residuals[2] = (camera[2] - rot[2]) * weight;

		residuals[3] = (camera[3] - tra[0]) * weight;
		residuals[4] = (camera[4] - tra[1]) * weight;
		residuals[5] = (camera[5] - tra[2]) * weight;

		return true;
	}

	static ceres::CostFunction* Create(const IMUConstraintCostFunc& corr)	{
		auto cFunc = new IMUConstraintCostFunc(corr);

		double camera[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		double residuals[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		(*cFunc)(camera, residuals);
		if (residuals[0] != residuals[0]) {
			cout << "invalid residual: " << residuals[0] << endl;
		}

		return (new ceres::AutoDiffCostFunction<IMUConstraintCostFunc, 6, 6>(cFunc));
	}

	IMUConstraint corr;
};


mat4f CeresSolver::solve(const std::vector<ICPCorr>& correspondences, const IMUConstraint& imuConstraint, const mat4f& initTransform)
{
	bool verbose = false;

	std::vector<double> pose_double(6, 0.0);

	{
		//init transform
		mat4d transform = initTransform;

		vec3d angleaxis = vec3d(0.0, 0.0, 0.0);
		vec3d translation = transform.getTranslation();

		const mat3d rot = transform.getRotation().getTranspose(); //needs to be column major
		ceres::RotationMatrixToAngleAxis(rot.getData(), (double*)&angleaxis);
		for (unsigned int k = 0; k < 3; k++) {
			pose_double[k] = angleaxis[k];
			pose_double[k + 3] = translation[k];
		}
	}

    Problem problem;
	//ICP term
	if (true) {
		for (size_t i = 0; i < correspondences.size(); i++) {
			ceres::CostFunction* costFunction = nullptr;
			costFunction = CorrespondenceCostFunc::Create(correspondences[i], pose_double.data());
			problem.AddResidualBlock(costFunction, NULL, pose_double.data());
		}
	}

	//IMU constraint
	if (false) {
		ceres::CostFunction* costFunction = nullptr;
		costFunction = IMUConstraintCostFunc::Create(imuConstraint);
		problem.AddResidualBlock(costFunction, NULL, pose_double.data());
	}
    
    
    //cout << "Solving..." << endl;

 //   Solver::Options options;
 //   Solver::Summary summary;

 //   options.minimizer_progress_to_stdout = true;

 //   //faster methods
 //   options.num_threads = 1;
 //   options.num_linear_solver_threads = 1;
 //   //options.linear_solver_type = ceres::LinearSolverType::SPARSE_NORMAL_CHOLESKY; //7.2s
 //   //options.linear_solver_type = ceres::LinearSolverType::SPARSE_SCHUR; //10.0s

 //   //slower methods
 //   //options.linear_solver_type = ceres::LinearSolverType::ITERATIVE_SCHUR; //40.6s
 //   //options.linear_solver_type = ceres::LinearSolverType::CGNR; //46.9s
	//options.linear_solver_type = ceres::LinearSolverType::DENSE_NORMAL_CHOLESKY;

 //   //options.min_linear_solver_iterations = linearIterationMin;
 //   options.max_num_iterations = 10000;
 //   options.function_tolerance = 1e-20;
 //   options.gradient_tolerance = 1e-10 * options.function_tolerance;

 //   // Default values, reproduced here for clarity
 //   options.trust_region_strategy_type = ceres::TrustRegionStrategyType::LEVENBERG_MARQUARDT;
 //   options.initial_trust_region_radius = 1e4;
 //   options.max_trust_region_radius = 1e16;
 //   options.min_trust_region_radius = 1e-32;
 //   options.min_relative_decrease = 1e-3;
 //   // Disable to match Opt
 //   //options.min_lm_diagonal = 1e-32;
 //   //options.max_lm_diagonal = std::numeric_limits<double>::infinity();


 //   options.initial_trust_region_radius = 1e4;
 //   options.jacobi_scaling = true;

	ceres::Solver::Options options;
	options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;	//4s
	//options.linear_solver_type = ceres::SPARSE_SCHUR;		//4s
	//options.linear_solver_type = ceres::ITERATIVE_SCHUR;	//11s
	//options.linear_solver_type = ceres::CGNR;				//12s
	options.minimizer_progress_to_stdout = verbose;
	options.max_num_iterations = 100000;
	//options.max_num_consecutive_invalid_steps = 100;
	options.num_threads = 7;
	options.num_linear_solver_threads = 7;
	options.function_tolerance = 1e-20;
	ceres::Solver::Summary summary;

    Solve(options, &problem, &summary);

	if (verbose) {
		std::cout << summary.FullReport() << std::endl;
		double cost = -1.0;
		problem.Evaluate(Problem::EvaluateOptions(), &cost, nullptr, nullptr, nullptr);
		cout << "Cost*2 end: " << cost * 2 << endl;

		cout << summary.FullReport() << endl;
	}

	vec6f pose;
	for (unsigned int k = 0; k < 6; k++) pose[k] = (float)pose_double[k];
	mat4f rotation = mat4f::identity();
	mat4f translation = mat4f::translation(vec3f((float)pose[3], (float)pose[4], (float)pose[5]));

	const vec3f axis = vec3f((float)pose[0], (float)pose[1], (float)pose[2]);
	const float angle = axis.length();
	if (angle >= 1e-10f) {
		rotation = mat4f::rotation(axis.getNormalized(), math::radiansToDegrees(angle));
	}
	return translation * rotation;
}

