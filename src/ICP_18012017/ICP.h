
#pragma once

#include "mLibInclude.h"

#include "CeresSolver.h"


#define GLOG_NO_ABBREVIATED_SEVERITIES

#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "glog/logging.h"


class ICP {
public:
	ICP() {
		m_intrinsic.setZero();
		m_width = m_height = 0;
	}
	~ICP() {}

	void init(const mat4f& intrinsic, const DepthImage32& src, const DepthImage32& dst) {
		MLIB_ASSERT(src.getDimensions() == dst.getDimensions());
		m_intrinsic = intrinsic;

		m_width = src.getWidth();
		m_height = src.getHeight();

		m_src = backProject(src);
		m_dst = backProject(dst);

		m_srcNormals = computeNormals(m_src);
		m_dstNormals = computeNormals(m_dst);

		//FreeImageWrapper::saveImage("test_src_normals.png", m_srcNormals);
		//FreeImageWrapper::saveImage("test_dst_normals.png", m_dstNormals);
	}

	mat4f align(unsigned int outerIter = 10, mat4f relativeIMUEstimate = mat4f::identity()) {
		const float threshDistCamera = 0.1f;	//10 cm;
		const float threshDistNormal = 0.9f;
		const float weightIMU_Min = 2.0f;
		const float weightIMU_Max = 20.0f;
		bool verbose = false;
		
		relativeIMUEstimate.invert();

		//std::cout << "transform: " << relativeIMUEstimate << std::endl;
		//mat4f debugTransform = mat4f::translation(vec3f(0.0f, 0.1f, 0.0f)) * mat4f::rotationX(5.0f);
		//std::cout << "debug transform:\n" << debugTransform << std::endl << std::endl;
		
		 
		mat4f trans = mat4f::identity();
		//trans = relativeIMUEstimate;
		//trans = debugTransform;

		for (unsigned int i = 0; i < outerIter; i++) {
			//applies the current transformation to the source points
			std::vector<ICPCorr> corr = findCorrespondences(m_src, m_dst, m_srcNormals, m_dstNormals, trans, threshDistCamera, threshDistNormal);			

			//test with ground truth correspondences -> works
			//corr.clear();
			//for (unsigned int k = 0; k < 100; k++) {
			//	vec3f p0 = vec3f(math::randomUniform(0.0f, 5.0f), math::randomUniform(0.0f, 5.0f), math::randomUniform(0.0f, 5.0f));
			//	vec3f n0 = vec3f(math::randomUniform(0.0f, 5.0f), math::randomUniform(0.0f, 5.0f), math::randomUniform(0.0f, 5.0f)).getNormalized();

			//	vec3f p1 = debugTransform * p0;
			//	vec3f n1 = debugTransform.transformNormalAffine(n0);

			//	ICPCorr c;
			//	c.p0 = p0;
			//	c.p1 = p1;
			//	c.n = n1;
			//	corr.push_back(c);
			//} 

			//const mat4f t = mat4f::rotationX(45.0f);
			//for (auto& c : corr) {
			//	c.p1 = t * c.p1;
			//}

			//TODO
			IMUConstraint imuConstraint;
			float weightIMU = math::lerp(weightIMU_Min, weightIMU_Max, (float)i / (float)outerIter - 1);
			imuConstraint.weight = weightIMU;
			{
				//init transform
				mat4d transform = relativeIMUEstimate;

				vec3d angleaxis = vec3d(0.0, 0.0, 0.0);
				vec3d translation = transform.getTranslation();

				const mat3d rot = transform.getRotation().getTranspose(); //needs to be column major
				ceres::RotationMatrixToAngleAxis(rot.getData(), (double*)&angleaxis);
				for (unsigned int k = 0; k < 3; k++) {
					imuConstraint.rotation[k] = (float)angleaxis[k];
					imuConstraint.translation[k] = (float)translation[k];
				}

				//std::cout << "transform: " << relativeIMUEstimate << std::endl;
				//std::cout << "constraint (rot):" << imuConstraint.rotation << std::endl;
				//std::cout << "constraint (tra):" << imuConstraint.translation << std::endl;
			}


			//Timer t;
			mat4f newTrans = CeresSolver::solve(corr, imuConstraint, trans);
			//std::cout << "solve time " << t.getElapsedTimeMS() << " [ms] " << std::endl;
			trans = newTrans;

			if (verbose) {
				std::cout << "[ found " << corr.size() << " correspondences ]" << std::endl;
				std::cout << "trans (" << i << ") :\n" << trans << std::endl;
			}
		}


		return trans.getInverse();
	}
private:

	vec3f projToCamera(unsigned int ux, unsigned int uy, float depth) const {
		if (depth == -std::numeric_limits<float>::infinity()) return vec3f(depth);
		float x = ((float)ux - m_intrinsic(0, 2)) / m_intrinsic(0, 0);
		float y = ((float)uy - m_intrinsic(1, 2)) / m_intrinsic(1, 1);
		return vec3f(depth*x, depth*y, depth);
	}

	vec3f cameraToProj(const vec3f& p) const {
		vec3f proj = m_intrinsic*p;
		proj.x /= p.z;
		proj.y /= p.z;
		return proj;
	}

	vec3f computeNormal(unsigned int x, unsigned int y, const PointImage& img) const {

		vec3f ret(img.getInvalidValue());
		if (x > 0 && y > 0 && x < img.getWidth() - 1 && y < img.getHeight() - 1) {
			vec3f cc = img(x, y);
			vec3f pc = img(x + 1, y + 0);
			vec3f cp = img(x + 0, y + 1);
			vec3f mc = img(x - 1, y + 0);
			vec3f cm = img(x + 0, y - 1);

			if (cc != img.getInvalidValue() &&
				pc != img.getInvalidValue() &&
				cp != img.getInvalidValue() &&
				mc != img.getInvalidValue() &&
				cm != img.getInvalidValue())	{

				vec3f n = (pc - mc) ^ (cp - cm);
				float l = n.length();
				if (l > 0.0f) {
					ret = n / l;
				}
			}
		}
		return ret;
	}

	PointImage computeNormals(const PointImage& img) const {
		PointImage res(img.getWidth(), img.getHeight());
		for (auto& n : res) {
			n.value = computeNormal(n.x, n.y, img);
		}
		return res;
	}

	PointImage backProject(const DepthImage32& img) const {
		PointImage res(img.getWidth(), img.getHeight());
		for (auto& p : res) {
			if (img.isValid(p.x, p.y)) {
				p.value = projToCamera(p.x, p.y, img(p.x, p.y));
			}
			else {
				p.value = res.getInvalidValue();
			}
		}
		return res;
	}



	std::vector<ICPCorr> findCorrespondences(const PointImage& src, const PointImage& dst, const PointImage& srcNormals, 
						 const PointImage& dstNormals, const mat4f& trans, float threshDistCamera, 
						 float threshDistNormal) const {

		std::vector<ICPCorr> res;
		for (auto& s : src) {
			if (!src.isValidValue(s.value) || !srcNormals.isValid(s.x, s.y)) continue;

			vec3f p = trans * s.value;
			vec3f proj = cameraToProj(p);

			vec2i dstCoord = math::round(vec2f(proj.x, proj.y));
			vec2i srcCoord = vec2i(s.x, s.y);

			if (dst.isValidCoordinate(dstCoord) && dst.isValid(dstCoord) && dstNormals.isValid(dstCoord)) {
				
				float distCamera = (p - dst(dstCoord)).length();

				const vec3f& srcNormal = trans.transformNormalAffine(srcNormals(s.x, s.y));
				const vec3f& dstNormal = dstNormals(dstCoord);
				float distNormal = srcNormal | dstNormal;
			
				if (distCamera <= threshDistCamera && distNormal >= threshDistNormal) {
					ICPCorr corr;
					//corr.p0 = p;	//use the already transform src point (this way the init will be right)
					corr.p0 = s.value;
					corr.p1 = dst(dstCoord);
					corr.n = dstNormals(dstCoord);
					res.push_back(corr);
				}
			}
		}

		return res;
	}

	unsigned int m_width, m_height;
	PointImage m_src;
	PointImage m_dst;
	PointImage m_srcNormals;
	PointImage m_dstNormals;
	mat4f m_intrinsic;
};






void testICP(const SensorData& sd) {
	DepthImage32 src = sd.computeDepthImage(0);
	DepthImage32 dst = sd.computeDepthImage(0);

	unsigned int subSample = 4;
	src.resize(src.getWidth() / subSample, src.getHeight() / subSample);
	dst.resize(dst.getWidth() / subSample, dst.getHeight() / subSample);
	
	mat4f intrinsic = sd.m_calibrationDepth.m_intrinsic;
	intrinsic._m00 *= 1.0f / subSample;		//focal length
	intrinsic._m11 *= 1.0f / subSample;		//focal length
	intrinsic._m02 *= (float)(src.getWidth() - 1) / (float)(src.getWidth()*subSample - 1);		//principal point
	intrinsic._m12 *= (float)(src.getHeight() - 1) / (float)(src.getHeight()*subSample - 1);	//principal point

	src.smooth(5);
	dst.smooth(5);


	ICP icp;
	icp.init(intrinsic, src, dst);
	mat4f trans = icp.align(5);
}





void testICPSequence(SensorData& sd) {

	for (auto& f : sd.m_frames) f.setCameraToWorld(mat4f::identity());
	mat4f firstRefInv = sd.m_frames[0].getCameraToWorld().getInverse();

	mat4f accumulated = mat4f::identity();

	sd.m_frames[0].setCameraToWorld(accumulated);
	sd.saveToPointCloud("pc_" + std::to_string(0) + ".ply", 0);
	const size_t step = 30;
	for (size_t i = 0; i < sd.m_frames.size() - step; i += step) {
		DepthImage32 src = sd.computeDepthImage(i);
		DepthImage32 dst = sd.computeDepthImage(i+step);

		unsigned int subSample = 4;
		src.resize(src.getWidth() / subSample, src.getHeight() / subSample);
		dst.resize(dst.getWidth() / subSample, dst.getHeight() / subSample);

		mat4f intrinsic = sd.m_calibrationDepth.m_intrinsic;
		intrinsic._m00 *= 1.0f / subSample;		//focal length
		intrinsic._m11 *= 1.0f / subSample;		//focal length
		intrinsic._m02 *= (float)(src.getWidth() - 1) / (float)(src.getWidth()*subSample - 1);		//principal point
		intrinsic._m12 *= (float)(src.getHeight() - 1) / (float)(src.getHeight()*subSample - 1);	//principal point

		src.smooth(5);
		dst.smooth(5);

		ICP icp;
		icp.init(intrinsic, src, dst);
		//Timer t;
		mat4f trans = icp.align(10);	
		//std::cout << "align time " << t.getElapsedTimeMS() << " [ms]" << std::endl;

		accumulated = accumulated * trans;
		//std::cout << "frame-to-frame trans (" << i << "):\n" << trans << std::endl;
		std::cout << "accumulated (" << i << "):\n" << accumulated << std::endl;

		//mat4f ref = firstRefInv * sd.m_frames[i + step].getCameraToWorld();
		//std::cout << "ref:\n" << ref << std::endl;

		sd.m_frames[i+step].setCameraToWorld(accumulated);
		sd.saveToPointCloud("pc_" + std::to_string(i+step) + ".ply", (unsigned int)i+step);

		if (i == 100*step) break;
		break;
	}
}



void realignICP(SensorData& sd) {

	const unsigned int subSample = 4;
	const size_t step = 1;

	//for (auto& f : sd.m_frames) f.setCameraToWorld(mat4f::identity());
	mat4f firstRefInv = sd.m_frames[0].getCameraToWorld().getInverse();

	mat4f accumulated = mat4f::identity();

	//if (i == 0) {
	//	sd.m_frames[0].setCameraToWorld(accumulated);
	//	sd.saveToPointCloud("pc_" + std::to_string(0) + ".ply", 0);
	//}

	std::vector<mat4f> relativeTransforms(sd.m_frames.size(), mat4f::identity());
	for (size_t i = 0; i < sd.m_frames.size() - 1; i++) {
		relativeTransforms[i+1] =  sd.m_frames[i].getCameraToWorld().getInverse() * sd.m_frames[i + step].getCameraToWorld();
	}
	
	accumulated = sd.m_frames[0].getCameraToWorld();
	
	for (size_t i = 0; i < sd.m_frames.size() - step; i += step) {

		mat4f relativeTransform = relativeTransforms[i];

		DepthImage32 src = sd.computeDepthImage(i);
		DepthImage32 dst = sd.computeDepthImage(i + step);
		
		src.resize(src.getWidth() / subSample, src.getHeight() / subSample);
		dst.resize(dst.getWidth() / subSample, dst.getHeight() / subSample);

		mat4f intrinsic = sd.m_calibrationDepth.m_intrinsic;
		intrinsic._m00 *= 1.0f / subSample;		//focal length
		intrinsic._m11 *= 1.0f / subSample;		//focal length
		intrinsic._m02 *= (float)(src.getWidth() - 1) / (float)(src.getWidth()*subSample - 1);		//principal point
		intrinsic._m12 *= (float)(src.getHeight() - 1) / (float)(src.getHeight()*subSample - 1);	//principal point

		src.smooth(5);
		dst.smooth(5);

		ICP icp;
		icp.init(intrinsic, src, dst);
		
		//std::cout << "relativeTransform " << relativeTransform << std::endl;
		Timer t;
		mat4f trans = icp.align(10, relativeTransform);
		std::cout << "align time " << t.getElapsedTimeMS() << " [ms]" << std::endl;

		//mat4f trans = relativeTransform;

		accumulated = accumulated * trans;
		//std::cout << "frame-to-frame trans (" << i << "):\n" << trans << std::endl;
		//std::cout << "accumulated (" << i << "):\n" << accumulated << std::endl;

		//mat4f ref = firstRefInv * sd.m_frames[i + step].getCameraToWorld();
		//std::cout << "ref:\n" << ref << std::endl;

		sd.m_frames[i + step].setCameraToWorld(accumulated);

		//sd.saveToPointCloud("pc_" + std::to_string(i + step) + ".ply", (unsigned int)i + step);
		//if (i == 100 * step) break;
		//break;
	}
}
