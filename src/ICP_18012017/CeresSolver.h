#pragma once

#include "mLibInclude.h"

#include <vector>

#include "config.h"

struct ICPCorr {
	vec3f p0;
	vec3f p1;
	vec3f n;
};

struct IMUConstraint {
	vec3f rotation;
	vec3f translation;
	float weight;
};

class CeresSolver {
public:
	static mat4f CeresSolver::solve(const std::vector<ICPCorr>& correspondences, const IMUConstraint& imuConstraint, const mat4f& initTransform);
private:
};

