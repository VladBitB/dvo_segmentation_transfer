

#include <iostream>
#include <Eigen/Dense>

#include <sophus/se3.hpp>

#include "./libs/ply_module/ply_module.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/highgui.hpp"



using namespace std; 
using namespace Eigen; 

string output_file = "./plane_generated.ply"; 
string output_file2 = "./plane_generated2.ply";
string output_file3 = "./plane_generated3.ply";

int main(){
    
    // ### sample points and save them in a point cloud
    
    int num_points = 100 * 100; 
    
    MatrixXd generated_plane(num_points, 3); 
    
    double min_x = -5.0f; 
    double max_x = 0.0f;
    
    double min_y = -5.0f;
    double max_y = 0.0f; 
    
    
    
    // ### the first plane
    
    int point_counter = 0; 
    for (double x = min_x; x < max_x; x = x + 0.1f)
        for (double y = min_y; y < max_y; y = y + 0.1f){
            
            //double z = 5 * x + 3 * y - 4; 
            double z = 0; 
            
            generated_plane(point_counter, 0) = x; 
            generated_plane(point_counter, 1) = y; 
            generated_plane(point_counter, 2) = z; 
            
            point_counter++; 
            
        }
    
        
    cout << "point_counter: " << point_counter << endl; 
     
    write_ply_file(output_file, generated_plane, nullptr, {255, 255, 255}, 2); 
    cout << "writing accomplished" << endl; 
    
    
    
    // ### the second plane
        
    MatrixXd generated_plane2(num_points, 3); 
    
    
    int point_counter2 = 0; 
    for (double x = min_x; x < max_x; x = x + 0.1f)
        for (double y = min_y; y < max_y; y = y + 0.1f){
            
            //double z = 5 * x + 3 * y - 4; 
            //double z = -2 * (x + 0.5 * y); 
            double z = 0;
            
            generated_plane2(point_counter2, 0) = x; 
            generated_plane2(point_counter2, 1) = z; 
            generated_plane2(point_counter2, 2) = y; 
            
            point_counter2++; 
            
        }
    
        
    cout << "point_counter2: " << point_counter2 << endl; 
     
    write_ply_file(output_file2, generated_plane2, nullptr, {255, 255, 255}, 2); 
    cout << "writing accomplished" << endl; 
    
    
    // ### the third plane
        
    MatrixXd generated_plane3(num_points, 3); 
    
    
    int point_counter3 = 0; 
    for (double x = min_x; x < max_x; x = x + 0.1f)
        for (double y = min_y; y < max_y; y = y + 0.1f){
            
            //double z = 5 * x + 3 * y - 4; 
            //double z = -2 * (x + 0.5 * y); 
            double z = 0;
            
            generated_plane3(point_counter3, 0) = z; 
            generated_plane3(point_counter3, 1) = x; 
            generated_plane3(point_counter3, 2) = y; 
            
            point_counter3++; 
            
        }
    
        
    cout << "point_counter3: " << point_counter3 << endl; 
     
    write_ply_file(output_file3, generated_plane3, nullptr, {255, 255, 255}, 2); 
    cout << "writing accomplished" << endl; 
    
    
    
    
    
    // ### project points using a virtual camera
    // ### use Kinect intrinsics for projection
    
    MatrixXd planes(1 * num_points, 3); 
    planes.block(0, 0, num_points, 3) = generated_plane3; 
    //planes.block(num_points, 0, num_points, 3) = generated_plane2;
    //planes.block(2*num_points, 0, num_points, 3) = generated_plane3;
    
    write_ply_file("./planes_SEPARATE_C.ply", planes, nullptr, {255, 255, 255}, 2); 
    
    
    
    char c; cin >> c; 
    
    
    // ### 
    
    
    Vector3d min_values; 
    Vector3d max_values; 
    Vector3d range; 
    min_values = planes.colwise().minCoeff(); 
    max_values = planes.colwise().maxCoeff(); 
    range = max_values - min_values; 
    
    double norm_scale = 1.0f; 
    double scale_factor = norm_scale / range.mean();  
    
    
    // ### ...or set it pre-defined 
    
    // double scale_factor = 1/100.0f; 
    
    planes = planes * scale_factor; 
    
    
    Vector3d center_of_mass; 
    center_of_mass = planes.colwise().mean(); 
    
    
    // ### convert point cloud to the depth map, use normalization
    
    Matrix3f K; 
    
    K << 525.0f,   0.0f,   319.5f,    // ### we are free to use any meaningful intrinsics, leave the K unchanged
         0.0f,     525.0f, 239.5f,
         0.0f,     0.0f,   1.0f;
         
    
    double fx = K(0,0);
    double fy = K(1,1);
    double cx = K(0,2);
    double cy = K(1,2);
         
    //
    // ### project the points 
    // ### create an empty depth map
    // ### format of the depth map 
    //
    
    /*
     The depth maps are stored as 640×480 16-bit monochrome images in PNG format.
     The depth images are scaled by a factor of 5000, i.e., a pixel value of 5000 in the depth 
     image corresponds to a distance of 1 meter from the camera, 10000 to 2 meter distance, etc. 
     A pixel value of 0 means missing value/no data. 
    */
    
    cv::Mat image(480, 640, CV_16U, cv::Scalar(0,0,0)); 
    
    
    double DISP_Z = 1.50; // distance to the camera along the Z direction, in m
    
    int h = 0;            // number of points projected out of bounds
    
    Vector3d camera;
    camera = center_of_mass;
    camera(2) = center_of_mass(2) + DISP_Z; 
    
    
    for (uint i = 0; i < planes.rows(); i++){ 
        
        double u = 0; 
        double v = 0; 
        
        double X = planes(i, 0);
        double Y = planes(i, 1);
        double Z = sqrt(   (planes(i, 0) - camera(0)) * (planes(i, 0) - camera(0)) + 
                            (planes(i, 1) - camera(1)) * (planes(i, 1) - camera(1)) + 
                            (planes(i, 2) - camera(2)) * (planes(i, 2) - camera(2))   
                       ); 
         
        
        u = fy * Y / Z + cy;                // 320, 240 are in image pixels, focal length is also in image pixels?..
        v = fx * X / Z + cx;                // 
        
        
        // ### u, v -> to camera coordinates
        
        // scale by 5000: 5000 corresponds to 1 m
        // set projection center to the center of mass of the point cloud
        // check if a point is projected onto the image plane 
        // compute number of out of image plane projections 
        
        if ( (u > 0) && (u < 480) && (v > 0) && (v < 640) ) 
          image.at<ushort>(u, v) = (ushort) ( Z * 5000.0f); 
        else h++;
        
    }
    
    cout << h << endl;      // number of points projected out of bounds
    
    
    // ### postprocess the depth map (Laplace-like filtering)
    
#if 1    
    
    // copy the initial image and work on it
    cv::Mat image_init = image.clone();
    
    uint F_SIZE = 11;                               // dimension of the Laplace-like kernel
    
    for (uint i = 0 + 3; i < 480 - 3; i++)
        for (uint j = 0 + 3; j < 640 - 3; j++){
            
            if (image_init.at<ushort>(i, j) == 0){ 
                
                double sum = 0.0f; 
                uint not_empty = 0; 
                
                for (uint k = 0; k < F_SIZE; k++)
                    for (uint l = 0; l < F_SIZE; l++){
                        uint i_subindex = i - 3 + k;
                        uint j_subindex = j - 3 + l;
                        
                        if (image_init.at<ushort>(i_subindex, j_subindex) != 0){
                           sum = sum + image_init.at<ushort>(i_subindex, j_subindex); 
                           not_empty++; 
                        }       
                }
                
              // ### this can be improved later, but works well for now 
              if (not_empty > 2){
                image.at<ushort>(i, j) = (ushort) ( sum / not_empty ); 
                }
    
            }
    
    }
    
#endif
    
    // ### save the depth map
    imwrite("./data/depthmap.png", image); 
    
    // create a dummy color image for integration into TSDF
    // The color images are stored as 640×480 8-bit RGB images in PNG format. 
    cv::Mat color_image(480, 640, CV_8UC3, cv::Scalar(0,255,0)); 
    imwrite("./data/color.png", color_image); 
    
    // ### done!
    
    
    
    return 0; 
}