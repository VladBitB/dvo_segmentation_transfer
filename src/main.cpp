// Copyright 2016 Robert Maier, Technical University of Munich
// Copyright 2017 Vlad Golyanik, University of Kaiserslautern

#include "./main.hpp"
#include "./GlobalDynamicReconstructor.hpp"


#define num_pairs 1100

int main(int argc, char *argv[])
{
  
	// ### test DynamicReconstructor 
	string settings_file_dr; 
        
        if (argc == 2) settings_file_dr = argv[1]; 
        else settings_file_dr = "settings.cfg"; 
	
	
	// ### for authomatic experiments...
	
	
#if 1	
	string settings_basis = "./settings_files/settings_%04d.cfg"; 
	
	string basis = "flo%0004d.flo";

	
	for (uint i = 1; i < num_pairs; i++)
	
	  
#endif	  
	{
	  
#if 1
	  char tmp_charx[200];
	  sprintf(tmp_charx, strcpy(tmp_charx, settings_basis.c_str()), i);
	  string current_settings = tmp_charx;
	  
	  
	  char tmp_char[200];
	  sprintf(tmp_char, strcpy(tmp_char, basis.c_str()), i);
	  string of_to_save = tmp_char;

	  cout << "current_settings: " << current_settings << endl; 
	  cout << "of_to_save: " << of_to_save << endl; 
	  DynamicReconstructor dynamic_reconstructor(current_settings); 
	  
#endif	  
	  
	 
	  
	//DynamicReconstructor dynamic_reconstructor(settings_file_dr); 
	
	
	dynamic_reconstructor.load_settings(); 
	
	
	// ### catch file names...
	
	
	dynamic_reconstructor.load_RGBD_data(); 
	dynamic_reconstructor.pre_processing(); 
	dynamic_reconstructor.compute_normals(0); 
	dynamic_reconstructor.initialize_segmentation(i); 
	dynamic_reconstructor.compute_adjacency_matrix(); 
	dynamic_reconstructor.fetch_segment_colors(); 
	
	dynamic_reconstructor.visualize_segment_neighbours(); 
	//char c; cin >> c; 
	
	//dynamic_reconstructor.visualize_segments(); 
	dynamic_reconstructor.init_poses(); 
	//dynamic_reconstructor.define_and_solve_nlo_problem(); 
	//dynamic_reconstructor.define_and_solve_global_nlo_problem(); 
	
	
	
	dynamic_reconstructor.define_and_solve_global_all_to_all_nlo_problem(); 
	
	//string of_to_save = "FLOTEST.flo"; 
	
	dynamic_reconstructor.reconstruct_every_segment(of_to_save, i); 
	
	//dynamic_reconstructor.post_regularizer(); 
	
	dynamic_reconstructor.save_results(2); 
	
	
	
	}
	
	// clean up
	//cv::destroyAllWindows();
	std::cout << "direct image alignment finished..." << std::endl;
	
	return 0;
}