#ifndef _AUX_H
#define _AUX_H

#define INVALID_RESIDUAL    0.0

double computeError(ceres::Problem &problem)
{
	// evaluate problem cost
	ceres::Problem::EvaluateOptions evalOptions;
	evalOptions.apply_loss_function = true;
	evalOptions.num_threads = 8;
	double errorEval;
	problem.Evaluate(evalOptions, &errorEval, 0, 0, 0);
	return errorEval;
}


int computeNumValidResiduals(const ceres::Problem &problem, const std::vector<double*> &parameters)
{
	std::vector<ceres::ResidualBlockId> problemResidualsBlocks;
	problem.GetResidualBlocks(&problemResidualsBlocks);
	int numValidResiduals = 0;
	for (size_t i = 0; i < problemResidualsBlocks.size(); ++i)
	{
		const ceres::CostFunction* costFunction = problem.GetCostFunctionForResidualBlock(problemResidualsBlocks[i]);
		double residual;
		bool eval = costFunction->Evaluate(&(parameters[0]), &residual, 0);
		if (eval && residual != INVALID_RESIDUAL)
			++numValidResiduals;
	}
	return numValidResiduals;
}


cv::Mat downsampleGray(const cv::Mat &gray)
{
	cv::Mat grayDown;
	cv::pyrDown(gray, grayDown, cv::Size(gray.cols / 2, gray.rows / 2));
    return grayDown;
}


cv::Mat downsampleDepth(const cv::Mat &depth)
{
	// downscaling by averaging the depth
	int w = depth.cols / 2;
	int h = depth.rows / 2;
    cv::Mat depthDown = cv::Mat::zeros(h, w, depth.type());
    for (int y = 0; y < h; ++y)
    {
		for (int x = 0; x < w; ++x)
        {
            int cnt = 0;
            float sum = 0.0f;
			float d0 = depth.at<float>(2 * y, 2 * x);
			if (d0 > 0.0f) { sum += d0; ++cnt; }
			float d1 = depth.at<float>(2 * y, 2 * x + 1);
			if (d1 > 0.0f) { sum += d1; ++cnt; }
			float d2 = depth.at<float>(2 * y + 1, 2 * x);
			if (d2 > 0.0f) { sum += d2; ++cnt; }
			float d3 = depth.at<float>(2 * y + 1, 2 * x + 1);
			if (d3 > 0.0f) { sum += d3; ++cnt; }
            if (cnt > 0)
				depthDown.at<float>(y, x) = sum / float(cnt);
        }
    }
    return depthDown;
}


void downsample(int numPyramidLevels, std::vector<Eigen::Matrix3d> &kPyramid,
	std::vector<cv::Mat> &grayRefPyramid, std::vector<cv::Mat> &depthRefPyramid, 
	 std::vector<cv::Mat> &grayCurPyramid, std::vector<cv::Mat> &depthCurPyramid)
{
	for (int i = 1; i < numPyramidLevels; ++i)
	{
		// downsample camera matrix
		Eigen::Matrix3d kDown = kPyramid[i - 1];
		kDown.topLeftCorner(2, 3) = kDown.topLeftCorner(2, 3) * 0.5;
		kPyramid.push_back(kDown);
		// downsample grayscale images
		grayRefPyramid.push_back(downsampleGray(grayRefPyramid[i - 1]));
		grayCurPyramid.push_back(downsampleGray(grayCurPyramid[i - 1]));
		// downsample depth images
		depthRefPyramid.push_back(downsampleDepth(depthRefPyramid[i - 1]));
		depthCurPyramid.push_back(downsampleDepth(depthCurPyramid[i - 1]));
	}
}


#endif