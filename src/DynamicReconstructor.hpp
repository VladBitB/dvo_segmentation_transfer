
#ifndef DYNAMICRECONSTRUCTOR_H
#define DYNAMICRECONSTRUCTOR_H

#include "./main.hpp"


class DynamicReconstructorL{
  
  
public: 
  
  DynamicReconstructorL(const string settings_file_) : settings_file(settings_file_), num_segments(0) {}; 
  
  ~DynamicReconstructorL(){
    
     delete [] normals; 
     delete [] vertexMapRefX; 
    
    
  }; 
  
  int load_settings(); 
  int load_RGBD_data(); 
  int pre_processing(); 
  int compute_normals(); 
  int initialize_segmentation(); 
  int visualize_segments(); 
  int init_poses(); 
  int compute_adjacency_matrix(); 
  int define_and_solve_nlo_problem(); 
  int define_and_solve_global_nlo_problem(); 
  int save_results(int save_type); 
  
  //int solve_nlo_problem(); 
  
  int fetch_segment_colors(); 
  
  int visualize_segment_neighbours(); 
  
  
  
  int load_settings_and_data(cv::Mat &depthRefL, 
			  cv::Mat &grayRefL, 
			  cv::Mat &depthCurL, 
			  cv::Mat &grayCurL, 
			  cv::Mat &segmentation, 
			  uint num_segments_, 
			  Eigen::Matrix3d &K_, 
			  conf_dvo_ceres &confL); 
  
  
  vector<Eigen::Matrix<double, 6, 1>> return_poses(){
    
    return poses_; 
    
  }; 
  
  
private:   
  
  string settings_file; 
  conf_dvo_ceres conf; 		// settings 
  
  // parameters 		// 
  int numPyramidLevels;
  int maxLvl;
  int minLvl;
  int maxIterations;
  int useHuber;
  int earlyBreak;
  double convergenceErrorRatio;
  bool debug;
  
  cv::Mat grayRef, grayCur, depthRef, depthCur, orig_depthRef, orig_depthCur; 
  
  // ### structures for global optimization
  vector<cv::Mat> grayCur_array; 
  vector<cv::Mat> depthCur_array; 
  
  Eigen::Matrix3d K; 
  std::vector<cv::Mat> grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr; 
  std::vector<Eigen::Matrix3d> kPyr; 
  
  cv::Mat *normals; 		//[numPyramidLevels]; 
  cv::Mat *vertexMapRefX; 	//[numPyramidLevels]; 
  
  // vector<cv::Mat> *normals_array; // current frames do not require normals
  
  
  std::vector<double> timestampsDepth;
  std::vector<double> timestampsColor;
  std::vector<std::string> filesDepth;
  std::vector<std::string> filesColor;
  
  std::string path_depthRef; 
  std::string path_depthCur; 
  std::string path_colorRef; 
  std::string path_colorCur; 
  
  uint num_segments; 
  
  vector<cv::Mat> segmentations; 
  
  int height, width; 
  
  vector<Eigen::Matrix<double, 6, 1>> poses_; 
  vector<double> weights; 
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> adjm; 
  int regularizer_num_pairs; 
  
  vector<Eigen::MatrixXi> segment_colors; // colors of the segmented reference
  Eigen::MatrixXi colors_current; 
  Eigen::MatrixXi colors_reference; 
  
  vector<int> num_elements_per_segment;
  
					  // add all parameters required for the reconstruction throughout the whole process
  
  
					  // methods load_settings, create energy function, fill residuals etc... 
  
  // instantiate problem
  // ceres::Problem *problem; 
  
  
}; 



int DynamicReconstructorL::load_settings_and_data(cv::Mat &depthRefL, 
						  cv::Mat &grayRefL, 
						  cv::Mat &depthCurL, 
						  cv::Mat &grayCurL, 
						  cv::Mat &segmentation, 
						  uint num_segments_, 
						  Eigen::Matrix3d &K_, 
						  conf_dvo_ceres &confL
 						){
  
  K = K_; 
  
  // ### here -> load all necessary settings 
  
  conf._optimization_type = OPTIMIZATION_TWO_FRAMES; 
  
  conf._invalid_residual = 0.0; 
  conf._alpha 	= 1.0; 
  conf._beta 	= 1.0; 
  conf._gamma 	= 1000000.0; 
  conf._zeta 	= 0.0; 
  
  
  conf._convergenceErrorRatio = confL._convergenceErrorRatio; 
  
  
  conf._depth_filter_type = confL._depth_filter_type; 
  conf._num_levels = confL._num_levels; 
  conf._max_iterations = confL._max_iterations; 
  
  conf._ceres_max_iterations = confL._ceres_max_iterations; 
  conf._ceres_num_threads = confL._ceres_num_threads; 
  
  conf._enable_segments = 3; 		// EXTERNAL_SEGMENTS
  conf._use_Huber = confL._use_Huber; 
  
  conf._early_break = confL._early_break; 
  conf._num_elem_threshold = confL._num_elem_threshold; 
  conf._save_segments = confL._save_segments; 
  
  conf._pose_pairs_per_segment = confL._pose_pairs_per_segment; 
  
  
  // set parameters
  numPyramidLevels = conf._num_levels; 
  maxLvl = numPyramidLevels - 1; 
  minLvl = 0; 
  maxIterations = conf._max_iterations; 
  useHuber = conf._use_Huber; 
  earlyBreak = conf._early_break; 
  convergenceErrorRatio = conf._convergenceErrorRatio; 
  debug = false; 
  
  
  // ### here -> copy the data from two RGB-D frames 
  
  depthRef = depthRefL.clone(); 
  depthCur = depthCurL.clone(); 
  
  grayRef = grayRefL.clone(); 
  grayCur = grayCurL.clone(); 
  
  height 	= depthCur.rows; 
  width 	= depthCur.cols; 
  
  // ### here -> segmentation 
  
  segmentations.push_back(segmentation); 
  
  num_segments = num_segments_; 
  
  
  
  // ### segmentation of the finest level accomplished, perform downsampling 
  auto downsample_segmentation = [](cv::Mat &segm){
    
    if (( (segm.rows % 2) != 0 ) || ( (segm.cols % 2) != 0)){
      cout << "not possible to downsample, not further divisible by 2..." << endl; 
      exit(0);
    }
    
    cv::Mat current_segmentation(segm.rows / 2, segm.cols / 2, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (int i = 0; i < segm.rows; i=i+2)
      for (int j = 0; j < segm.cols; j=j+2){
	current_segmentation.at<int>(i/2, j/2) = segm.at<int>(i, j); 
      }
    
    return current_segmentation; 
    
  };
  
  
  for (int lvl = 0; lvl < maxLvl; lvl++) {
    segmentations.push_back(downsample_segmentation(segmentations[lvl])); 
  }
  
  return 0; 
  
  
}



int DynamicReconstructorL::load_settings(){
  
  conf.load_dvo_ceres_settings(settings_file);
  
  if (conf.dump_dvo_ceres_settings_ == 1){
         cout << endl; 
         conf.dump_dvo_ceres_settings(); 
  }
  
  // set parameters
  numPyramidLevels = conf._num_levels; 
  maxLvl = numPyramidLevels - 1;
  minLvl = 0;
  maxIterations = conf._max_iterations; 
  useHuber = conf._use_Huber; 
  earlyBreak = conf._early_break; 
  convergenceErrorRatio = conf._convergenceErrorRatio; 
  debug = false;
  
  
  return 0; 
  
}



int DynamicReconstructorL::load_RGBD_data(){
  
  std::cout << "load input RGB-D data..." << std::endl; 
  
  TumBenchmark* tumBenchmark_ = new TumBenchmark(); 
  if (!tumBenchmark_->loadCamIntrinsics(conf._intrinsics_file, K)){
            std::cout << "Camera intrinsics file ('" << conf._intrinsics_file << "') could not be loaded! Using defaults..." << std::endl;
            // default intrinsics
            K <<    525.0f, 0.0f, 319.5f,
                    0.0f, 525.0f, 239.5f,
                    0.0f, 0.0f, 1.0f;
  }
        
  std::cout << K << std::endl; 
       
  if (!tumBenchmark_->loadAssoc(conf._data_assoc_file, timestampsDepth, filesDepth, timestampsColor, filesColor)){
            std::cout << "Assoc file ('" << conf._data_assoc_file << "') could not be loaded!" << std::endl;
            return 1;
  }
    
  path_depthRef = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[0]; 
  path_depthCur = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[1]; 
  path_colorRef = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[0]; 
  path_colorCur = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[1]; 
  //std::cout << path_depthRef << endl; 
  //std::cout << path_depthCur << endl; 
  //std::cout << path_colorRef << endl; 
  //std::cout << path_colorRef << endl; 
        
  // ### take the first two images respectively (later: all required images)
        
  cout << "loading files ..."; 
  cout.flush();  
  depthRef    = tumBenchmark_->loadDepth(path_depthRef);  
  depthCur    = tumBenchmark_->loadDepth(path_depthCur);  
  grayRef     = tumBenchmark_->loadColor_CV_32FC1(path_colorRef); 
  grayCur     = tumBenchmark_->loadColor_CV_32FC1(path_colorCur); 
  
  cout << "optimization_type: " << conf._optimization_type << endl; 
  
  if ( conf._optimization_type == OPTIMIZATION_GLOBAL ){
    
    uint _n_frames = filesColor.size(); 
    
    for (uint i = 2; i < _n_frames; i++){
      
      cv::Mat color_measurement; 
      cv::Mat depth_measurement; 
      
      string path_c = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[i]; 
      string path_d = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[i]; 
      
      color_measurement = tumBenchmark_->loadColor_CV_32FC1(path_c); 
      depth_measurement = tumBenchmark_->loadDepth(path_d); 
      
      grayCur_array.push_back(color_measurement); 
      depthCur_array.push_back(depth_measurement); 
      
    }
    
    
    /*
    for (uint i = 0; i < grayCur_array.size(); i++){
      imshow("image", grayCur_array[i]); 
      cvWaitKey(0); 
    }
    */
    
    
  }
  
  //cout << "grayCur_array.size(): " << grayCur_array.size() << endl; 
  //char r; cin >> r; 
  
  
  cout << " [done]" << endl; 
  
  // ### set height and width of the finest level image
  
  height 	= depthCur.rows; 
  width 	= depthCur.cols; 
  
  return 0; 
  
}; 



int DynamicReconstructorL::pre_processing(){
  
  // ### Gaussian Blur
  cv::Mat dst; 
  cv::GaussianBlur( grayRef, dst, cv::Size( 7, 7), 0, 0 ); 
  grayRef = dst.clone(); 
  cv::GaussianBlur( grayCur, dst, cv::Size( 7, 7), 0, 0 ); 
  grayCur = dst.clone(); 
  
  if (conf._optimization_type == OPTIMIZATION_GLOBAL){
    
    for (uint i = 0; i < grayCur_array.size(); i++){
      
      cv::Mat buf; 
      cv::GaussianBlur( grayCur_array[i], buf, cv::Size( 7, 7), 0, 0 ); 
      grayCur_array[i] = buf.clone(); 
      
    }
    
    
    
  }
  
  
  // ### 
  
  // TODO filtering of the depth maps for global optimization is not implemented yet
  
  
  
  // ###
        
  orig_depthRef = depthRef.clone();
  orig_depthCur = depthCur.clone();
        
  if (conf._depth_filter_type == AVG_FILTER){
            std::cout << "running wavg filter" << std::endl; 
            weighted_average_filter(depthRef); 
            weighted_average_filter(depthCur); 
  }
    
  if (conf._depth_filter_type ==  BILATERAL_FILTER){
            std::cout << "running bilateralFilter" << std::endl; 
            bilateralFilter(orig_depthRef, depthRef, 0.5); 
            bilateralFilter(orig_depthCur, depthCur, 0.5); 
  }
     
  if (conf._depth_filter_type == MEDIAN_FILTER){
            std::cout << "running median filter" << std::endl; 
            median_filter(depthRef); 
            median_filter(depthCur); 
  } 
  
  
  std::cout << "downsample input ..." << std::endl;
  
  grayRefPyr.push_back(grayRef);
  depthRefPyr.push_back(depthRef);
  grayCurPyr.push_back(grayCur);
  depthCurPyr.push_back(depthCur);
  kPyr.push_back(K);
  downsample(numPyramidLevels, kPyr, grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr); 
  
  
  // TODO downsampling of the depth maps and images for global optimization is not implemented yet
  
  
  
  return 0; 
  
}; 




int DynamicReconstructorL::compute_normals(){
   
  normals 	= new cv::Mat[numPyramidLevels]; 
  vertexMapRefX = new cv::Mat[numPyramidLevels]; 
  
  RgbdProcessing* rgbdProc = new RgbdProcessing(); 
  
  for (uint i = 0; i < numPyramidLevels; i++){
    
    cout << "level: " << i << endl; 
    rgbdProc->computeVertexMap(kPyr[i], depthRefPyr[i], vertexMapRefX[i]); 
    rgbdProc->computeNormals(vertexMapRefX[i], normals[i], 5.0f); 
    
    /*
    cv::Mat normalsVis; 
    rgbdProcX->visualizeNormals(normals[i], normalsVis); 
    imshow("level ", normalsVis);
    cvWaitKey(0); 
    */
    
  }
  
  return 0; 
  
}; 




int DynamicReconstructorL::initialize_segmentation(){
  
  if ( conf._enable_segments == NO_SEGMENTS ){ 
    
    num_segments = 1; 
    
    cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    
    // the whole segmentation is filled with 0, i.e., an index of the only segment
    
    segmentations.push_back(current_segmentation); 
    num_elements_per_segment.push_back(height * width); 
    
  }
  else if ( conf._enable_segments == TEST_SEGMENTS ){ 
    
    num_segments = 2; 
    
    cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (uint i = height / 2; i < height; i++)
      for (uint j = 0; j < width; j++)
	current_segmentation.at<int>(i, j) = 1;    
      
    // two segments are obtained in total, with indexes 0 and 1
    
    segmentations.push_back(current_segmentation); 
     
    num_elements_per_segment.push_back( (width * height) / 2);
    num_elements_per_segment.push_back( num_elements_per_segment[0] ); 
    
  }
  else if ( conf._enable_segments == REGULAR_SEGMENTS ){ 
    
    num_segments = (width / conf._tile_width ) *  ( height / conf._tile_height ); 
    
    auto regular_segmentation = [](vector<cv::Mat> &segmentations_, int w, int h, int segm_width, int segm_height)
	  {
	    
	    cv::Mat regular_segmentation(h, w, CV_32S, cv::Scalar(0, 0, 0)); 
	    
	    for (uint i = 0; i < h; i++)
	      for (uint j = 0; j < w; j++){
		
		int segm_displ_y = i / segm_height; 
		int segm_displ_x = j / segm_width; 
		int width_in_segments = w / segm_width; 
		int current_segment = segm_displ_y * width_in_segments + segm_displ_x; 
		regular_segmentation.at<int>(i, j) = current_segment; 
	      }
	      
	    segmentations_.push_back(regular_segmentation); 
	      
	  };
    
    regular_segmentation(segmentations, width, height, conf._tile_width, conf._tile_height); 
    
    for (uint i = 0; i < num_segments; i++)
      num_elements_per_segment.push_back(conf._tile_width * conf._tile_height); 
    
    
    //update_correspondences(w, h, (const float*)depthRef.data, (const float*)depthCur.data, 
    //					       poses_, CORRESP, intrinsics, segmentations[lvl]); 
    
    
    //cout << segmentations.size() << endl; 
    //cout << num_elements_per_segment.size() << endl; 
    //cout << 
    //char t; cin >> t; 
    
  }
  else if ( conf._enable_segments == SEGMENTS_ENABLED ){
    
	  string mesh_file_name = "./RESULT/MESH_REF.ply"; 
	  
	  // compute mesh from the first (reference) point cloud
	  // lookup maps (row, col) coordinate to a vertex in a mesh 
	  // lookup is required because not all vertices are included 
	  // into the mesh 
	  vector<int> lookup = compute_and_save_mesh(depthRef, K, mesh_file_name); 
	  
	  // segment the mesh using Felzenszwalb algorithm
	  const float kthr = 50; 							// graph cut k threshold 
	  const int minSegVerts = 500; 							// min number of points in a segment
	  const std::vector<int> comps = segment(mesh_file_name, kthr, minSegVerts); 
	  
	  // save a coloured ply segmentation
	  std::string coloured_file = "./RESULT/SEGMENTED.ply"; 
	  visualize_segmentation(comps, mesh_file_name, coloured_file); 
	  
	  cout << "mesh written and segmented..." << endl; 	  
	  cout << "computing segments" << endl; 
	  
	  // create segmentation masks 
	  // create a new segmentation mask associated with a number (index converter...) 
	  
	  std::map<int, uint> segm_key; 
	  for (uint i = 0; i < comps.size(); i++){ 
	    int current_value = comps[i]; 
	    if ( segm_key.insert(std::make_pair(current_value, num_segments)).second == true )
	      num_segments++; 
	  }
	  
	  num_elements_per_segment.reserve(num_segments); 
	  num_elements_per_segment.assign(num_segments, 0); 
	  
	  // ### remove all segments which are smaller than 10000 pixels...
	  for (uint i = 0; i < grayRefPyr[0].rows * grayRefPyr[0].cols; i++){			// TODO replace with width, heigh
	    int segment_id = segm_key.find(comps[lookup[i]])->second; 
	    num_elements_per_segment[segment_id]++; 
	  }
	  
	  uint num_elem_threshold = conf._num_elem_threshold; 
	  uint n_suitable_segments = 0; 

	  // ### reassign numbers to the segments sequentially...
	  vector<int> corrective_shift(num_segments, 0); 
	  
	  for (int i = 0; i < num_segments; i++){
	    if (num_elements_per_segment[i] > num_elem_threshold){
              corrective_shift[i] = n_suitable_segments;
	      n_suitable_segments++; 
	      }
	    else 
	      corrective_shift[i] = -1; 
	  }
	  
	  cout << "n_suitable_segments: " << n_suitable_segments << endl; 
	  num_segments = n_suitable_segments; 
	  
	  cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
	  
	  for (uint i = 0; i < grayRefPyr[0].rows * grayRefPyr[0].cols; i++){			// TODO replace with width, heigh
	    
	    if (lookup[i] != -1){
	     
	      uint c_segm = segm_key.find(comps[lookup[i]])->second; 
	      uint r = trunc( i / grayRefPyr[0].cols); 
	      uint c = i % grayRefPyr[0].cols; 
	         
	      if (num_elements_per_segment[c_segm] > num_elem_threshold)
	        current_segmentation.at<int>(r, c) = corrective_shift[c_segm]; 
	      else 
		current_segmentation.at<int>(r, c) = -1; 
	          
	    }
	    else{
	      uint r = trunc( i / grayRefPyr[0].cols); 
	      uint c = i % grayRefPyr[0].cols; 
	      current_segmentation.at<int>(r, c) = -1; 
	    }
	    
	  } 
	      
	  // ###
	  
	  segmentations.push_back(current_segmentation); 
	  
  }
  else if ( conf._enable_segments == SEGMENTS_SLIC ){
    
    /* Load the image and convert to Lab colour space. */
    const char* path_colorRef_ = path_colorRef.c_str(); 
    IplImage *image = cvLoadImage(path_colorRef_, 1); 
    IplImage *lab_image = cvCloneImage(image); 
    cvCvtColor(image, lab_image, CV_BGR2Lab); 
    
    /* Yield the number of superpixels and weight-factors from the user. */
    int w = image->width, h = image->height; 
    int nr_superpixels = conf._nr_superpixels; 
    int nc = conf._nc; 
    
    
    double step = sqrt((w * h) / (double) nr_superpixels); 
    
    /* Perform the SLIC superpixel algorithm. */
    Slic slic; 
    slic.generate_superpixels(lab_image, step, nc); 
    slic.create_connectivity(lab_image); 
    
    string save_path = "./slic_segmentation.png"; 
    slic.save_segmentation_DR(save_path, nr_superpixels); 
    
    // ### convert to supported representation + auxiliary structures
    
    cv::Mat slic_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    num_segments = slic.slic_convert(slic_segmentation, num_elements_per_segment); 
    segmentations.push_back(slic_segmentation); 
    
    
    
    /*
    for (uint r = 0; r < slic_segmentation.rows; r++){
      for (uint c = 0; c < slic_segmentation.cols; c++)
	cout << slic_segmentation.at<int>(r, c) << " ";
      cout << endl;  
    }
    */
    
    //cout << num_segments << endl; 
    
    //visualize_segments(); 
    
    //char c; cin >> c; 
    
    
    
  }
  
  // ### check if there are zero depth values 
  // ### check if there are segments with very small number of points
  // ### --> 
  
  /*
  
  const float* depth_data_ = (const float*)depthRef.data; 
  uint zero_counter = 0; 
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      float dRef = depth_data_[i*width + j]; 
      
      if ( dRef == 0 ) {
	zero_counter++; 
	segmentations[0].at<int>(i, j) = -1; 
	
      }
      
      
    }
  
  
  cout << "zero_counter " << zero_counter << endl; 
  
  
  // ### recompute segments...
  
  vector<int> new_segm_counter; 
  new_segm_counter.push_back(-1);
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      if ( !(std::find(new_segm_counter.begin(), new_segm_counter.end(), segmentations[0].at<int>(i, j)) != new_segm_counter.end()) )
        new_segm_counter.push_back(segmentations[0].at<int>(i, j));
      
    }
  
  cout << "new # of segments: " << new_segm_counter.size() - 1 << endl; 
  
  */
  
  //char c; cin >> c; 
  
  //num_segments = new_segm_counter.size() - 1; 
  
  // ### 
  
  
  
  // ### segmentation of the finest level accomplished, perform downsampling 
  auto downsample_segmentation = [](cv::Mat &segm){
    
    if (( (segm.rows % 2) != 0 ) || ( (segm.cols % 2) != 0)){
      cout << "not possible to downsample, not further divisible by 2..." << endl; 
      exit(0);
    }
    
    cv::Mat current_segmentation(segm.rows / 2, segm.cols / 2, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (int i = 0; i < segm.rows; i=i+2)
      for (int j = 0; j < segm.cols; j=j+2){
	current_segmentation.at<int>(i/2, j/2) = segm.at<int>(i, j); 
      }
    
    return current_segmentation; 
    
  };
  
  
  for (int lvl = 0; lvl < maxLvl; lvl++) {
    segmentations.push_back(downsample_segmentation(segmentations[lvl])); 
  }
  
  return 0; 
  
} 



int DynamicReconstructorL::visualize_segments(){
	
	cout << "segmentations.size(): " << segmentations.size() << endl; 
	cout << "num_segments: " << num_segments << endl; 
	
	vector<cv::Mat> vizualizations; 
	visualize_segmentation(segmentations, vizualizations, num_segments); 
	cout << "visualization computed" << endl; 
	for (int i = 0; i < vizualizations.size(); i++){
	  imshow("", vizualizations[i]); 
	  cvWaitKey(0); 
	}
	
  return 0; 
	
}



int DynamicReconstructorL::init_poses(){
  
  uint n_current_frames =  grayCur_array.size() + 1; 
  //cout << n_current_frames << endl; 
  //char r; cin >> r; 
  
  poses_.resize(num_segments * n_current_frames); 			// num_segments *  ( grayCur_array.size() + 1 ) 
  
  for (uint i = 0; i < num_segments * n_current_frames; i++){
    poses_[i].setZero(); 
  }
  
  // in case of the global optimization: a pose per current frame
  
  // ### init using the two-frame alignment for the N-frame case
  // ### from the loaded data... compute 
  
  
  
  
  
  
  
  
  
  
  return 0; 
  
}



int DynamicReconstructorL::compute_adjacency_matrix(){
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> adjm_(num_segments, num_segments); 
  adjacency_matrix(adjm_, segmentations, num_segments, conf._pose_pairs_per_segment, depthRef); 
  regularizer_num_pairs = adjm_.sum(); 
  cout << "regularizer_num_pairs = " << regularizer_num_pairs << endl; 
  adjm = adjm_; 
  //cout << adjm << endl; 
  
  if (weights.empty()){ 
    weights.resize(regularizer_num_pairs); 
    for (uint i = 0; i < regularizer_num_pairs; i++)
      weights[i] = 1.0f; 
  }
    
  //cout << weights.size() << endl; 
  //char t; cin >> t; 
  
}



struct SuccessfulStepCallbackL : public ceres::IterationCallback
	{
	public:
		SuccessfulStepCallbackL()
		{
		}

		virtual ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary)
		{
			if (summary.iteration > 0 && summary.step_is_successful)
				return ceres::SOLVER_TERMINATE_SUCCESSFULLY;
			else
				return ceres::SOLVER_CONTINUE;
		}
	};



int DynamicReconstructorL::define_and_solve_nlo_problem(){
  
  double tmr = (double)cv::getTickCount();
	
  for (int lvl = maxLvl; lvl >= minLvl; --lvl)
  {
		double tmrLvl = (double)cv::getTickCount(); 
		
		// fetch data for the current level
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		grayCur = grayCurPyr[lvl];
		depthCur = depthCurPyr[lvl];
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) }; 
                
		// shortcuts
		const int w = grayCur.cols;
		const int h = grayCur.rows;
    
#define weightedL 0		
		
#if weightedL		
                // ### 2wh for brightness constancy and projective ICP; regularizer_num_pairs for every regularizer pair of poses
		const size_t numResiduals = 2 * (size_t)w * (size_t)h + (size_t)(2 * regularizer_num_pairs); 	// 2 * 
#else
		const size_t numResiduals = 2 * (size_t)w * (size_t)h + (size_t)regularizer_num_pairs; 
#endif
		
		
		double alpha_normalizer = 1.0f / (double) (w * h); 
		double beta_normalizer 	= 1.0f / (double) (w * h); 
		double gamma_normalizer = 1.0f / (double) regularizer_num_pairs; 
		
		//double alpha_normalizer = 1.0f; 
		//double beta_normalizer  = 1.0f; 
		//double gamma_normalizer = 1.0f; 
		
		
                
		
		
		// ### loss function for the brightness constancy
		ceres::LossFunction* lossFunction = 0;
		if (useHuber)
			lossFunction = new ceres::HuberLoss(50.0f / 255.0f);              // 50 grayscale steps 
                

                ceres::LossFunction* lossFunctionBrightnessScaled = 0;
                lossFunctionBrightnessScaled = new ceres::ScaledLoss(lossFunction, conf._alpha * alpha_normalizer, ceres::TAKE_OWNERSHIP); 

                // ### a scaled loss function for the depth variance         
                ceres::LossFunction* lossFunctionDepth = 0;
		if (useHuber){
			lossFunctionDepth = new ceres::HuberLoss(10.0f / 100.0f);          // 10 cm   
                }
                
                ceres::LossFunction* lossFunctionDepthScaled = 0;
                lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, conf._beta * beta_normalizer, ceres::TAKE_OWNERSHIP);  
		  
		  
		  
		  // instantiate problem
		ceres::Problem problem; 
		
                // ### a "reference" is transformed to the "current" frame
		
		std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		std::vector<double*> parameters;
		
		
		// ### now: push additionally the weights
		
		for (uint i = 0; i < num_segments; i++)
		  parameters.push_back(poses_[i].data()); 
		
		for (uint i = 0; i < regularizer_num_pairs; i++)
		  parameters.push_back(&weights[i]); 
		
		
		
                
		
		
		
		//cout << parameters.size() << endl; 
		//cout << regularizer_num_pairs << endl; 
		//char c; cin >> c; 
		
		
		double errorAvgLast = std::numeric_limits<double>::max();  
		
		//cout << "poses_.size(): " << poses_.size() << endl; 
		//cout << "num_segments: " << num_segments << endl; 
		
		for (int itr = 0; itr < maxIterations; ++itr)
		{
		  
		  
		  
		  
		 
		  
		  
		  
		  
			std::cout << "   iteration " << itr << ":" << std::endl; 
                        
			Eigen::MatrixXi CORRESP; 
			
			
			for (uint i = 0; i < num_segments; i++){
			  cout << "pose " << i << ":: " << poses_[i].transpose() << endl;   
			}
			
			
			update_correspondences(w, h, (const float*)depthRef.data, (const float*)depthCur.data, 
					       poses_, CORRESP, intrinsics, segmentations[lvl]); 
			
			// ###
			
			uint num_wrong = 0; 
			uint num_correct = 0; 
			uint eval_depth = 0; 
			uint n_invalid_residual = 0; 
			
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
				  
					const size_t id = (size_t)(y*w + x);
					const size_t id_depth = (size_t)(w*h + y*w + x);
					
					int n_segm = segmentations[lvl].at<int>(y, x); 
					
					
					if (n_segm == -1){
					  residualsBlocks[id] = 0;
					  residualsBlocks[id_depth] = 0; 
					  continue;
					}
					
					
					
                                        // ### add brightness constancy residual

					DvoErrorPhotometric* costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y); 
                                        
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);
                                        
                                           
                                        /*
                                        DvoErrorAuto* costPhotometric = new DvoErrorAuto(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        */
                                        
                                        //ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorAuto, 1, 6>(costPhotometric); 
                                        
					double residual;
                                        
					bool eval = costFunction->Evaluate(&(parameters[n_segm]), &residual, 0);
                                        
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, 
								lossFunctionBrightnessScaled, poses_[n_segm].data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					
					// ### add depth variation residual or projective ICP
					
			
#if 0               			// the common depth variation term with bicubic interpolation                         
                                            
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
#endif                                            
#if 0               			// interpolation with accounting for the weights
                                        
                                        DvoErrorAutoDepth* costDepthVariation = new DvoErrorAutoDepth(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorAutoDepth, 1, 6>(costDepthVariation);
#endif                                        
#if 1
					int r = -1, c = -1; 
					
					if (CORRESP(y, x) != -1){
					  r = trunc( (int) CORRESP(y, x) / w); 
					  c = (int) CORRESP(y, x) % w; 
					}

					float* ptrNormals = (float*)normals[lvl].data;				
					size_t off = (y*w + x) * 3;						
					
					DvoErrorDepthICP* costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					
					ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthICP, 1, 6>(costDepthVariationICP);
#endif
                                            
                                        double residualDepth; 
                                        
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[n_segm]), &residualDepth, 0); 
					
					if (evalDepth) eval_depth++; 
					if (residualDepth != INVALID_RESIDUAL) n_invalid_residual++; 
					
					const float* dd_ = (const float*)depthRef.data;
					
					
					if (evalDepth && (residualDepth != INVALID_RESIDUAL) && (CORRESP(y, x) != -1)  ) // && ( dd_[y*w + x] != 0)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, 
									lossFunctionDepthScaled, poses_[n_segm].data());
							
							//num_correct++; 
							
							
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
							
							//num_wrong++; 
							
							
						}
						delete costFunctionDepth;
					}

				    
				}
				
			}
			
			
			/*
			cout << "num_wrong: " << num_wrong << endl; 
			cout << "num_correct: " << num_correct << endl; 
			cout << "eval_depth: " << eval_depth << endl; 
			cout << "valid_residual: " << n_invalid_residual << endl; 
			*/
			
			// ### regularization residuals
			
	
	
			auto add_regularizer = [](std::vector<ceres::ResidualBlockId> &residualsBlocks, 
						  size_t r_block_index, 
						  double _gamma_, 
						  std::vector<double*> &pose_combination, 
						  ceres::Problem &problem, 
#if weightedL		     
						  DvoErrorRegularizationWeighted* costDvoErrorRegularization,
#else
						  DvoErrorRegularization* costDvoErrorRegularization,
#endif
						  ceres::CostFunction* costFunctionReg, 
						  ceres::LossFunction* lossFunctionReg, 
						  ceres::LossFunction* lossFunctionRegScaled
 						)
			{
			  
			  
			  assert( pose_combination.size() == 2 ); 
			   
			  size_t current_index = r_block_index; 
			  
			  lossFunctionRegScaled = new ceres::ScaledLoss(lossFunctionReg, _gamma_, ceres::TAKE_OWNERSHIP); 
#if weightedL			
			  costDvoErrorRegularization = new DvoErrorRegularizationWeighted(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularizationWeighted, 6, 6, 6, 1>(costDvoErrorRegularization); 
#else
			  costDvoErrorRegularization = new DvoErrorRegularization(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularization, 6, 6, 6>(costDvoErrorRegularization); 
#endif
			  
			  vector<double> residualReg(6);                 
			  bool evalReg = costFunctionReg->Evaluate(&(pose_combination[0]), residualReg.data(), 0); 
			  
			  //cout << "reg residuals: " << residualReg[0] << " " << residualReg[1] << " " << residualReg[2] << " "  
			  //<< residualReg[3] << " " << residualReg[4] << " " << residualReg[5] << endl; 
			  //cout << evalReg << endl; 
			  
			  if ( evalReg && (residualReg[0] != INVALID_RESIDUAL) ) // 
			  {
			  if (!residualsBlocks[current_index])
			  {
				residualsBlocks[current_index] = 
				problem.AddResidualBlock(costFunctionReg, 
				lossFunctionRegScaled, pose_combination);   
			  }
			  }
			  else
			  {
			  if (residualsBlocks[current_index])
			  {
				problem.RemoveResidualBlock(residualsBlocks[current_index]);
				residualsBlocks[current_index] = 0;
			  }
			  delete costFunctionReg; 
			  }
			    
			};
			
			
			uint num_combinations = regularizer_num_pairs; 
			
			
			
			// ### here -> push additionally the weights into the pose combination
			
			auto build_pose_combinations = [](std::vector<std::vector<double*> > &pose_combinations, 
						     std::vector<double*> &parameters, 
						     Eigen::SparseMatrix<int, Eigen::RowMajor> &adjm, 
						     int num_segments_ 
						    ){
			  
			  assert(num_segments_ == adjm.rows()); 
			  
			  int n_combinations = adjm.sum(); 
			  int current_comb = 0; 
			  
			  
			  for (int i = 0; i < adjm.outerSize(); i++)
			    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
			      
			      assert(it.value() == 1); 
			      
			      int pose_index_A = it.row(); 
			      int pose_index_B = it.col(); 
			      int weight_index = num_segments_+ current_comb; 
			      
			      pose_combinations[current_comb].push_back(parameters[pose_index_A]); 
			      pose_combinations[current_comb].push_back(parameters[pose_index_B]); 
#if weightedL			      
			      pose_combinations[current_comb].push_back(parameters[weight_index]); 
#endif			      
			      current_comb++; 
			      
			    }
			  
			  assert(current_comb == adjm.sum()); 
			  
			};
			
			
			
			std::vector<std::vector<double*> > pose_combinations(num_combinations); 
			build_pose_combinations(pose_combinations, parameters, adjm, num_segments); 
			size_t index_basis = 2 * (size_t)w * (size_t)h; 
			
			for (uint i = 0; i < num_combinations; i++){
			  
			  size_t current_index = index_basis + (size_t)i; 

#if weightedL
			  DvoErrorRegularizationWeighted* costDvoErrorRegularization; 
#else		  
			  DvoErrorRegularization* costDvoErrorRegularization; 
#endif	
			  ceres::CostFunction* costFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionRegScaled = 0; 
			  
			  add_regularizer(residualsBlocks, 
					current_index, 
					conf._gamma * gamma_normalizer, 
					pose_combinations[i], 
					problem, 
					costDvoErrorRegularization, 
					//weights.data(), 
					//i, 
					costFunctionReg, 
					lossFunctionReg, 
					lossFunctionRegScaled); 
					
			}	// for all combinations
		  
		  
			// ### here: add the new regularizer \sum_(1 - w^2)^2
			
			
#if weightedL			
			
			size_t index_W_shift = 2 * (size_t)w * (size_t)h + regularizer_num_pairs; 
			
			
			//////
			
			double zeta = 1.0; 
			double zeta_normalizer 	= 1.0f / (double) regularizer_num_pairs; 
			
			
			
			for (uint i = 0; i < num_combinations; i++){
			
			DR_Robust_Optimizer_Error* cost_DR_Robust_Optimizer_Error; 
			ceres::CostFunction* costFunctionRO = 0; 
			ceres::LossFunction* lossFunctionRO = 0; 
			ceres::LossFunction* lossFunctionROScaled = 0;  
			  
			size_t w_index = num_segments + (size_t)i; 			// shift of the weights in the parameter array
			size_t res_shift = index_W_shift + i; 
			lossFunctionROScaled = new ceres::ScaledLoss(lossFunctionRO, conf._zeta * zeta_normalizer, ceres::TAKE_OWNERSHIP); 
			cost_DR_Robust_Optimizer_Error = new DR_Robust_Optimizer_Error(); 
			
			costFunctionRO = 
			new ceres::AutoDiffCostFunction<DR_Robust_Optimizer_Error, 1, 1>(cost_DR_Robust_Optimizer_Error); 
			
			double residualRegW;                 
			bool evalRegW = costFunctionRO->Evaluate(&(parameters[w_index]), &residualRegW, 0); 
			
			//cout << evalRegW << endl; 
			//cout << residualRegW << endl; 
			
			if ( evalRegW && (residualRegW != INVALID_RESIDUAL) ) // 
			{
			if (!residualsBlocks[res_shift])
			{
				residualsBlocks[res_shift] = problem.AddResidualBlock(costFunctionRO, lossFunctionROScaled, &weights[i]); 
			}
			}
			else
			{
			if (residualsBlocks[res_shift])
			{
				problem.RemoveResidualBlock(residualsBlocks[res_shift]); 
				residualsBlocks[res_shift] = 0;
			}
			delete costFunctionRO; 
			}   
			 
			};
			
			
#endif			
			
			
			
			
			
			//////
			
			
			
			// ###
		
		
		
			int numValidResidualsBefore = problem.NumResidualBlocks(); 
			if (numValidResidualsBefore == 0)
				continue;

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = conf._ceres_max_iterations;   				//                            
			options.num_threads = 8;
			options.num_linear_solver_threads = 8;
			//options.dogleg_type = ceres::SUBSPACE_DOGLEG; 
			//options.use_nonmonotonic_steps = true; 
			//options.line_search_type = ceres::WOLFE; 
			options.line_search_direction_type = ceres::BFGS; 
			//options.function_tolerance = 1e-20;
			//options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_SCHUR;		//
			//options.linear_solver_type = ceres::ITERATIVE_SCHUR;		// much much faster...
			options.linear_solver_type = ceres::CGNR;			// much much faster... pcg
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;
			
			//SuccessfulStepCallbackL successfulStepCb;
			//options.callbacks.push_back(&successfulStepCb);
			
			ceres::Solver::Summary summary;
			cout << "start solving..." << endl; 
			
			
			
			ceres::Solve(options, &problem, &summary);
			
			/*
			auto t2 = chrono::high_resolution_clock::now();
			cout << "-> non-rigid point cloud registration ready" << endl << "  -> elapsed time: " <<
			chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms" << endl << endl;
			*/
			
			
			if (debug)
				std::cout << summary.FullReport() << std::endl; 
			
			std::cout << std::endl; 
			std::cout << "initial cost: " << summary.initial_cost << std::endl; 
			std::cout << "final cost: " << summary.final_cost << std::endl; 
			std::cout << "fixed cost: " << summary.fixed_cost << std::endl; 
			std::cout << std::endl; 
			
			
			double error = summary.initial_cost / summary.final_cost; 
			
			if ( earlyBreak && ( error < (1.0/convergenceErrorRatio)) ){ 
			  break;
			}
			
			std::cout << std::endl; 
			
		}
  
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                std::cout << std::endl; 
		
  }
  
  // post-processing
  tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
  std::cout << "runtime: " << tmr << std::endl;
  
  // final poses
  for (uint i = 0; i < num_segments; i++)
    std::cout << "pose " << i << " final = " << poses_[i].transpose() << std::endl;
  
  return 0; 
  
}





// ### global optimization : reference to all current frames ("one to many")

int DynamicReconstructorL::define_and_solve_global_nlo_problem(){
  
  double tmr = (double)cv::getTickCount();
  
  for (int lvl = maxLvl; lvl >= minLvl; --lvl)
  {
		double tmrLvl = (double)cv::getTickCount(); 
		
		// fetch data for the current level
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		grayCur = grayCurPyr[lvl];
		depthCur = depthCurPyr[lvl];
		
		// ### here: take the image and depth values saved in grayCur_array and depthCur_array
		// ### for now: only the finest level
		
		uint n_current_frames =  grayCur_array.size() + 1; 
		
		
		// ###
		
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) }; 
  
		
                
		// shortcuts
		const int w = grayCur.cols;
		const int h = grayCur.rows;
    
#define weighted 1
		
#if weighted		
                // ### 2wh for brightness constancy and projective ICP; regularizer_num_pairs for every regularizer pair of poses
		const size_t numResiduals = 2 * n_current_frames * (size_t)w * (size_t)h + (size_t)(2 * regularizer_num_pairs); 	// 2 * 
#else
		const size_t numResiduals = 2 * n_current_frames * (size_t)w * (size_t)h + (size_t)regularizer_num_pairs; 
#endif
		
		
		double alpha_normalizer = 1.0f / (double) (w * h); 
		double beta_normalizer 	= 1.0f / (double) (w * h); 
		double gamma_normalizer = 1.0f / (double) regularizer_num_pairs; 
		
		//double alpha_normalizer = 1.0f; 
		//double beta_normalizer = 1.0f; 
		//double gamma_normalizer = 1.0f; 
		
		//cout << parameters.size() << endl; 
		//cout << regularizer_num_pairs << endl; 
		//char c; cin >> c; 
		
		
		double errorAvgLast = std::numeric_limits<double>::max();  
		
		//cout << "poses_.size(): " << poses_.size() << endl; 
		//cout << "num_segments: " << num_segments << endl; 
		
		
		
		for (int itr = 0; itr < maxIterations; ++itr)
		{
		  
		  
		  // ###
		  
		  
		  
		  // ### loss function for the brightness constancy
		ceres::LossFunction* lossFunction = 0;
		if (useHuber)
			lossFunction = new ceres::HuberLoss(50.0f / 255.0f);              // 50 grayscale steps 
                

                ceres::LossFunction* lossFunctionBrightnessScaled = 0;
                lossFunctionBrightnessScaled = new ceres::ScaledLoss(lossFunction, conf._alpha * alpha_normalizer, ceres::TAKE_OWNERSHIP); 

                // ### a scaled loss function for the depth variance         
                ceres::LossFunction* lossFunctionDepth = 0;
		if (useHuber){
			lossFunctionDepth = new ceres::HuberLoss(10.0f / 100.0f);          // 10 cm   
                }
                
                ceres::LossFunction* lossFunctionDepthScaled = 0;
                lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, conf._beta * beta_normalizer, ceres::TAKE_OWNERSHIP); 
                
		
		
		    // instantiate problem
		    ceres::Problem problem; 
		  
		    // ### a "reference" is transformed to the "current" frame
		
		    std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		    std::vector<double*> parameters;
		
		    
		
		    for (uint i = 0; i < num_segments * n_current_frames; i++)
		      parameters.push_back(poses_[i].data()); 
		
		    
		    // ### push additionally the weights
		    
		    for (uint i = 0; i < regularizer_num_pairs; i++)
		      parameters.push_back(&weights[i]); 
		    
		    
		    
		    
		    // ### 
		  
		  
		       
		  
		    
		      
		      
		      
			std::cout << "   iteration " << itr << ":" << std::endl; 
                        
			Eigen::MatrixXi CORRESP[n_current_frames]; 
			
			
			for (uint cur_i = 0; cur_i < n_current_frames; cur_i++)
			{
			
			
			for (uint i = 0; i < num_segments * n_current_frames; i++){
			  cout << "pose " << i << ":: " << poses_[i].transpose() << endl;   
			}
			
			
			
			// ### here: change the pointers accordingly
			
			
			
			// TODO poses_ -> pass accordingly, a correct subset of poses 
			
			// TODO
			
			//if (cur_i == 0){}
			
			
			
			
			//vector<Eigen::Matrix<double, 6, 1>> *ref = &poses_[num_segments * cur_i]; 
			
			// to extract a subvector is the best solution
			
			
			if (cur_i == 0){
			  
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin();
			  vector<Eigen::Matrix<double, 6, 1>>::const_iterator last = poses_.begin() + num_segments;
			  vector<Eigen::Matrix<double, 6, 1>> pose_subset_(first, last);
			  
			  
			  update_correspondences(w, h, (const float*)depthRef.data, (const float*)depthCur.data, 
					       pose_subset_, CORRESP[cur_i], intrinsics, segmentations[lvl]); 
			  
			}
			else{
			  
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin() + num_segments * ( cur_i);
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator last = poses_.begin() + num_segments * (cur_i + 1);
			  vector< Eigen::Matrix<double, 6, 1> > pose_subset_(first, last);
			  
			  
			  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i-1].data; 
			  
			  
			  update_correspondences(w, h, (const float*)depthRef.data, _p_depthCur_array, 
					       pose_subset_, CORRESP[cur_i], intrinsics, segmentations[lvl]); 
			  
			}
			
			//&(poses_[num_segments * cur_i]),
			
			
			// ###
			
			uint num_wrong = 0; 
			uint num_correct = 0; 
			uint eval_depth = 0; 
			uint n_invalid_residual = 0; 
			
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
				  
					const size_t id_correction = 2 * cur_i * (size_t)w * (size_t)h; 
					
					const size_t id 	= id_correction + 	(size_t)(y*w + x); 
					const size_t id_depth 	= id_correction +	(size_t)(w*h + y*w + x); 
					
					//const size_t id_depth_correction 	= cur_i * n_current_frames; 
					
					const size_t pose_correction = num_segments * cur_i; 
					
					int n_segm = segmentations[lvl].at<int>(y, x); 
					
					
					if (n_segm == -1){
					  residualsBlocks[id] = 0; 
					  residualsBlocks[id_depth] = 0; 
					  continue; 
					}
					
					// TODO place all current frames to a single array or
					// TODO add another block...
					// TODO make grayCur an array for all residuals?..
					
					
					
					
					
					
					
					DvoErrorPhotometric* costPhotometric = 0; 
					
					if (cur_i == 0){
					
					  // ### add brightness constancy residual
					  
					  costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);  
					  
					
					}
					else{
					  
					  // ### here: take data from grayCur_array and depthCur_array
					  // ###
					  // ###
					  
					  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i - 1].data; 
					  const float * _p_grayCur_array  = (const float*)grayCur_array[cur_i - 1].data; 
					  
					  costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						_p_grayCur_array, _p_depthCur_array,
						intrinsics, x, y); 
					  
					  
					}
					
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);
                                        
                                           
                                        /*
                                        DvoErrorAuto* costPhotometric = new DvoErrorAuto(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        */
                                        
                                        //ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorAuto, 1, 6>(costPhotometric); 
                                        
					double residual;
                                        
					bool eval = costFunction->Evaluate(&(parameters[pose_correction + n_segm]), &residual, 0);
                                        
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, 
								lossFunctionBrightnessScaled, poses_[pose_correction + n_segm].data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					
					// ### add depth variation residual or projective ICP
					
			
#if 0               			// the common depth variation term with bicubic interpolation                         
                                            
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
#endif                                            
#if 0               			// interpolation with accounting for the weights
                                        
                                        DvoErrorAutoDepth* costDepthVariation = new DvoErrorAutoDepth(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorAutoDepth, 1, 6>(costDepthVariation);
#endif                                        
#if 1
					int r = -1, c = -1; 
					
					if (CORRESP[cur_i](y, x) != -1){
					  r = trunc( (int) CORRESP[cur_i](y, x) / w); 
					  c = (int) CORRESP[cur_i](y, x) % w; 
					}

					float* ptrNormals = (float*)normals[lvl].data; 
					size_t off = (y*w + x) * 3; 
					
					
					DvoErrorDepthICP* costDepthVariationICP = 0; 
					
					
					if (cur_i == 0){
					
					costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					
					}
					else{
					  
					  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i - 1].data; 
					  const float * _p_grayCur_array  = (const float*)grayCur_array[cur_i - 1].data; 
					  
					  costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						(const float*)grayRef.data, (const float*)depthRef.data,
						_p_depthCur_array, _p_depthCur_array,
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					    
					}  
					  
					  
					ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthICP, 1, 6>(costDepthVariationICP);
#endif
                                            
                                        double residualDepth; 
                                        
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[pose_correction + n_segm]), &residualDepth, 0); 
					
					if (evalDepth) eval_depth++; 
					if (residualDepth != INVALID_RESIDUAL) n_invalid_residual++; 
					
					//const float* dd_ = (const float*)depthRef.data;
					
					
					if (evalDepth && (residualDepth != INVALID_RESIDUAL) && (CORRESP[cur_i](y, x) != -1)  ) // && ( dd_[y*w + x] != 0)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, 
									lossFunctionDepthScaled, poses_[pose_correction + n_segm].data()); 
							
							//num_correct++; 
							
							
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
							
							//num_wrong++; 
							
							
						}
						delete costFunctionDepth;
					}

				    
				}
				
			}
			
			
			
			} // ### end n_current_frames
			
			
			
			/*
			cout << "num_wrong: " << num_wrong << endl; 
			cout << "num_correct: " << num_correct << endl; 
			cout << "eval_depth: " << eval_depth << endl; 
			cout << "valid_residual: " << n_invalid_residual << endl; 
			*/
			
			// ### regularization residuals
			
	
	
			
			
	
			auto add_regularizer = [](std::vector<ceres::ResidualBlockId> &residualsBlocks, 
						  size_t r_block_index, 
						  double _gamma_, 
						  std::vector<double*> &pose_combination, 
						  ceres::Problem &problem, 
#if weighted		     
						  DvoErrorRegularizationWeighted* costDvoErrorRegularization,
#else
						  DvoErrorRegularization* costDvoErrorRegularization,
#endif
						  ceres::CostFunction* costFunctionReg, 
						  ceres::LossFunction* lossFunctionReg, 
						  ceres::LossFunction* lossFunctionRegScaled
 						)
			{
			  
			  
			  assert( pose_combination.size() == 2 ); 
			   
			  size_t current_index = r_block_index; 
			  
			  lossFunctionRegScaled = new ceres::ScaledLoss(lossFunctionReg, _gamma_, ceres::TAKE_OWNERSHIP); 
#if weighted			
			  costDvoErrorRegularization = new DvoErrorRegularizationWeighted(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularizationWeighted, 6, 6, 6, 1>(costDvoErrorRegularization); 
#else
			  costDvoErrorRegularization = new DvoErrorRegularization(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularization, 6, 6, 6>(costDvoErrorRegularization); 
#endif
			  
			  vector<double> residualReg(6);                 
			  bool evalReg = costFunctionReg->Evaluate(&(pose_combination[0]), residualReg.data(), 0); 
			  
			  //cout << "reg residuals: " << residualReg[0] << " " << residualReg[1] << " " << residualReg[2] << " "  
			  //<< residualReg[3] << " " << residualReg[4] << " " << residualReg[5] << endl; 
			  //cout << evalReg << endl; 
			  
			  if ( evalReg && (residualReg[0] != INVALID_RESIDUAL) ) // 
			  {
			  if (!residualsBlocks[current_index])
			  {
				residualsBlocks[current_index] = 
				problem.AddResidualBlock(costFunctionReg, 
				lossFunctionRegScaled, pose_combination);   
			  }
			  }
			  else
			  {
			  if (residualsBlocks[current_index])
			  {
				problem.RemoveResidualBlock(residualsBlocks[current_index]);
				residualsBlocks[current_index] = 0;
			  }
			  delete costFunctionReg; 
			  }
			    
			};
			
			
			uint num_combinations = regularizer_num_pairs; 
			
			
			
			// ### here -> push additionally the weights into the pose combination
			
			auto build_pose_combinations = [](std::vector<std::vector<double*> > &pose_combinations, 
						     std::vector<double*> &parameters, 
						     Eigen::SparseMatrix<int, Eigen::RowMajor> &adjm, 
						     int num_segments_ 
						    ){
			  
			  assert(num_segments_ == adjm.rows()); 
			  
			  int n_combinations = adjm.sum(); 
			  int current_comb = 0; 
			  
			  
			  for (int i = 0; i < adjm.outerSize(); i++)
			    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
			      
			      assert(it.value() == 1); 
			      
			      int pose_index_A = it.row(); 
			      int pose_index_B = it.col(); 
			      int weight_index = num_segments_+ current_comb; 
			      
			      pose_combinations[current_comb].push_back(parameters[pose_index_A]); 
			      pose_combinations[current_comb].push_back(parameters[pose_index_B]); 
#if weighted			      
			      pose_combinations[current_comb].push_back(parameters[weight_index]); 
#endif			      
			      current_comb++; 
			      
			    }
			  
			  assert(current_comb == adjm.sum()); 
			  
			};
			
			
			std::vector<std::vector<double*> > pose_combinations(num_combinations); 
			
			
			// TODO check build_pose_combinations -> add weights for all pose combinations? (for other frame pairs)
			
			build_pose_combinations(pose_combinations, parameters, adjm, num_segments * n_current_frames); 
			
			
			
			//size_t index_basis = 2 * (size_t)w * (size_t)h; 
			size_t index_basis = 2 * (n_current_frames) * (size_t)w * (size_t)h; 
			
			
			
			
			
			for (uint i = 0; i < num_combinations; i++){
			  
			  // TODO: 
			  
			  size_t current_index = index_basis + (size_t)i; 

			  
			  //cout <<  index_basis  << endl; 
			  //cout << (size_t)i << endl; 
			  //cout << numResiduals << endl; 
			  
#if weighted
			  DvoErrorRegularizationWeighted* costDvoErrorRegularization; 
#else		  
			  DvoErrorRegularization* costDvoErrorRegularization; 
#endif	
			  ceres::CostFunction* costFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionRegScaled = 0; 
			  
			  
			  cout << "ITERATION: " << itr  << endl; 
			  
			  add_regularizer(residualsBlocks, 
					current_index, 
					conf._gamma * gamma_normalizer, 
					pose_combinations[i], 
					problem, 
					costDvoErrorRegularization, 
					//weights.data(), 
					//i, 
					costFunctionReg, 
					lossFunctionReg, 
					lossFunctionRegScaled); 
					
			}	// for all combinations
		  
		  
		  
			// ### here: add the new regularizer \sum_(1 - w^2)^2
			
			
#if weighted			
			
			//size_t index_W_shift = 2 * (size_t)w * (size_t)h + regularizer_num_pairs; 
			size_t index_W_shift = 2 * n_current_frames * (size_t)w * (size_t)h + regularizer_num_pairs; 
			
			//////
			
			double zeta = 1.0; 
			double zeta_normalizer 	= 1.0f / (double) regularizer_num_pairs; 
			
			
			
			for (uint i = 0; i < num_combinations; i++){
			
			DR_Robust_Optimizer_Error* cost_DR_Robust_Optimizer_Error; 
			ceres::CostFunction* costFunctionRO = 0; 
			ceres::LossFunction* lossFunctionRO = 0; 
			ceres::LossFunction* lossFunctionROScaled = 0; 
			  
			
			//num_segments * n_current_frames
			
			size_t w_index = num_segments * n_current_frames + (size_t)i; 			// shift of the weights in the parameter array
			size_t res_shift = index_W_shift + i; 
			lossFunctionROScaled = new ceres::ScaledLoss(lossFunctionRO, conf._zeta * zeta_normalizer, ceres::TAKE_OWNERSHIP); 
			cost_DR_Robust_Optimizer_Error = new DR_Robust_Optimizer_Error(); 
			
			costFunctionRO = 
			new ceres::AutoDiffCostFunction<DR_Robust_Optimizer_Error, 1, 1>(cost_DR_Robust_Optimizer_Error); 
			
			double residualRegW;                 
			bool evalRegW = costFunctionRO->Evaluate(&(parameters[w_index]), &residualRegW, 0); 
			
			//cout << evalRegW << endl; 
			//cout << residualRegW << endl; 
			
			if ( evalRegW && (residualRegW != INVALID_RESIDUAL) ) // 
			{
			if (!residualsBlocks[res_shift])
			{
				residualsBlocks[res_shift] = problem.AddResidualBlock(costFunctionRO, lossFunctionROScaled, &weights[i]); 
			}
			}
			else
			{
			if (residualsBlocks[res_shift])
			{
				problem.RemoveResidualBlock(residualsBlocks[res_shift]); 
				residualsBlocks[res_shift] = 0;
			}
			delete costFunctionRO; 
			}   
			 
			};
			
			
#endif			
			
			
			
			
			
			//////
			
			
			
			// ###
		
		
		
			int numValidResidualsBefore = problem.NumResidualBlocks(); 
			if (numValidResidualsBefore == 0)
				continue;

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = conf._ceres_max_iterations;   				//                            
			options.num_threads = 8;
			options.num_linear_solver_threads = 8;
			//options.dogleg_type = ceres::SUBSPACE_DOGLEG; 
			//options.use_nonmonotonic_steps = true; 
			//options.line_search_type = ceres::WOLFE; 
			options.line_search_direction_type = ceres::BFGS; 
			//options.function_tolerance = 1e-20;
			//options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_SCHUR;		//
			//options.linear_solver_type = ceres::ITERATIVE_SCHUR;		// much much faster...
			options.linear_solver_type = ceres::CGNR;			// much much faster... pcg
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;
			
			SuccessfulStepCallbackL successfulStepCb;
			options.callbacks.push_back(&successfulStepCb);
			
			ceres::Solver::Summary summary;
			cout << "start solving..." << endl; 
			
			
			
			ceres::Solve(options, &problem, &summary);
			
			/*
			auto t2 = chrono::high_resolution_clock::now();
			cout << "-> non-rigid point cloud registration ready" << endl << "  -> elapsed time: " <<
			chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms" << endl << endl;
			*/
			
			
			if (debug)
				std::cout << summary.FullReport() << std::endl; 
			
			std::cout << std::endl; 
			std::cout << "initial cost: " << summary.initial_cost << std::endl; 
			std::cout << "final cost: " << summary.final_cost << std::endl; 
			std::cout << "fixed cost: " << summary.fixed_cost << std::endl; 
			std::cout << std::endl; 
			
			
			double error = summary.initial_cost / summary.final_cost; 
			
			if ( earlyBreak && ( error < (1.0/convergenceErrorRatio)) ){ 
			  break;
			}
			
			std::cout << std::endl; 
			
		}
  
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                std::cout << std::endl; 
		
  }
  
  // post-processing
  tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
  std::cout << "runtime: " << tmr << std::endl;
  
  // final poses
  for (uint i = 0; i < num_segments; i++)
    std::cout << "pose " << i << " final = " << poses_[i].transpose() << std::endl;
  
  return 0; 
  
}

// ###





// ### global optimization : "all to all"

/*

int DynamicReconstructorL::define_and_solve_global_all_to_all_nlo_problem(){
  
  double tmr = (double)cv::getTickCount();
  
  for (int lvl = maxLvl; lvl >= minLvl; --lvl)
  {
		double tmrLvl = (double)cv::getTickCount(); 
		
		// fetch data for the current level
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		grayCur = grayCurPyr[lvl];
		depthCur = depthCurPyr[lvl];
		
		// ### here: take the image and depth values saved in grayCur_array and depthCur_array
		// ### for now: only the finest level
		
		uint n_current_frames = grayCur_array.size() + 1; 
		
		
		// ###
		
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) }; 
  
		
                
		// shortcuts
		const int w = grayCur.cols;
		const int h = grayCur.rows;
    
#define weighted 1
		
#if weighted		
                // ### 2wh for brightness constancy and projective ICP; regularizer_num_pairs for every regularizer pair of poses
		const size_t numResiduals = 2 * n_current_frames * (size_t)w * (size_t)h + (size_t)(2 * regularizer_num_pairs); 	// 2 * 
#else
		const size_t numResiduals = 2 * n_current_frames * (size_t)w * (size_t)h + (size_t)regularizer_num_pairs; 
#endif
		
		
		double alpha_normalizer = 1.0f / (double) (w * h); 
		double beta_normalizer 	= 1.0f / (double) (w * h); 
		double gamma_normalizer = 1.0f / (double) regularizer_num_pairs; 
		
		//double alpha_normalizer = 1.0f; 
		//double beta_normalizer = 1.0f; 
		//double gamma_normalizer = 1.0f; 
		
		//cout << parameters.size() << endl; 
		//cout << regularizer_num_pairs << endl; 
		//char c; cin >> c; 
		
		
		double errorAvgLast = std::numeric_limits<double>::max();  
		
		//cout << "poses_.size(): " << poses_.size() << endl; 
		//cout << "num_segments: " << num_segments << endl; 
		
		
		
		for (int itr = 0; itr < maxIterations; ++itr)
		{
		  
		  
		  // ###
		  
		  
		  
		  // ### loss function for the brightness constancy
		ceres::LossFunction* lossFunction = 0;
		if (useHuber)
			lossFunction = new ceres::HuberLoss(50.0f / 255.0f);              // 50 grayscale steps 
                

                ceres::LossFunction* lossFunctionBrightnessScaled = 0;
                lossFunctionBrightnessScaled = new ceres::ScaledLoss(lossFunction, conf._alpha * alpha_normalizer, ceres::TAKE_OWNERSHIP); 

                // ### a scaled loss function for the depth variance         
                ceres::LossFunction* lossFunctionDepth = 0;
		if (useHuber){
			lossFunctionDepth = new ceres::HuberLoss(10.0f / 100.0f);          // 10 cm   
                }
                
                ceres::LossFunction* lossFunctionDepthScaled = 0;
                lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, conf._beta * beta_normalizer, ceres::TAKE_OWNERSHIP); 
                
		
		
		    // instantiate problem
		    ceres::Problem problem; 
		  
		    // ### a "reference" is transformed to the "current" frame
		
		    std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		    std::vector<double*> parameters;
		
		    
		
		    for (uint i = 0; i < num_segments * n_current_frames; i++)
		      parameters.push_back(poses_[i].data()); 
		
		    
		    // ### push additionally the weights
		    
		    for (uint i = 0; i < regularizer_num_pairs; i++)
		      parameters.push_back(&weights[i]); 
		    
		    
		    
		    
		    // ### 
		  
		  
		       
		  
		    
		      
		      
		      
			std::cout << "   iteration " << itr << ":" << std::endl; 
                        
			Eigen::MatrixXi CORRESP[n_current_frames]; 
			
			
			for (uint cur_i = 0; cur_i < n_current_frames; cur_i++)
			{
			
			
			for (uint i = 0; i < num_segments * n_current_frames; i++){
			  cout << "pose " << i << ":: " << poses_[i].transpose() << endl;   
			}
			
			
			
			// ### here: change the pointers accordingly
			
			
			
			// TODO poses_ -> pass accordingly, a correct subset of poses 
			
			// TODO
			
			//if (cur_i == 0){}
			
			
			
			
			//vector<Eigen::Matrix<double, 6, 1>> *ref = &poses_[num_segments * cur_i]; 
			
			// to extract a subvector is the best solution
			
			
			if (cur_i == 0){
			  
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin();
			  vector<Eigen::Matrix<double, 6, 1>>::const_iterator last = poses_.begin() + num_segments;
			  vector<Eigen::Matrix<double, 6, 1>> pose_subset_(first, last);
			  
			  
			  update_correspondences(w, h, (const float*)depthRef.data, (const float*)depthCur.data, 
					       pose_subset_, CORRESP[cur_i], intrinsics, segmentations[lvl]); 
			  
			}
			else{
			  
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin() + num_segments * ( cur_i);
			  vector< Eigen::Matrix<double, 6, 1> >::const_iterator last = poses_.begin() + num_segments * (cur_i + 1);
			  vector< Eigen::Matrix<double, 6, 1> > pose_subset_(first, last);
			  
			  
			  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i-1].data; 
			  
			  
			  update_correspondences(w, h, (const float*)depthRef.data, _p_depthCur_array, 
					       pose_subset_, CORRESP[cur_i], intrinsics, segmentations[lvl]); 
			  
			}
			
			//&(poses_[num_segments * cur_i]),
			
			
			// ###
			
			uint num_wrong = 0; 
			uint num_correct = 0; 
			uint eval_depth = 0; 
			uint n_invalid_residual = 0; 
			
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
				  
					const size_t id_correction = 2 * cur_i * (size_t)w * (size_t)h; 
					
					const size_t id 	= id_correction + 	(size_t)(y*w + x); 
					const size_t id_depth 	= id_correction +	(size_t)(w*h + y*w + x); 
					
					//const size_t id_depth_correction 	= cur_i * n_current_frames; 
					
					const size_t pose_correction = num_segments * cur_i; 
					
					int n_segm = segmentations[lvl].at<int>(y, x); 
					
					
					if (n_segm == -1){
					  residualsBlocks[id] = 0; 
					  residualsBlocks[id_depth] = 0; 
					  continue; 
					}
					
					// TODO place all current frames to a single array or
					// TODO add another block...
					// TODO make grayCur an array for all residuals?..
					
					
					
					
					
					
					
					DvoErrorPhotometric* costPhotometric = 0; 
					
					if (cur_i == 0){
					
					  // ### add brightness constancy residual
					  
					  costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);  
					  
					
					}
					else{
					  
					  // ### here: take data from grayCur_array and depthCur_array
					  // ###
					  // ###
					  
					  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i - 1].data; 
					  const float * _p_grayCur_array  = (const float*)grayCur_array[cur_i - 1].data; 
					  
					  costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						_p_grayCur_array, _p_depthCur_array,
						intrinsics, x, y); 
					  
					  
					}
					
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);
                                        
                                           
                                        
                                        //DvoErrorAuto* costPhotometric = new DvoErrorAuto(
					//	grayRef, depthRef, 
					//	grayCur, depthCur, 
					//	kPyr[lvl], x, y); 
                                        
                                        
                                        
                                        //ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorAuto, 1, 6>(costPhotometric); 
                                        
					double residual;
                                        
					bool eval = costFunction->Evaluate(&(parameters[pose_correction + n_segm]), &residual, 0);
                                        
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, 
								lossFunctionBrightnessScaled, poses_[pose_correction + n_segm].data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					
					// ### add depth variation residual or projective ICP
					
			
#if 0               			// the common depth variation term with bicubic interpolation                         
                                            
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
#endif                                            
#if 0               			// interpolation with accounting for the weights
                                        
                                        DvoErrorAutoDepth* costDepthVariation = new DvoErrorAutoDepth(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorAutoDepth, 1, 6>(costDepthVariation);
#endif                                        
#if 1
					int r = -1, c = -1; 
					
					if (CORRESP[cur_i](y, x) != -1){
					  r = trunc( (int) CORRESP[cur_i](y, x) / w); 
					  c = (int) CORRESP[cur_i](y, x) % w; 
					}

					float* ptrNormals = (float*)normals[lvl].data; 
					size_t off = (y*w + x) * 3; 
					
					
					DvoErrorDepthICP* costDepthVariationICP = 0; 
					
					
					if (cur_i == 0){
					
					costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					
					}
					else{
					  
					  const float * _p_depthCur_array = (const float*)depthCur_array[cur_i - 1].data; 
					  const float * _p_grayCur_array  = (const float*)grayCur_array[cur_i - 1].data; 
					  
					  costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						(const float*)grayRef.data, (const float*)depthRef.data,
						_p_depthCur_array, _p_depthCur_array,
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					    
					}  
					  
					  
					ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthICP, 1, 6>(costDepthVariationICP);
#endif
                                            
                                        double residualDepth; 
                                        
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[pose_correction + n_segm]), &residualDepth, 0); 
					
					if (evalDepth) eval_depth++; 
					if (residualDepth != INVALID_RESIDUAL) n_invalid_residual++; 
					
					//const float* dd_ = (const float*)depthRef.data;
					
					
					if (evalDepth && (residualDepth != INVALID_RESIDUAL) && (CORRESP[cur_i](y, x) != -1)  ) // && ( dd_[y*w + x] != 0)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, 
									lossFunctionDepthScaled, poses_[pose_correction + n_segm].data()); 
							
							//num_correct++; 
							
							
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
							
							//num_wrong++; 
							
							
						}
						delete costFunctionDepth;
					}

				    
				}
				
			}
			
			
			
			} // ### end n_current_frames
			
			
			
			
			// cout << "num_wrong: " << num_wrong << endl; 
			// cout << "num_correct: " << num_correct << endl; 
			// cout << "eval_depth: " << eval_depth << endl; 
			// cout << "valid_residual: " << n_invalid_residual << endl; 
			
			
			// ### regularization residuals
			
	
	
			
			
	
			auto add_regularizer = [](std::vector<ceres::ResidualBlockId> &residualsBlocks, 
						  size_t r_block_index, 
						  double _gamma_, 
						  std::vector<double*> &pose_combination, 
						  ceres::Problem &problem, 
#if weighted		     
						  DvoErrorRegularizationWeighted* costDvoErrorRegularization,
#else
						  DvoErrorRegularization* costDvoErrorRegularization,
#endif
						  ceres::CostFunction* costFunctionReg, 
						  ceres::LossFunction* lossFunctionReg, 
						  ceres::LossFunction* lossFunctionRegScaled
 						)
			{
			  
			  
			  assert( pose_combination.size() == 2 ); 
			   
			  size_t current_index = r_block_index; 
			  
			  lossFunctionRegScaled = new ceres::ScaledLoss(lossFunctionReg, _gamma_, ceres::TAKE_OWNERSHIP); 
#if weighted			
			  costDvoErrorRegularization = new DvoErrorRegularizationWeighted(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularizationWeighted, 6, 6, 6, 1>(costDvoErrorRegularization); 
#else
			  costDvoErrorRegularization = new DvoErrorRegularization(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularization, 6, 6, 6>(costDvoErrorRegularization); 
#endif
			  
			  vector<double> residualReg(6);                 
			  bool evalReg = costFunctionReg->Evaluate(&(pose_combination[0]), residualReg.data(), 0); 
			  
			  //cout << "reg residuals: " << residualReg[0] << " " << residualReg[1] << " " << residualReg[2] << " "  
			  //<< residualReg[3] << " " << residualReg[4] << " " << residualReg[5] << endl; 
			  //cout << evalReg << endl; 
			  
			  if ( evalReg && (residualReg[0] != INVALID_RESIDUAL) ) // 
			  {
			  if (!residualsBlocks[current_index])
			  {
				residualsBlocks[current_index] = 
				problem.AddResidualBlock(costFunctionReg, 
				lossFunctionRegScaled, pose_combination);   
			  }
			  }
			  else
			  {
			  if (residualsBlocks[current_index])
			  {
				problem.RemoveResidualBlock(residualsBlocks[current_index]);
				residualsBlocks[current_index] = 0;
			  }
			  delete costFunctionReg; 
			  }
			    
			};
			
			
			uint num_combinations = regularizer_num_pairs; 
			
			
			
			// ### here -> push additionally the weights into the pose combination
			
			auto build_pose_combinations = [](std::vector<std::vector<double*> > &pose_combinations, 
						     std::vector<double*> &parameters, 
						     Eigen::SparseMatrix<int, Eigen::RowMajor> &adjm, 
						     int num_segments_ 
						    ){
			  
			  assert(num_segments_ == adjm.rows()); 
			  
			  int n_combinations = adjm.sum(); 
			  int current_comb = 0; 
			  
			  
			  for (int i = 0; i < adjm.outerSize(); i++)
			    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
			      
			      assert(it.value() == 1); 
			      
			      int pose_index_A = it.row(); 
			      int pose_index_B = it.col(); 
			      int weight_index = num_segments_+ current_comb; 
			      
			      pose_combinations[current_comb].push_back(parameters[pose_index_A]); 
			      pose_combinations[current_comb].push_back(parameters[pose_index_B]); 
#if weighted			      
			      pose_combinations[current_comb].push_back(parameters[weight_index]); 
#endif			      
			      current_comb++; 
			      
			    }
			  
			  assert(current_comb == adjm.sum()); 
			  
			};
			
			
			std::vector<std::vector<double*> > pose_combinations(num_combinations); 
			
			
			// TODO check build_pose_combinations -> add weights for all pose combinations? (for other frame pairs)
			
			build_pose_combinations(pose_combinations, parameters, adjm, num_segments * n_current_frames); 
			
			
			
			//size_t index_basis = 2 * (size_t)w * (size_t)h; 
			size_t index_basis = 2 * (n_current_frames) * (size_t)w * (size_t)h; 
			
			
			
			
			
			for (uint i = 0; i < num_combinations; i++){
			  
			  // TODO: 
			  
			  size_t current_index = index_basis + (size_t)i; 

			  
			  //cout <<  index_basis  << endl; 
			  //cout << (size_t)i << endl; 
			  //cout << numResiduals << endl; 
			  
#if weighted
			  DvoErrorRegularizationWeighted* costDvoErrorRegularization; 
#else		  
			  DvoErrorRegularization* costDvoErrorRegularization; 
#endif	
			  ceres::CostFunction* costFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionRegScaled = 0; 
			  
			  
			  cout << "ITERATION: " << itr  << endl; 
			  
			  add_regularizer(residualsBlocks, 
					current_index, 
					conf._gamma * gamma_normalizer, 
					pose_combinations[i], 
					problem, 
					costDvoErrorRegularization, 
					//weights.data(), 
					//i, 
					costFunctionReg, 
					lossFunctionReg, 
					lossFunctionRegScaled); 
					
			}	// for all combinations
		  
		  
		  
			// ### here: add the new regularizer \sum_(1 - w^2)^2
			
			
#if weighted			
			
			//size_t index_W_shift = 2 * (size_t)w * (size_t)h + regularizer_num_pairs; 
			size_t index_W_shift = 2 * n_current_frames * (size_t)w * (size_t)h + regularizer_num_pairs; 
			
			//////
			
			double zeta = 1.0; 
			double zeta_normalizer 	= 1.0f / (double) regularizer_num_pairs; 
			
			
			
			for (uint i = 0; i < num_combinations; i++){
			
			DR_Robust_Optimizer_Error* cost_DR_Robust_Optimizer_Error; 
			ceres::CostFunction* costFunctionRO = 0; 
			ceres::LossFunction* lossFunctionRO = 0; 
			ceres::LossFunction* lossFunctionROScaled = 0; 
			  
			
			//num_segments * n_current_frames
			
			size_t w_index = num_segments * n_current_frames + (size_t)i; 			// shift of the weights in the parameter array
			size_t res_shift = index_W_shift + i; 
			lossFunctionROScaled = new ceres::ScaledLoss(lossFunctionRO, conf._zeta * zeta_normalizer, ceres::TAKE_OWNERSHIP); 
			cost_DR_Robust_Optimizer_Error = new DR_Robust_Optimizer_Error(); 
			
			costFunctionRO = 
			new ceres::AutoDiffCostFunction<DR_Robust_Optimizer_Error, 1, 1>(cost_DR_Robust_Optimizer_Error); 
			
			double residualRegW;                 
			bool evalRegW = costFunctionRO->Evaluate(&(parameters[w_index]), &residualRegW, 0); 
			
			//cout << evalRegW << endl; 
			//cout << residualRegW << endl; 
			
			if ( evalRegW && (residualRegW != INVALID_RESIDUAL) ) // 
			{
			if (!residualsBlocks[res_shift])
			{
				residualsBlocks[res_shift] = problem.AddResidualBlock(costFunctionRO, lossFunctionROScaled, &weights[i]); 
			}
			}
			else
			{
			if (residualsBlocks[res_shift])
			{
				problem.RemoveResidualBlock(residualsBlocks[res_shift]); 
				residualsBlocks[res_shift] = 0;
			}
			delete costFunctionRO; 
			}   
			 
			};
			
			
#endif			
			
			
			
			
			
			//////
			
			
			
			// ###
		
		
		
			int numValidResidualsBefore = problem.NumResidualBlocks(); 
			if (numValidResidualsBefore == 0)
				continue;

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = conf._ceres_max_iterations;   				//                            
			options.num_threads = 8;
			options.num_linear_solver_threads = 8;
			//options.dogleg_type = ceres::SUBSPACE_DOGLEG; 
			//options.use_nonmonotonic_steps = true; 
			//options.line_search_type = ceres::WOLFE; 
			options.line_search_direction_type = ceres::BFGS; 
			//options.function_tolerance = 1e-20;
			//options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_SCHUR;		//
			//options.linear_solver_type = ceres::ITERATIVE_SCHUR;		// much much faster...
			options.linear_solver_type = ceres::CGNR;			// much much faster... pcg
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;
			
			SuccessfulStepCallback successfulStepCb;
			options.callbacks.push_back(&successfulStepCb);
			
			ceres::Solver::Summary summary;
			cout << "start solving..." << endl; 
			
			
			
			ceres::Solve(options, &problem, &summary);
			
			
			// auto t2 = chrono::high_resolution_clock::now();
			// cout << "-> non-rigid point cloud registration ready" << endl << "  -> elapsed time: " <<
			// chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms" << endl << endl;
			
			
			
			if (debug)
				std::cout << summary.FullReport() << std::endl; 
			
			std::cout << std::endl; 
			std::cout << "initial cost: " << summary.initial_cost << std::endl; 
			std::cout << "final cost: " << summary.final_cost << std::endl; 
			std::cout << "fixed cost: " << summary.fixed_cost << std::endl; 
			std::cout << std::endl; 
			
			
			double error = summary.initial_cost / summary.final_cost; 
			
			if ( earlyBreak && ( error < (1.0/convergenceErrorRatio)) ){ 
			  break;
			}
			
			std::cout << std::endl; 
			
		}
  
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                std::cout << std::endl; 
		
  }
  
  // post-processing
  tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
  std::cout << "runtime: " << tmr << std::endl;
  
  // final poses
  for (uint i = 0; i < num_segments; i++)
    std::cout << "pose " << i << " final = " << poses_[i].transpose() << std::endl;
  
  return 0; 
  
}


*/

// ###








// 1 - save all segments (only for small # of segments)
// 2 - save only the final registration result
// 
int DynamicReconstructorL::save_results(int save_type){
  
  Eigen::MatrixXd pc_cur, pc_ref_whole; 
  cv::Mat vertexMapCur, vertexMapRef_; 
  
  RgbdProcessing* rgbdProc_ = new RgbdProcessing(); 
  rgbdProc_->computeVertexMap(K, depthCur, vertexMapCur); 
  cv::Mat segment1(depthRef.rows, depthRef.cols, CV_8UC1, cv::Scalar(255)); 
  extract_point_cloud(pc_cur, segment1, vertexMapCur); 
  string cur_ply = "./RESULT/cur.ply"; 
  write_ply_file(cur_ply, pc_cur, &colors_current, {255, 0, 255}, 1); 
  
  // ### save reference as a whole
  
  rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef_); 
  extract_point_cloud(pc_ref_whole, segment1, vertexMapRef_); 
  string ref_ply_ = "./RESULT/ref_.ply";
  write_ply_file(ref_ply_, pc_ref_whole, &colors_reference, {0, 255, 255}, 1); 
  
  Eigen::MatrixXd ref_transformed_ = Eigen::MatrixXd::Zero(depthRef.rows * depthRef.cols, 3);   	// maximum all points
  uint segment_shift = 0; 
  Eigen::MatrixXi ColorsWhole = Eigen::MatrixXi(depthRef.rows * depthRef.cols, 3); 
  
  // ###
  
  save_type = conf._save_segments; 
  
  //if ( save_type == 1 ){
  
  for (uint s = 0; s < num_segments; s++){
      
        // ### translation
        Eigen::Vector3d translation = poses_[s].topRows<3>(); 
        
        Eigen::Vector3d v = poses_[s].bottomRows<3>(); 
        double angle = v.norm(); 
        Eigen::Vector3d axis = v.normalized(); 
	
	//sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        //axis[0] = v[0] / angle;
        //axis[1] = v[1] / angle;
        //axis[2] = v[2] / angle;
        
        Eigen::AngleAxisd axisangle(angle, axis); 
        Eigen::Matrix3d R = axisangle.toRotationMatrix(); 
        Eigen::MatrixXd pc_ref, pc_ref_rotated; 
	
        //RgbdProcessing* rgbdProc_ = new RgbdProcessing(); 
        cv::Mat vertexMapRef; 
        rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef);  
        //rgbdProc_->computeVertexMap(K, depthCur, vertexMapCur); 
	
	extract_point_cloud(pc_ref, segmentations, s, vertexMapRef); 
	       
	pc_ref_rotated = pc_ref * R.inverse(); 
	pc_ref_rotated = pc_ref_rotated.rowwise()  + translation.transpose(); 
	
	
	if ( save_type == 1 ){
	  string ref_ply, ref_rot_ply; 
	  stringstream ss;
	  ss.str("");
	  ss << "./RESULT/ref" << s << " .ply";
	  ref_ply = ss.str();
	
	  ss.str(""); 
	  ss << "./RESULT/ref_rot_segm" << s << " .ply"; 
	  ref_rot_ply = ss.str(); 
	
	  // TODO add an option: no colors, use texture or a single color
	  write_ply_file(ref_ply, pc_ref, &segment_colors[s], {0, 255, 255}, 1); 
	  write_ply_file(ref_rot_ply, pc_ref_rotated, &segment_colors[s], {255, 255, 0}, 1); 
	  std::cout << "written" << std::endl; 
	}
	else if ( save_type == 2){
	  
	  //cout << pc_ref_rotated.rows() << " " << pc_ref_rotated.cols() << endl; 
	  //cout << num_elements_per_segment[s] << endl;
	  
	  ref_transformed_.block(segment_shift, 0, num_elements_per_segment[s], 3) = pc_ref_rotated; 
	  ColorsWhole.block(segment_shift, 0, num_elements_per_segment[s], 3) = segment_colors[s]; 
	  segment_shift += num_elements_per_segment[s]; 
	  
	  
	}
  }
   
  if ( save_type == 2 ){
    
    Eigen::MatrixXd ref_transformed = ref_transformed_.block(0, 0, segment_shift, 3);       
    string ref_ply_transformed = "./RESULT/ref_transformed.ply";
    write_ply_file(ref_ply_transformed, ref_transformed, &ColorsWhole, {255, 255, 0}, 1);  
    
    
  }
  
  
  // ### if there are more current frames
  
  
  
  
  
  if (conf._optimization_type == 2){
      
      for (uint i = 0; i < depthCur_array.size(); i++){
	
	uint s_correction = num_segments * (i+1);    // TODO -> correction should be adjusted...
	
	Eigen::MatrixXd ref_transformed_ = Eigen::MatrixXd::Zero(depthRef.rows * depthRef.cols, 3);	// maximum all points
	uint segment_shift = 0; 
	Eigen::MatrixXi ColorsWhole = Eigen::MatrixXi(depthRef.rows * depthRef.cols, 3); 
	
	
	for (uint s = 0; s < num_segments; s++){
	  
	  // ### translation
	  Eigen::Vector3d translation = poses_[s_correction + s].topRows<3>(); 
        
	  Eigen::Vector3d v = poses_[s_correction + s].bottomRows<3>(); 
	  double angle = v.norm(); 
	  Eigen::Vector3d axis = v.normalized(); 
	
        
	  Eigen::AngleAxisd axisangle(angle, axis); 
	  Eigen::Matrix3d R = axisangle.toRotationMatrix(); 
	  Eigen::MatrixXd pc_ref, pc_ref_rotated; 
	  
	  
	  cv::Mat vertexMapRef; 
	  rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef);  
	  //rgbdProc_->computeVertexMap(K, depthCur, vertexMapCur); 
	
	  extract_point_cloud(pc_ref, segmentations, s, vertexMapRef); 
	       
	  pc_ref_rotated = pc_ref * R.inverse(); 
	  pc_ref_rotated = pc_ref_rotated.rowwise()  + translation.transpose(); 
	  
	  ref_transformed_.block(segment_shift, 0, num_elements_per_segment[s], 3) = pc_ref_rotated; 
	  ColorsWhole.block(segment_shift, 0, num_elements_per_segment[s], 3) = segment_colors[s]; 
	  segment_shift += num_elements_per_segment[s]; 
	  
	  
	  
	  
	}
	
	
	Eigen::MatrixXd ref_transformed = ref_transformed_.block(0, 0, segment_shift, 3); 
	stringstream ref_ply_transformed; 
	ref_ply_transformed.str(""); 
	ref_ply_transformed << "./RESULT/ref_transformed " << i << ".ply"; 
	write_ply_file(ref_ply_transformed.str(), ref_transformed, &ColorsWhole, {255, 255, 0}, 1); 
	
	
      }
      
      
      
    }
  
  
  
  
  
  // ### weights...
  
  cout << "weights: " << endl; 
  for (uint i = 0; i < weights.size(); i++)
    cout << weights[i] << endl; 
  cout << endl; 
  
  
  
  return 0; 
  
}



int DynamicReconstructorL::fetch_segment_colors(){
    
  int counter = 0; 
  
  cv::Mat colorCur = cv::imread(path_colorCur); 
  cv::Mat colorRef = cv::imread(path_colorRef); 
  Eigen::MatrixXi ColorsCurrent(width * height, 3); 
  Eigen::MatrixXi ColorsReference(width * height, 3); 
  counter = 0; 
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      cv::Vec3b color = colorCur.at<cv::Vec3b>(i, j); 
      cv::Vec3b colorR = colorRef.at<cv::Vec3b>(i, j); 
      ColorsCurrent(counter, 0) = color[2]; 
      ColorsCurrent(counter, 1) = color[1]; 
      ColorsCurrent(counter, 2) = color[0]; 
      ColorsReference(counter, 0) = colorR[2];
      ColorsReference(counter, 1) = colorR[1];
      ColorsReference(counter, 2) = colorR[0];
      counter++; 
    }
  
  colors_current = ColorsCurrent; 
  colors_reference = ColorsReference; 
  
  
  for (uint s = 0; s < num_segments; s++){
  
  // ### pre-compute colors?.. somewhere above
	// ### add Colors to the output...
	Eigen::MatrixXi ColorsBuffer(width * height, 3); 
	
	counter = 0; 
	
	// lambda: operates on  Eigen::MatrixXi &Colors, 
	//			vector<cv::Mat> segmentations, 
	//			uint s (segment), 
	//			path_colorRef
	auto fill_colors = [&]()
	{
	  
	  cv::Mat colorRef = cv::imread(path_colorRef); 
	  for (uint i = 0; i < height; i++)
	    for (uint j = 0; j < width; j++){
	       if ( segmentations[0].at<int>(i, j) == s ){ 
		cv::Vec3b color = colorRef.at<cv::Vec3b>(i, j); 
		ColorsBuffer(counter, 0) = color[2]; 
		ColorsBuffer(counter, 1) = color[1]; 
		ColorsBuffer(counter, 2) = color[0]; 
		counter++; 
	      }
	  }
	}; 
	
	fill_colors(); 
	Eigen::MatrixXi ColorsSegment = ColorsBuffer.block(0, 0, counter, 3); 
	segment_colors.push_back(ColorsSegment); 
  }  
  
  return 0; 
  
}



int DynamicReconstructorL::visualize_segment_neighbours(){
  
  // ### segmentations
  
  // ### required: adjm, segmentation mask, segments mask
  
  cout << height << endl; 
  cout << width << endl; 
  cout << num_segments << endl; 
  
  //char f; cin >> f; 
  
  vector<cv::Mat> pose_pairs_visualizations; 
  
  
  for (uint i = 0; i < num_segments; i++){
    cv::Mat visualization_current_segment(height, width, CV_8UC3, cv::Scalar(0,0,0)); 
    pose_pairs_visualizations.push_back(visualization_current_segment); 
  }
  
  // ### unrelated segments, current segment, segment pairs...
  
  vector<vector<int>> adjm_lines(num_segments); 
  
  
  for (int i = 0; i < adjm.outerSize(); i++)
    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
	assert(it.value() == 1); 
	int current_segment 	= it.row(); 
	int neighbour_segment 	= it.col(); 
	adjm_lines[current_segment].push_back(neighbour_segment); 
    }
  
  
  //for (uint i = )
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      uint segment_pp = segmentations[0].at<int>(i, j); 
      
      // color segment_pp pixel in green 
      cv::Vec3b &color = pose_pairs_visualizations[segment_pp].at<cv::Vec3b>(i, j);       
      color = {0, 255, 0}; 
      
      
      // determine, in which images the same pixel needs to be colored differently
      
      for (uint k = 0; k < adjm_lines.size(); k++){
	if ( (std::find(adjm_lines[k].begin(), adjm_lines[k].end(), segment_pp) != adjm_lines[k].end()) ){  
	  //color = {255, 0, 255}; 
	  cv::Vec3b &color2 = pose_pairs_visualizations[k].at<cv::Vec3b>(i, j);       
	  color2 = {0, 255, 255}; 
	}
	  
	  
      }
            
            
            
    }
  
  
  for (uint i = 0; i < num_segments; i++){
    
    stringstream ss; 
    ss.str(""); 
    ss << "./neighbours_vis/segment" << i << ".png"; 
    cv::imwrite(ss.str(), pose_pairs_visualizations[i]); 
    
  }
  
  
}






/*
int DynamicReconstructor::solve_nlo_problem(){
  return 0; 
}
*/




#endif