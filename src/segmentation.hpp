
#ifndef SEGMENTATION_H
#define SEGMENTATION_H

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/PolygonMesh.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/pca.h>

#include <iostream>
#include <string>
#include <sstream>


#include <Eigen/Dense>
#include "./ply_module/ply_module.hpp"

#include "./Felzenszwalb/segment-graph.h" 		// felzenswalb segmenter

typedef pcl::PointXYZRGB Point;
typedef pcl::PointCloud<Point> Cloud;
typedef pcl::PolygonMesh Mesh;
typedef pcl::Normal Normal;
typedef pcl::PointCloud<Normal> NormalCloud;
typedef Eigen::Vector3d Vector3d;

void mesh_to_points_normals(const Mesh::ConstPtr& mesh, Cloud::Ptr& cloud, NormalCloud::Ptr& normals) {
  // Unfold mesh
  pcl::fromPCLPointCloud2( mesh->cloud, *cloud );
  std::vector<int> counts(cloud->points.size(), 0);
  normals->points.resize(cloud->points.size());

  for (size_t i = 0; i < mesh->polygons.size(); i++) {
    const pcl::Vertices& vv = mesh->polygons[i];

    // Get the 3 points
    int i1 = vv.vertices[0];
    int i2 = vv.vertices[1];
    int i3 = vv.vertices[2];
    Point& p1 = cloud->points[i1];
    Point& p2 = cloud->points[i2];
    Point& p3 = cloud->points[i3];

    // Convert to eigen points
    Vector3d pe1(p1.x, p1.y, p1.z);
    Vector3d pe2(p2.x, p2.y, p2.z);
    Vector3d pe3(p3.x, p3.y, p3.z);

    // Find normal
    Vector3d normal = (pe2 - pe1).cross(pe3 - pe1);
    normal = normal / normal.norm();
    Normal pnormal( normal[0], normal[1], normal[2] );

    // smoothly blend with the old normal estimate at this point. Each face votes for the normal of vertices around it.
    float v;
    Normal a;
    a = normals->points[i1];
    v = 1.0 / (counts[i1] + 1.0);
    normals->points[i1] = Normal(v * pnormal.normal_x + (1.0 - v) * a.normal_x, v * pnormal.normal_y + (1.0 - v) * a.normal_y,
                   v * pnormal.normal_z + (1.0 - v) * a.normal_z);

    a = normals->points[i2];
    v = 1.0 / (counts[i2] + 1.0);
    normals->points[i2] = Normal(v * pnormal.normal_x + (1.0 - v) * a.normal_x, v * pnormal.normal_y + (1.0 - v) * a.normal_y,
                   v * pnormal.normal_z + (1.0 - v) * a.normal_z);

    a = normals->points[i3];
    v = 1.0 / (counts[i3] + 1.0);
    normals->points[i3] = Normal(v * pnormal.normal_x + (1.0 - v) * a.normal_x, v * pnormal.normal_y + (1.0 - v) * a.normal_y,
                   v * pnormal.normal_z + (1.0 - v) * a.normal_z);

    counts[i1]++; counts[i2]++; counts[i3]++;
  }
}



// return all edges, WITH duplicates
void getEdges(Mesh::Ptr mesh, std::vector<std::pair<int, int> >& e) {
  for (int i = 0; i < mesh->polygons.size(); i++) {
    const pcl::Vertices& v = mesh->polygons[i];
    int v1 = v.vertices[0];
    int v2 = v.vertices[1];
    int v3 = v.vertices[2];
    e.push_back(std::make_pair(v1, v2));
    e.push_back(std::make_pair(v1, v3));
    e.push_back(std::make_pair(v3, v2));
  }
}



std::vector<int> segment(const std::string& plyFile, const float kthr, const int minSegVerts) {
  // Load the geometry from .ply and extract the mesh, cloud, normals
  Mesh::Ptr sceneMesh(new Mesh);
  Cloud::Ptr sceneCloud(new Cloud);
  NormalCloud::Ptr sceneNormals(new NormalCloud);
  cout << "Loading file " << plyFile << endl;
  pcl::io::loadPolygonFile(plyFile, *sceneMesh);
  mesh_to_points_normals(sceneMesh, sceneCloud, sceneNormals);

  cout << "Constructing edge graph based on mesh connectivity..." << endl;
  size_t Npoly = sceneMesh->polygons.size();
  size_t N = sceneCloud->points.size();
  std::vector<std::pair<int, int> >es;
  getEdges(sceneMesh, es);
  size_t Ne = es.size();
  edge* edges = new edge[Ne];
  for (size_t i = 0; i < Ne; i++) {
    int a = es[i].first;
    int b = es[i].second;

    edges[i].a = a;
    edges[i].b = b;

    Normal& n1 = sceneNormals->points[a];
    Normal& n2 = sceneNormals->points[b];
    Point& p1 = sceneCloud->points[a];
    Point& p2 = sceneCloud->points[b];

    float dx = p2.x - p1.x;
    float dy = p2.y - p1.y;
    float dz = p2.z - p1.z;
    float dd = sqrt(dx * dx + dy * dy + dz * dz); dx /= dd; dy /= dd; dz /= dd;
    float dot = n1.normal_x * n2.normal_x + n1.normal_y * n2.normal_y + n1.normal_z * n2.normal_z;
    float dot2 = n2.normal_x * dx + n2.normal_y * dy + n2.normal_z * dz;
    float ww = 1.0 - dot;
    if (dot2 > 0) { ww = ww * ww; } // make it much less of a problem if convex regions have normal difference
    edges[i].w = ww;
  }
  cout << "made graph" << endl;

  // Segment!
  universe* u = segment_graph(static_cast<int>(N), static_cast<int>(Ne), edges, kthr);
  cout << "segmented" << endl;

  // Joining small segments
  for (int j = 0; j < Ne; j++) {
    int a = u->find(edges[j].a);
    int b = u->find(edges[j].b);
    if ((a != b) && ((u->size(a) < minSegVerts) || (u->size(b) < minSegVerts))) {
      u->join(a, b);
    }
  }

  // Return segment indices as vector
  std::vector<int> outComps(N);
  for (int q = 0; q < N; q++) {
    outComps[q] = u->find(q);
  }
  return outComps;
}



int visualize_segmentation(const std::vector<int> &comps, const std::string& plyFile, std::string output_file){
    
    // ### get access to vertices and segmentation results
    
    Mesh::Ptr sceneMesh(new Mesh);
    Cloud::Ptr sceneCloud(new Cloud);
    NormalCloud::Ptr sceneNormals(new NormalCloud);
    cout << "Loading file " << plyFile << endl;
    pcl::io::loadPolygonFile(plyFile, *sceneMesh);
    mesh_to_points_normals(sceneMesh, sceneCloud, sceneNormals);
    
    
    // ### write down the .ply with different colour values
    
    /*
    sceneCloud->points[0].x; 
    sceneCloud->points[0].r; 
    */
    
    uint num_points = sceneCloud->size(); 
    
    //char c; cin >> c; 
    
    Eigen::MatrixXd Vertices(num_points, 3); 
    Eigen::MatrixXi Colours = Eigen::MatrixXi::Zero(num_points, 3); 
    
    for (uint i = 0; i < num_points; i++){
        Vertices(i, 0) = sceneCloud->points[i].x; 
        Vertices(i, 1) = sceneCloud->points[i].y; 
        Vertices(i, 2) = sceneCloud->points[i].z; 
        
        // ### assign different color depending on the segment... -> compute colour uniformly from the segment id
        
        int r_ = comps[i] % 255; 
        int g_ = (comps[i] + 125) % 255;
        int b_ = (comps[i]) % 128; 
        
        Colours(i, 0) = r_; 
        Colours(i, 1) = g_; 
        Colours(i, 2) = b_; 
        
    }
    
    
    write_ply_file(output_file, Vertices, &Colours, {0, 255, 255}, 1); 
    cout << "done!.." << endl; 
    //char c; cin >> c; 
    
    
    
}

















#endif


