// Copyright 2016 Robert Maier, Technical University Munich
#include "app_test.h"

#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "rgbd_processing.h"
#include "tum_benchmark.h"


AppTest::AppTest() :
    tumBenchmark_(new TumBenchmark()),
    rgbdProc_(new RgbdProcessing())
{
}


AppTest::~AppTest()
{
    delete tumBenchmark_;
    delete rgbdProc_;
}


bool AppTest::run()
{
    // data folder
	std::string dataFolder = "C:/Users/rmaier/Documents/dev/projects/sceneopt/data/scene3d_fountain_all/";
	std::cout << "RGB-D data folder: " << dataFolder << std::endl;
	std::string assocFile = dataFolder + "rgbd_assoc.txt";
	std::cout << "RGB-D data assoc file: " << assocFile << std::endl;
	// cam intrinsics
	std::string camIntrinsicsFile = dataFolder + "../CamIntrinsics.txt";
	std::cout << "intrinsics file: " << camIntrinsicsFile << std::endl;
    
    // load camera intrinsics
    Eigen::Matrix3f K;
    if (!tumBenchmark_->loadCamIntrinsics(camIntrinsicsFile, K))
    {
        std::cout << "Camera intrinsics file ('" << camIntrinsicsFile << "') could not be loaded! Using defaults..." << std::endl;
        // default intrinsics
        K <<    525.0f, 0.0f, 319.5f,
        0.0f, 525.0f, 239.5f,
        0.0f, 0.0f, 1.0f;
    }
    //std::cout << "Intrinsics K " << std::endl << K << std::endl;
    
    // input data
	float maxDepth = 2.0f;
	int startFrame = 0;                 // start frame
    int endFrame = 0;                   // end frame (0 = all frames)
    
    // load file names
	std::vector<double> timestampsDepth;
	std::vector<double> timestampsColor;
    std::vector<std::string> filesDepth;
    std::vector<std::string> filesColor;
	if (!tumBenchmark_->loadAssoc(assocFile, timestampsDepth, filesDepth, timestampsColor, filesColor))
    {
        std::cout << "Assoc file ('" << assocFile << "') could not be loaded!" << std::endl;
        return false;
    }
    int numFrames = (int)filesDepth.size();
    if (endFrame == 0)
        endFrame = numFrames;

    // process frames
    bool canceled = false;
    int framesProcessed = 0;
    for (size_t i = startFrame; i < endFrame && !canceled; ++i)
    {
        // load input frame
        std::string fileColor = filesColor[i];
        std::string fileDepth = filesDepth[i];
        if (i % 50 == 0)
            std::cout << "File " << i << ": " << fileColor << ", " << fileDepth << std::endl;

        cv::Mat depth = tumBenchmark_->loadDepth(dataFolder + fileDepth);
        int w = depth.cols;
        int h = depth.rows;
        
        cv::Mat color = tumBenchmark_->loadColor(dataFolder + fileColor);
        if (color.empty())
            color = cv::Mat::zeros(h, w, CV_32FC3);
#if 1
        cv::imshow("depth", depth * 0.5f);
        cv::imshow("color", color);
#endif
		
        // threshold depth
        cv::Mat depthThres = depth.clone();
        if (maxDepth > 0.0f)
            rgbdProc_->threshold(depthThres, 0.0f, maxDepth);

        // compute normals
        cv::Mat vertexMap;
        rgbdProc_->computeVertexMap(K, depth, vertexMap);
        cv::Mat normals;
        rgbdProc_->computeNormals(vertexMap, normals);

        cv::Mat normalsVis;
        rgbdProc_->visualizeNormals(normals, normalsVis);
        cv::imshow("normals", normalsVis);
        
        char c = cv::waitKey(1);
        if (c == 27)
            canceled = true;
        
        framesProcessed++;
    }
    
    // close all opencv windows
    cv::destroyAllWindows();

    return true;
}
