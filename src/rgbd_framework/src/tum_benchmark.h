#ifndef TUM_BENCHMARK_H
#define TUM_BENCHMARK_H

#include <string>
#include <vector>
#ifndef WIN64
    #define EIGEN_DONT_ALIGN_STATICALLY
#endif
#include <Eigen/Dense>

#include <opencv2/core/core.hpp>


class TumBenchmark
{
public:
    TumBenchmark();
    ~TumBenchmark();

	bool loadAssoc(const std::string &assocFile,
		std::vector<double> &timestampsDepth, std::vector<std::string> &filesDepth,
		std::vector<double> &timestampsColor, std::vector<std::string> &filesColor) const;

    cv::Mat loadColor(const std::string &filename);
    cv::Mat loadColor_CV_32FC1(const std::string &filename);
    cv::Mat loadDepth(const std::string &filename);
    bool loadCamIntrinsics(const std::string &filename, Eigen::Matrix3d &K);

    bool savePoses(const std::string &filename,
                   const std::vector<Eigen::Matrix4f> &poses, const std::vector<double> &timestamps);
private:
};

#endif
