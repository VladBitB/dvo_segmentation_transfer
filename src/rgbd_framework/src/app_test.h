// Copyright 2016 Robert Maier, Technical University Munich
#ifndef APP_TEST_H
#define APP_TEST_H

#ifndef WIN64
    #define EIGEN_DONT_ALIGN_STATICALLY
#endif
#include <Eigen/Dense>

#include <opencv2/core/core.hpp>


class RgbdProcessing;
class TumBenchmark;


class AppTest
{
public:
    AppTest();
	~AppTest();

    bool run();

private:
    TumBenchmark* tumBenchmark_;
    RgbdProcessing* rgbdProc_;
};

#endif
