// Copyright 2016 Robert Maier, Technical University Munich
#include <iostream>

#ifndef WIN64
    #define EIGEN_DONT_ALIGN_STATICALLY
#endif
#include <Eigen/Dense>

#include "app_test.h"

#define STR1(x)  #x
#define STR(x)  STR1(x)


int main(int argc, char *argv[])
{
    // folders
    std::string sourceFolder = std::string(STR(APP_SOURCE_DIR));

    // run app
    AppTest fusion;
    fusion.run();
    
    return 0;
}
