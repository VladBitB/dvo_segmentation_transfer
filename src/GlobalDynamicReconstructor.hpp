
#ifndef GLOBALDYNAMICRECONSTRUCTOR_H
#define GLOBALDYNAMICRECONSTRUCTOR_H

#include "./main.hpp"
#include "./DynamicReconstructor.hpp"


class DynamicReconstructor{
  
  
public: 
  
  DynamicReconstructor(const string settings_file_) : settings_file(settings_file_), num_segments(0) {}; 
  
  ~DynamicReconstructor(){
    
    /*
     for (uint i = 0; i < n_frames; i++)
     {
    
     delete [] normals[i]; 
     delete [] vertexMapRefX[i]; 
    
     }
     */
    
    
  }; 
  
  int load_settings(); 
  int load_RGBD_data(); 
  int pre_processing(); 
  int compute_normals(int visualize); 
  int initialize_segmentation(); 
  int visualize_segments(); 
  int init_poses(); 
  int compute_adjacency_matrix(); 
  int define_and_solve_nlo_problem(); 
  int define_and_solve_global_nlo_problem(); 
  int define_and_solve_global_all_to_all_nlo_problem(); 
  int save_results(int save_type); 
  
  int reconstruct_every_segment(); 
  
  //int solve_nlo_problem(); 
  
  // group poses into several clusters, prepare poses and segments for integration into TSDF
  int post_regularizer(); 
  
  int fetch_segment_colors(); 
  
  int visualize_segment_neighbours(); 
  
  
  
  
private:   
  
  string settings_file; 
  conf_dvo_ceres conf; 		// settings 
  
  // parameters 		// 
  int numPyramidLevels;
  int maxLvl;
  int minLvl;
  int maxIterations;
  int useHuber;
  int earlyBreak;
  double convergenceErrorRatio;
  bool debug;
  
  cv::Mat grayRef, grayCur, depthRef, depthCur, orig_depthRef, orig_depthCur; 
  
  
  vector<cv::Mat> g_rgb_images;
  vector<cv::Mat> g_depths; 
  
  uint n_frames = 0; 
  
  uint reference_frame = 0; 
  
  
  // ### structures for global optimization
  vector<cv::Mat> grayCur_array; 
  vector<cv::Mat> depthCur_array; 
  
  Eigen::Matrix3d K; 
  std::vector<cv::Mat> grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr; 
  std::vector<Eigen::Matrix3d> kPyr; 
  
  
  std::vector< std::vector<cv::Mat> > grayPyr; 
  std::vector< std::vector<cv::Mat> > depthPyr; 
  
  
  
  vector< vector<cv::Mat> > normals; 		//[numPyramidLevels]; 
  vector< vector<cv::Mat> > vertexMapRefX; 	//[numPyramidLevels]; 
  
  // vector<cv::Mat> *normals_array; // current frames do not require normals
  
  
  std::vector<double> timestampsDepth;
  std::vector<double> timestampsColor;
  std::vector<std::string> filesDepth;
  std::vector<std::string> filesColor;
  
  std::string path_depthRef; 
  std::string path_depthCur; 
  std::string path_colorRef; 
  std::string path_colorCur; 
  
  uint num_segments; 
  
  vector<cv::Mat> segmentations; 
  
  int height, width; 
  
  vector<Eigen::Matrix<double, 6, 1>> poses_; 
  vector<double> weights; 
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> adjm; 
  int regularizer_num_pairs; 
  
  vector<Eigen::MatrixXi> segment_colors; // colors of the segmented reference
  
  Eigen::MatrixXi colors_current; 
  Eigen::MatrixXi colors_reference; 
  
  vector<Eigen::MatrixXi> frame_colors; 
  
  vector<std::string> paths_rgb; 
  vector<std::string> paths_depth; 
  
  
  
  
  vector<int> num_elements_per_segment; 
  
					  // add all parameters required for the reconstruction throughout the whole process
  
  
					  // methods load_settings, create energy function, fill residuals etc... 
  
  // instantiate problem
  // ceres::Problem *problem; 
  
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> frame_connectivity; 
  
  uint n_pose_sets = 0; 
  uint n_poses = 0; 
  
  
  // ### private functions
  
  int save_transformed_reference(vector<Eigen::Matrix<double, 6, 1>> current_poses, string filename); 
  
  
  
  protected:
  Eigen::MatrixXi ColorsReference;
}; 



int DynamicReconstructor::load_settings(){
  
  conf.load_dvo_ceres_settings(settings_file);
  
  if (conf.dump_dvo_ceres_settings_ == 1){
         cout << endl; 
         conf.dump_dvo_ceres_settings(); 
  }
  
  // set parameters
  numPyramidLevels = conf._num_levels; 
  maxLvl = numPyramidLevels - 1;
  minLvl = 0;
  maxIterations = conf._max_iterations; 
  useHuber = conf._use_Huber; 
  earlyBreak = conf._early_break; 
  convergenceErrorRatio = conf._convergenceErrorRatio; 
  debug = false;
  
  
  return 0; 
  
}





int DynamicReconstructor::load_RGBD_data(){
  
  std::cout << "load input RGB-D data..." << std::endl; 
  
  TumBenchmark* tumBenchmark_ = new TumBenchmark(); 
  if (!tumBenchmark_->loadCamIntrinsics(conf._intrinsics_file, K)){
            std::cout << "Camera intrinsics file ('" << conf._intrinsics_file << "') could not be loaded! Using defaults..." << std::endl;
            // default intrinsics
            K <<    525.0f, 0.0f, 319.5f,
                    0.0f, 525.0f, 239.5f,
                    0.0f, 0.0f, 1.0f;
  }
        
  std::cout << K << std::endl; 
       
  if (!tumBenchmark_->loadAssoc(conf._data_assoc_file, timestampsDepth, filesDepth, timestampsColor, filesColor)){
            std::cout << "Assoc file ('" << conf._data_assoc_file << "') could not be loaded!" << std::endl;
            return 1;
  }
  
  n_frames = filesColor.size(); 
    
  for (uint i = 0; i < n_frames; i++){
    
    cv::Mat color_measurement; 
    cv::Mat depth_measurement; 
      
    string path_c = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[i]; 
    string path_d = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[i]; 
      
    paths_rgb.push_back(path_c); 
    paths_depth.push_back(path_d); 
      
    color_measurement = tumBenchmark_->loadColor_CV_32FC1(path_c); 
    depth_measurement = tumBenchmark_->loadDepth(path_d); 
      
    g_rgb_images.push_back(color_measurement); 
    g_depths.push_back(depth_measurement); 
    
  }
  
  path_colorRef = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[reference_frame]; 
  
  cout << " [done]" << endl; 
  
  // ### set height and width of the finest level image
  
  height = g_depths[0].rows; 
  width  = g_depths[0].cols; 
  
  
  // std::vector<cv::Mat> grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr; 
  // cv::Mat grayRef, grayCur, depthRef, depthCur; 
  
  // not a deep copy of the reference frame
  grayRef 	= g_rgb_images[reference_frame]; 
  depthRef 	= g_depths[reference_frame];   
  
  
  return 0; 
  
}; 



int DynamicReconstructor::pre_processing(){ 
  
  grayPyr.resize(n_frames); 
  depthPyr.resize(n_frames); 
  
  for (uint i = 0; i < n_frames; i++){
    
    // ### Gaussian Blur 
    cv::Mat dst; 
    cv::GaussianBlur( g_rgb_images[i], dst, cv::Size( 7, 7), 0, 0 ); 
    g_rgb_images[i] = dst.clone(); 
    
    if (conf._depth_filter_type == AVG_FILTER){
            std::cout << "running wavg filter" << std::endl; 
            weighted_average_filter(g_depths[i]); 
    }
    
    if (conf._depth_filter_type ==  BILATERAL_FILTER){
      
	    cv::Mat orig_depth = g_depths[i].clone();
            std::cout << "running bilateralFilter" << std::endl; 
            bilateralFilter(orig_depth, g_depths[i], 0.5); 
    }
     
    if (conf._depth_filter_type == MEDIAN_FILTER){
            std::cout << "running median filter" << std::endl; 
            median_filter(g_depths[i]); 
    }
    
    grayPyr[i].push_back(g_rgb_images[i]); 
    depthPyr[i].push_back(g_depths[i]); 

  }
  
  
  
  std::cout << "downsample input ..." << std::endl; 
    
  kPyr.push_back(K);
  
  
  for (uint i = 0; i < depthPyr[0].size(); i++){
    depthRefPyr.push_back(depthPyr[0][i]);
    grayRefPyr.push_back(grayPyr[0][i]); 
  }
  
  
  // ATTENTION pyramids are not supported in the global optimization yet 
  //downsample(numPyramidLevels, kPyr, grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr); 
  
  return 0; 
  
}; 




int DynamicReconstructor::compute_normals(int visualize){
  
  // ### now: each frame has a set of normals
  // normals[l] -> a set of normals for the frame l
  // normals[l][i] -> all subsampled normals for the frame l
  
  normals.resize(n_frames); 
  vertexMapRefX.resize(n_frames); 
  
  for (uint l = 0; l < n_frames; l++)
  {
  
  normals[l].resize(numPyramidLevels);    		//= new cv::Mat[numPyramidLevels]; 
  vertexMapRefX[l].resize(numPyramidLevels); 		//= new cv::Mat[numPyramidLevels]; 
  
  RgbdProcessing* rgbdProc = new RgbdProcessing(); 
  
    for (uint i = 0; i < numPyramidLevels; i++){
    
      cout << "level: " << i << endl;     
      rgbdProc->computeVertexMap(kPyr[i], depthPyr[l][i], vertexMapRefX[l][i]); 
      rgbdProc->computeNormals(vertexMapRefX[l][i], normals[l][i], 5.0f);
      
      if (visualize == 1)
      {
	cv::Mat normalsVis; 
	rgbdProc->visualizeNormals(normals[l][i], normalsVis); 
	imshow("level ", normalsVis);
	cvWaitKey(0); 
      }
    
    }
  
  }
  
  
  return 0; 
  
};




int DynamicReconstructor::initialize_segmentation(){
  
  // ### segmentation must be performed on the reference frame
  // 
  
  
  
  if ( conf._enable_segments == NO_SEGMENTS ){ 
    
    num_segments = 1; 
    
    cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    
    // the whole segmentation is filled with 0, i.e., an index of the only segment
    
    segmentations.push_back(current_segmentation); 
    num_elements_per_segment.push_back(height * width); 
    
  }
  else if ( conf._enable_segments == TEST_SEGMENTS ){ 
    
    num_segments = 2; 
    
    cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (uint i = height / 2; i < height; i++)
      for (uint j = 0; j < width; j++)
	current_segmentation.at<int>(i, j) = 1;    
      
    // two segments are obtained in total, with indexes 0 and 1
    
    segmentations.push_back(current_segmentation); 
     
    num_elements_per_segment.push_back( (width * height) / 2);
    num_elements_per_segment.push_back( num_elements_per_segment[0] ); 
    
  }
  else if ( conf._enable_segments == REGULAR_SEGMENTS ){ 
    
    num_segments = (width / conf._tile_width ) *  ( height / conf._tile_height ); 
    
    auto regular_segmentation = [](vector<cv::Mat> &segmentations_, int w, int h, int segm_width, int segm_height)
	  {
	    
	    cv::Mat regular_segmentation(h, w, CV_32S, cv::Scalar(0, 0, 0)); 
	    
	    for (uint i = 0; i < h; i++)
	      for (uint j = 0; j < w; j++){
		
		int segm_displ_y = i / segm_height; 
		int segm_displ_x = j / segm_width; 
		int width_in_segments = w / segm_width; 
		int current_segment = segm_displ_y * width_in_segments + segm_displ_x; 
		regular_segmentation.at<int>(i, j) = current_segment; 
	      }
	      
	    segmentations_.push_back(regular_segmentation); 
	      
	  };
    
    regular_segmentation(segmentations, width, height, conf._tile_width, conf._tile_height); 
    
    for (uint i = 0; i < num_segments; i++)
      num_elements_per_segment.push_back(conf._tile_width * conf._tile_height); 
    
    
    //update_correspondences(w, h, (const float*)depthRef.data, (const float*)depthCur.data, 
    //					       poses_, CORRESP, intrinsics, segmentations[lvl]); 
    
    
    //cout << segmentations.size() << endl; 
    //cout << num_elements_per_segment.size() << endl; 
    //cout << 
    //char t; cin >> t; 
    
  }
  else if ( conf._enable_segments == SEGMENTS_ENABLED ){
    
	  string mesh_file_name = "./RESULT/MESH_REF.ply"; 
	  
	  // compute mesh from the first (reference) point cloud
	  // lookup maps (row, col) coordinate to a vertex in a mesh 
	  // lookup is required because not all vertices are included 
	  // into the mesh 
	  vector<int> lookup = compute_and_save_mesh(depthRef, K, mesh_file_name); 
	  
	  // segment the mesh using Felzenszwalb algorithm
	  const float kthr = 50; 							// graph cut k threshold 
	  const int minSegVerts = 500; 							// min number of points in a segment
	  const std::vector<int> comps = segment(mesh_file_name, kthr, minSegVerts); 
	  
	  // save a coloured ply segmentation
	  std::string coloured_file = "./RESULT/SEGMENTED.ply"; 
	  visualize_segmentation(comps, mesh_file_name, coloured_file); 
	  
	  cout << "mesh written and segmented..." << endl; 	  
	  cout << "computing segments" << endl; 
	  
	  // create segmentation masks 
	  // create a new segmentation mask associated with a number (index converter...) 
	  
	  std::map<int, uint> segm_key; 
	  for (uint i = 0; i < comps.size(); i++){ 
	    int current_value = comps[i]; 
	    if ( segm_key.insert(std::make_pair(current_value, num_segments)).second == true )
	      num_segments++; 
	  }
	  
	  num_elements_per_segment.reserve(num_segments); 
	  num_elements_per_segment.assign(num_segments, 0); 
	  
	  // ### remove all segments which are smaller than 10000 pixels...
	  for (uint i = 0; i < grayRefPyr[0].rows * grayRefPyr[0].cols; i++){			// TODO replace with width, heigh
	    int segment_id = segm_key.find(comps[lookup[i]])->second; 
	    num_elements_per_segment[segment_id]++; 
	  }
	  
	  uint num_elem_threshold = conf._num_elem_threshold; 
	  uint n_suitable_segments = 0; 

	  // ### reassign numbers to the segments sequentially...
	  vector<int> corrective_shift(num_segments, 0); 
	  
	  for (int i = 0; i < num_segments; i++){
	    if (num_elements_per_segment[i] > num_elem_threshold){
              corrective_shift[i] = n_suitable_segments;
	      n_suitable_segments++; 
	      }
	    else 
	      corrective_shift[i] = -1; 
	  }
	  
	  cout << "n_suitable_segments: " << n_suitable_segments << endl; 
	  num_segments = n_suitable_segments; 
	  
	  cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
	  
	  for (uint i = 0; i < grayRefPyr[0].rows * grayRefPyr[0].cols; i++){			// TODO replace with width, heigh
	    
	    if (lookup[i] != -1){
	     
	      uint c_segm = segm_key.find(comps[lookup[i]])->second; 
	      uint r = trunc( i / grayRefPyr[0].cols); 
	      uint c = i % grayRefPyr[0].cols; 
	         
	      if (num_elements_per_segment[c_segm] > num_elem_threshold)
	        current_segmentation.at<int>(r, c) = corrective_shift[c_segm]; 
	      else 
		current_segmentation.at<int>(r, c) = -1; 
	          
	    }
	    else{
	      uint r = trunc( i / grayRefPyr[0].cols); 
	      uint c = i % grayRefPyr[0].cols; 
	      current_segmentation.at<int>(r, c) = -1; 
	    }
	    
	  } 
	      
	  // ###
	  
	  segmentations.push_back(current_segmentation); 
	  
  }
  else if ( conf._enable_segments == SEGMENTS_SLIC ){
    
    /* Load the image and convert to Lab colour space. */
    const char* path_colorRef_ = path_colorRef.c_str(); 
    IplImage *image = cvLoadImage(path_colorRef_, 1); 
    IplImage *lab_image = cvCloneImage(image); 
    cvCvtColor(image, lab_image, CV_BGR2Lab); 
    
    /* Yield the number of superpixels and weight-factors from the user. */
    int w = image->width, h = image->height; 
    int nr_superpixels = conf._nr_superpixels; 
    int nc = conf._nc; 
    
    
    double step = sqrt((w * h) / (double) nr_superpixels); 
    
    /* Perform the SLIC superpixel algorithm. */
    Slic slic; 
    slic.generate_superpixels(lab_image, step, nc); 
    slic.create_connectivity(lab_image); 
    
    string save_path = "./slic_segmentation.png"; 
    slic.save_segmentation_DR(save_path, nr_superpixels); 
    
    // ### convert to supported representation + auxiliary structures
    
    cv::Mat slic_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    num_segments = slic.slic_convert(slic_segmentation, num_elements_per_segment); 
    segmentations.push_back(slic_segmentation); 
    
    
    
    /*
    for (uint r = 0; r < slic_segmentation.rows; r++){
      for (uint c = 0; c < slic_segmentation.cols; c++)
	cout << slic_segmentation.at<int>(r, c) << " ";
      cout << endl;  
    }
    */
    
    //cout << num_segments << endl; 
    
    //visualize_segments(); 
    
    //char c; cin >> c; 
    
    
    
  }
  
  // ### check if there are zero depth values 
  // ### check if there are segments with very small number of points
  // ### --> 
  
  /*
  
  const float* depth_data_ = (const float*)depthRef.data; 
  uint zero_counter = 0; 
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      float dRef = depth_data_[i*width + j]; 
      
      if ( dRef == 0 ) {
	zero_counter++; 
	segmentations[0].at<int>(i, j) = -1; 
	
      }
      
      
    }
  
  
  cout << "zero_counter " << zero_counter << endl; 
  
  
  // ### recompute segments...
  
  vector<int> new_segm_counter; 
  new_segm_counter.push_back(-1);
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      if ( !(std::find(new_segm_counter.begin(), new_segm_counter.end(), segmentations[0].at<int>(i, j)) != new_segm_counter.end()) )
        new_segm_counter.push_back(segmentations[0].at<int>(i, j));
      
    }
  
  cout << "new # of segments: " << new_segm_counter.size() - 1 << endl; 
  
  */
  
  //char c; cin >> c; 
  
  //num_segments = new_segm_counter.size() - 1; 
  
  // ### 
  
  
  
  // ### segmentation of the finest level accomplished, perform downsampling 
  auto downsample_segmentation = [](cv::Mat &segm){
    
    if (( (segm.rows % 2) != 0 ) || ( (segm.cols % 2) != 0)){
      cout << "not possible to downsample, not further divisible by 2..." << endl; 
      exit(0);
    }
    
    cv::Mat current_segmentation(segm.rows / 2, segm.cols / 2, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (int i = 0; i < segm.rows; i=i+2)
      for (int j = 0; j < segm.cols; j=j+2){
	current_segmentation.at<int>(i/2, j/2) = segm.at<int>(i, j); 
      }
    
    return current_segmentation; 
    
  };
  
  
  for (int lvl = 0; lvl < maxLvl; lvl++) {
    segmentations.push_back(downsample_segmentation(segmentations[lvl])); 
  }
  
  return 0; 
  
} 



int DynamicReconstructor::visualize_segments(){
	
	cout << "segmentations.size(): " << segmentations.size() << endl; 
	cout << "num_segments: " << num_segments << endl; 
	
	vector<cv::Mat> vizualizations; 
	visualize_segmentation(segmentations, vizualizations, num_segments); 
	cout << "visualization computed" << endl; 
	for (int i = 0; i < vizualizations.size(); i++){
	  imshow("", vizualizations[i]); 
	  cvWaitKey(0); 
	}
	
  return 0; 
	
}



int DynamicReconstructor::init_poses(){
  
  //uint n_current_frames =  grayCur_array.size() + 1; 
  //cout << n_current_frames << endl; 
  //char r; cin >> r; 
  
  // ### frame connectivity matrix
  // for 3 segments: build it manually for now...
  
  // frame connectivity matrix
  
  //frame_connectivity = Eigen::MatrixXi::Zero(n_frames, n_frames); 
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> frame_connectivity_(n_frames, n_frames); 
  
  for (uint i = 0; i < n_frames; i++)
    for (uint j = 0; j < n_frames; j++){
      if (j > i){
	//frame_connectivity_(i, j) = 1; 
	frame_connectivity_.insert(i, j) = 1; 
      }
    }
  
  frame_connectivity = frame_connectivity_; 
  n_pose_sets = frame_connectivity.sum(); 
  n_poses = num_segments * n_pose_sets; 
  
  poses_.resize(n_poses); 						// num_segments *  ( grayCur_array.size() + 1 ) 
  
  
  
   for (uint i = 0; i < n_poses; i++)
     poses_[i].setZero(); 
  
  
  // ### only for debugging
  /*
  for (uint i = 0; i < n_pose_sets; i++)
    for (uint j = 0; j < num_segments; j++){
      
      uint ind = i * num_segments + j; 
      
      poses_[ind][0] = i; 
      poses_[ind][1] = i; 
      poses_[ind][2] = i; 
      poses_[ind][3] = i; 
      poses_[ind][4] = i; 
      poses_[ind][5] = i; 
      
    }
  */
  
  
  // ### in case of the global optimization: a pose per current frame
  // ### place here two-frame things...
  // define a set of file names - settings
  
#if 1
  
  vector<Eigen::Matrix<double, 6, 1>> sequential_transformations_; 
  
  uint n_seq_trans = (g_depths.size() - 1 ) * num_segments; 
  sequential_transformations_.resize(n_seq_trans); 
  
  for (uint i = 0; i < n_seq_trans; i++)
    sequential_transformations_[i].setZero(); 
  
  cout << "START FRAME-TO-FRAME INITIALIZATION" << endl; 
  
  
  for (uint frame = 0; frame <  ( g_rgb_images.size() - 1 ); frame++)
  {
    
    DynamicReconstructorL dynamic_reconstructorL(""); 
    dynamic_reconstructorL.load_settings_and_data(g_depths[frame], 
						  g_rgb_images[frame],  
						  g_depths[frame + 1], 
						  g_rgb_images[frame + 1], 
						  segmentations[0], 
						  num_segments, 
						  K, 
						  conf
 						); 
    dynamic_reconstructorL.pre_processing(); 
    dynamic_reconstructorL.compute_normals(); 
    dynamic_reconstructorL.compute_adjacency_matrix(); 
    dynamic_reconstructorL.init_poses(); 
    dynamic_reconstructorL.define_and_solve_nlo_problem(); 
    vector<Eigen::Matrix<double, 6, 1>> current_poses_result; 
    current_poses_result = dynamic_reconstructorL.return_poses(); 
    
    
    uint sequential_shift = frame * num_segments; 
    
    for (uint segm = 0; segm < num_segments; segm++)
      sequential_transformations_[ sequential_shift + segm ] = current_poses_result[segm]; 
    
    cout << "g_rgb_images.size(): " << g_rgb_images.size() << endl; 
    cout << "frame: " << frame << endl; 
    cout << "frame + 1: " << frame + 1 << endl; 
    //cout << "current_poses_result.size(): " << current_poses_result.size() << endl; 
    //char c; cin >> c; 
    
  }
  
  
  auto return_chained = [](vector<Eigen::Matrix<double, 6, 1>> &ftf_transformations, uint num_segm, uint f1, uint f2){
      
      auto concat_poses = [](Eigen::Matrix<double, 6, 1> &pose23, Eigen::Matrix<double, 6, 1> &pose12){ 
    
    // if ( (v[0] == 0.0f) && (v[1] == 0.0f) && (v[2] == 0.0f) )   
    // R = Eigen::Matrix3d::Identity(3, 3);  
        
    // translation
    Eigen::Vector3d t23 = pose23.topRows<3>(); 
    Eigen::Vector3d t12 = pose12.topRows<3>(); 
    // rotation
    Eigen::Vector3d p23 = pose23.bottomRows<3>(); 
    Eigen::Vector3d p12 = pose12.bottomRows<3>(); 
    
    // rotation matrices
    Eigen::Matrix3d R23, R12, R13; 
    
    if (p23.norm() == 0.0f){
      R23 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_1 = p23.norm(); 
      Eigen::Vector3d axis_1 = p23.normalized(); 
      Eigen::AngleAxisd axisangle_1(angle_1, axis_1); 
      R23 = axisangle_1.toRotationMatrix(); 
    }
    
    if (p12.norm() == 0.0f){
      R12 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_2 = p12.norm(); 
      Eigen::Vector3d axis_2 = p12.normalized(); 
      Eigen::AngleAxisd axisangle_2(angle_2, axis_2); 
      R12 = axisangle_2.toRotationMatrix(); 
    }
    
    R13 = R23 * R12; 
    
    Eigen::AngleAxisd axisangle_result; 
    axisangle_result.fromRotationMatrix<Eigen::Matrix3d>(R13); 
    
    Eigen::Vector3d p13; 
    p13 = axisangle_result.axis() * axisangle_result.angle(); 
    
    
    
    Eigen::Vector3d t13 = t12 + t23; 
    
    Eigen::Matrix<double, 6, 1> pose13; 
    pose13.head(3) = t13; 
    pose13.tail(3) = p13; 
    
    
    
    return pose13; 
    
  }; 
      
      vector<Eigen::Matrix<double, 6, 1>> concat_transformations; 
      concat_transformations.resize(num_segm); 
      for (uint segm = 0; segm < num_segm; segm++)
	concat_transformations[segm].setZero(); 
      
      uint initial_shift = f2 * num_segm; 
      
      for (uint f = f1; f < f2; f++){
	
	uint pose_shift = f * num_segm; 
	
	// ### fetch subsequence of poses
	
	for (uint segm = 0; segm < num_segm; segm++)
	  concat_transformations[segm] = concat_poses(ftf_transformations[pose_shift + segm], concat_transformations[segm]); 

      }
      
      return concat_transformations; 
      
    };
  
  
  
    // ### find the correct index for saving in poses_
    // write to the global structure 

    
    uint global_correction = 0; 
    
    for (uint i = 0; i < g_depths.size(); i++)
      for (uint j = 0; j < g_depths.size(); j++){
	
	if (j > i)
	{
	  
	  vector<Eigen::Matrix<double, 6, 1>> chained = return_chained(sequential_transformations_, num_segments, i, j); 
	  
	  for (uint segm = 0; segm < num_segments; segm++)
	    poses_[global_correction + segm] = chained[segm]; 
	  
	  global_correction += num_segments; 
	  
	}
	
      }
    
  
  // ###
    
  // cout << "ALL POSES INITIALIZED" << endl; 
  // char c; cin >> c; 
 
 
#endif 
  
  return 0; 
  
}



int DynamicReconstructor::compute_adjacency_matrix(){
  
  Eigen::SparseMatrix<int, Eigen::RowMajor> adjm_(num_segments, num_segments); 
  adjacency_matrix(adjm_, segmentations, num_segments, conf._pose_pairs_per_segment, depthRef); 
  regularizer_num_pairs = adjm_.sum(); 
  cout << "regularizer_num_pairs = " << regularizer_num_pairs << endl; 
  adjm = adjm_; 
  //cout << adjm << endl; 
  
  if (weights.empty()){ 
    weights.resize(regularizer_num_pairs); 
    for (uint i = 0; i < regularizer_num_pairs; i++)
      weights[i] = 1.0f; 
  }
    
  //cout << weights.size() << endl; 
  //char t; cin >> t; 
  
}



struct SuccessfulStepCallback : public ceres::IterationCallback
	{
	public:
		SuccessfulStepCallback()
		{
		}

		virtual ceres::CallbackReturnType operator()(const ceres::IterationSummary& summary)
		{
			if (summary.iteration > 0 && summary.step_is_successful)
				return ceres::SOLVER_TERMINATE_SUCCESSFULLY;
			else
				return ceres::SOLVER_CONTINUE;
		}
	};







// ### global optimization : "all to all"

int DynamicReconstructor::define_and_solve_global_all_to_all_nlo_problem(){
  
  
  // ### debug information
  
  cout << "n_pose_sets: " << n_pose_sets 	<< endl; 
  cout << "regularizer_num_pairs " 		<< regularizer_num_pairs << endl; 
  cout << "n_poses " << n_poses 		<< endl; 
  cout << "frame_connectivity.sum() " 		<< endl << 
  frame_connectivity << endl; 
  //char t; cin >> t; 
  
  
  // ###
  
  double tmr = (double)cv::getTickCount();
  
  for (int lvl = maxLvl; lvl >= minLvl; --lvl)
  {
		double tmrLvl = (double)cv::getTickCount(); 
		
		// fetch data for the current level
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		
		//grayCur = grayCurPyr[lvl];
		//depthCur = depthCurPyr[lvl];
		
		// ### here: take the image and depth values saved in grayCur_array and depthCur_array
		// ### for now: only the finest level
		
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) }; 
		
		// shortcuts
		const int w = grayRef.cols;
		const int h = grayRef.rows;
		
		
		int num_chainings = frame_connectivity.sum() - ( frame_connectivity.rows() - 1); 
		
		
		//cout << num_chainings << endl; 
		
		//char f; cin >> f; 
		
#define weighted 1
		
#if weighted	
                // ### 2wh for brightness constancy and projective ICP; regularizer_num_pairs for every regularizer pair of poses
		const size_t numResiduals = 2 * n_pose_sets * (size_t)w * (size_t)h + (size_t)((n_pose_sets + 1) * regularizer_num_pairs) 
					    + num_chainings * num_segments; 	// 2 * 
#else
		const size_t numResiduals = 2 * n_pose_sets * (size_t)w * (size_t)h + (size_t)(n_pose_sets) * regularizer_num_pairs 
					    + num_chainings * num_segments; 
#endif
		
		double alpha_normalizer = 1.0f / (double) n_pose_sets * (w * h); 
		double beta_normalizer 	= 1.0f / (double) n_pose_sets * (w * h); 
		double gamma_normalizer = 1.0f / (double) regularizer_num_pairs * n_pose_sets; 
		
		//double alpha_normalizer = 1.0f; 
		//double beta_normalizer = 1.0f; 
		//double gamma_normalizer = 1.0f; 
		
		//cout << regularizer_num_pairs << endl; 
		//char c; cin >> c; 
		
		double errorAvgLast = std::numeric_limits<double>::max(); 

		
		for (int itr = 0; itr < maxIterations; ++itr)
		{
		  
		  
		  // ### loss function for the brightness constancy
		  ceres::LossFunction* lossFunction = 0;
		  if (useHuber)
			lossFunction = new ceres::HuberLoss(25.0f / 255.0f);              // 50 grayscale steps 
                

		  ceres::LossFunction* lossFunctionBrightnessScaled = 0;
		  lossFunctionBrightnessScaled = new ceres::ScaledLoss(lossFunction, conf._alpha * alpha_normalizer, ceres::TAKE_OWNERSHIP); 

		  // ### a scaled loss function for the depth variance         
		  ceres::LossFunction* lossFunctionDepth = 0;
		  if (useHuber){
			lossFunctionDepth = new ceres::HuberLoss(15.0f / 100.0f);          // 10 cm   
		  }
		  
		  ceres::LossFunction* lossFunctionDepthScaled = 0;
		  lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, conf._beta * beta_normalizer, ceres::TAKE_OWNERSHIP); 
		  
		  
		  // instantiate problem
		  ceres::Problem problem; 
		  
		  // ### a "reference" is transformed to the "current" frame
		  
		  std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		  std::vector<double*> parameters;
		    
		  
		  for (uint i = 0; i < n_poses; i++)
		    parameters.push_back(poses_[i].data()); 
		  
		  // ### push additionally the weights
		    
		  for (uint i = 0; i < regularizer_num_pairs; i++)
		    parameters.push_back(&weights[i]); 
		    
		      
		      
		  std::cout << "   iteration " << itr << ":" << std::endl; 
                        
		  
		  for (uint i = 0; i < n_poses; i++) 
		    cout << "pose " << i << ":: " << poses_[i].transpose() << endl; 
			
		  
		   uint cur_i = 0; 
			
			  
		    for (int q = 0; q < frame_connectivity.outerSize(); q++)
		      for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it_c(frame_connectivity, q); it_c; ++it_c){
			
			
			assert(it_c.value() == 1); 
			      
			int frame_A = it_c.row(); 
			int frame_B = it_c.col();   
			  
			vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin() + num_segments * cur_i; 
			vector< Eigen::Matrix<double, 6, 1> >::const_iterator last = poses_.begin() + num_segments * (cur_i + 1); 
			vector< Eigen::Matrix<double, 6, 1> > pose_subset_(first, last);
			  
			  
			const float * _p_depthRef_array = (const float*)g_depths[frame_A].data; 
			const float * _p_depthCur_array = (const float*)g_depths[frame_B].data; 
			  
			const float * _p_rgbRef_array = (const float*)g_rgb_images[frame_A].data; 
			const float * _p_rgbCur_array = (const float*)g_rgb_images[frame_B].data; 
			  
			  
			Eigen::MatrixXi CORRESP; 
			  
			update_correspondences(w, h, _p_depthRef_array, _p_depthCur_array, 
					       pose_subset_, CORRESP, intrinsics, segmentations[lvl]); 
			
			
			uint num_wrong = 0; 
			uint num_correct = 0; 
			uint eval_depth = 0; 
			uint n_invalid_residual = 0; 
			
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
				  
					const size_t id_correction = 2 * cur_i * (size_t)w * (size_t)h; 
					const size_t id 	= id_correction + 	(size_t)(y*w + x); 
					const size_t id_depth 	= id_correction +	(size_t)(w*h + y*w + x); 
					
					const size_t pose_correction = num_segments * cur_i; 
					
					int n_segm = segmentations[lvl].at<int>(y, x); 
					
					if (n_segm == -1){
					  //cout << " ######## n_segm == -1 ########" << endl; 
					  residualsBlocks[id] = 0; 
					  residualsBlocks[id_depth] = 0; 
					  continue; 
					}
					
					
					DvoErrorPhotometric* costPhotometric = 0; 
					
					  costPhotometric = new DvoErrorPhotometric(w, h,
						_p_rgbRef_array, _p_depthRef_array,
						_p_rgbCur_array, _p_depthCur_array,
						intrinsics, x, y); 
					
					  
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);

					
					double residual;
					bool eval = costFunction->Evaluate(&(parameters[pose_correction + n_segm]), &residual, 0);
					
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, 
								lossFunctionBrightnessScaled, poses_[pose_correction + n_segm].data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					
					// ### add depth variation residual or projective ICP
			
#if 0               			// the common depth variation term with bicubic interpolation                         
                                            
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
#endif                                            
#if 0               			// interpolation with accounting for the weights
                                        
                                        DvoErrorAutoDepth* costDepthVariation = new DvoErrorAutoDepth(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorAutoDepth, 1, 6>(costDepthVariation);
#endif                                        
#if 1
					int r = -1, c = -1; 
					
					if (CORRESP(y, x) != -1){
					  r = trunc( (int) CORRESP(y, x) / w); 
					  c = (int) CORRESP(y, x) % w; 
					}

					// normals[l] -> a set of normals for the frame l
					// normals[l][i] -> all subsampled normals for the frame l
					
					float* ptrNormals = (float*)normals[frame_A][lvl].data; 
					size_t off = (y*w + x) * 3; 
					
					DvoErrorDepthICP* costDepthVariationICP = 0; 
					
					costDepthVariationICP = new DvoErrorDepthICP(w, h, 
						_p_rgbRef_array, _p_depthRef_array, 
						_p_rgbCur_array, _p_depthCur_array, 
						intrinsics, x, y, c, r, ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]); 
					    
					
					ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthICP, 1, 6>(costDepthVariationICP);
#endif
                                            
                                        double residualDepth; 
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[pose_correction + n_segm]), &residualDepth, 0); 
					
					if (evalDepth) eval_depth++; 
					if (residualDepth != INVALID_RESIDUAL) n_invalid_residual++; 
					
					
					if (evalDepth && (residualDepth != INVALID_RESIDUAL) && (CORRESP(y, x) != -1)  ) // && ( dd_[y*w + x] != 0)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, 
									lossFunctionDepthScaled, poses_[pose_correction + n_segm].data()); 
							
							//num_correct++; 
							
							
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
							
							//num_wrong++; 
							
							
						}
						delete costFunctionDepth;
					}

				    
				}
				
			}
			
			
			
			cur_i++; 
			
			
			} // ### end frame_connectivity non-zero elements
			
			
			
			/*
			cout << "num_wrong: " << num_wrong << endl; 
			cout << "num_correct: " << num_correct << endl; 
			cout << "eval_depth: " << eval_depth << endl; 
			cout << "valid_residual: " << n_invalid_residual << endl; 
			*/
			
			// ### regularization residuals
			
			
			for (uint n_pose_combinations = 0; n_pose_combinations < n_pose_sets; n_pose_combinations++)
			{
			
			
			
			
			auto add_regularizer = [](std::vector<ceres::ResidualBlockId> &residualsBlocks, 
						  size_t r_block_index, 
						  double _gamma_, 
						  std::vector<double*> &pose_combination, 
						  ceres::Problem &problem, 
#if weighted		     
						  DvoErrorRegularizationWeighted* costDvoErrorRegularization,
#else
						  DvoErrorRegularization* costDvoErrorRegularization,
#endif
						  ceres::CostFunction* costFunctionReg, 
						  ceres::LossFunction* lossFunctionReg, 
						  ceres::LossFunction* lossFunctionRegScaled
 						)
			{
			  
			  
			  assert( pose_combination.size() == 2 ); 
			   
			  size_t current_index = r_block_index; 
			  
			  lossFunctionRegScaled = new ceres::ScaledLoss(lossFunctionReg, _gamma_, ceres::TAKE_OWNERSHIP); 
#if weighted			
			  costDvoErrorRegularization = new DvoErrorRegularizationWeighted(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularizationWeighted, 6, 6, 6, 1>(costDvoErrorRegularization); 
#else
			  costDvoErrorRegularization = new DvoErrorRegularization(); 		
			  costFunctionReg = 
			  new ceres::AutoDiffCostFunction<DvoErrorRegularization, 6, 6, 6>(costDvoErrorRegularization); 
#endif
			  
			  vector<double> residualReg(6);                 
			  bool evalReg = costFunctionReg->Evaluate(&(pose_combination[0]), residualReg.data(), 0); 
			  
			  //cout << "reg residuals: " << residualReg[0] << " " << residualReg[1] << " " << residualReg[2] << " "  
			  //<< residualReg[3] << " " << residualReg[4] << " " << residualReg[5] << endl; 
			  //cout << evalReg << endl; 
			  
			  if ( evalReg && (residualReg[0] != INVALID_RESIDUAL) ) // 
			  {
			  if (!residualsBlocks[current_index])
			  {
				residualsBlocks[current_index] = 
				problem.AddResidualBlock(costFunctionReg, 
				lossFunctionRegScaled, pose_combination);   
			  }
			  }
			  else
			  {
			  if (residualsBlocks[current_index])
			  {
				problem.RemoveResidualBlock(residualsBlocks[current_index]);
				residualsBlocks[current_index] = 0;
			  }
			  delete costFunctionReg; 
			  }
			    
			};
			
			
			uint num_combinations = regularizer_num_pairs; 
			
			
			
			// ### here -> push additionally the weights into the pose combination
			
			auto build_pose_combinations = [](std::vector<std::vector<double*> > &pose_combinations, 
						     std::vector<double*> &parameters, 
						     Eigen::SparseMatrix<int, Eigen::RowMajor> &adjm, 
						     int num_segments_, 
						     int current_p_comb,
						     int n_posesXX
						    ){
			  
			  //assert(num_segments_ == adjm.rows()); 
			  
			  
			  int shiftXX = num_segments_ * current_p_comb; 
			  
			  
			  
			  int n_combinations = adjm.sum(); 
			  int current_comb = 0; 
			  
			  
			  for (int i = 0; i < adjm.outerSize(); i++)
			    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
			      
			      assert(it.value() == 1); 
			      
			      int pose_index_A = it.row(); 
			      int pose_index_B = it.col(); 
			      int weight_index = n_posesXX + current_comb; 
			      
			      pose_combinations[current_comb].push_back(parameters[shiftXX + pose_index_A]); 
			      pose_combinations[current_comb].push_back(parameters[shiftXX + pose_index_B]); 
#if weighted			      
			      pose_combinations[current_comb].push_back(parameters[weight_index]); 
#endif			      
			      current_comb++; 
			      
			    }
			  
			  assert(current_comb == adjm.sum()); 
			  
			};
			
			
			std::vector<std::vector<double*> > pose_combinations(num_combinations); 
			
			// a single set of weights for segment pose pairs
			build_pose_combinations(pose_combinations, parameters, adjm, num_segments, n_pose_combinations, n_poses); 
			
			size_t index_basis = 2 * (n_pose_sets) * (size_t)w * (size_t)h + n_pose_combinations * num_combinations; 
			
			for (uint i = 0; i < num_combinations; i++){
			  
			  size_t current_index = index_basis + (size_t)i; 
			  
			  
			  //cout << "n_pose_sets: " << n_pose_sets << endl; 
			  //cout << "n_pose_sets: " << n_pose_sets << endl; 
			  
			  
#if weighted
			  DvoErrorRegularizationWeighted* costDvoErrorRegularization; 
#else		  
			  DvoErrorRegularization* costDvoErrorRegularization; 
#endif	
			  ceres::CostFunction* costFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionReg = 0; 
			  ceres::LossFunction* lossFunctionRegScaled = 0; 
			  
			  
			  add_regularizer(residualsBlocks, 
					current_index, 
					conf._gamma * gamma_normalizer, 
					pose_combinations[i], 
					problem, 
					costDvoErrorRegularization, 
					//weights.data(), 
					//i, 
					costFunctionReg, 
					lossFunctionReg, 
					lossFunctionRegScaled); 
					
			} // for all combinations
		  
		  
		  
		  
			} // ### YYYYYYYYYYYYYYYY
		  
		  
		  
			// ### the regularizer \sum_(1 - w^2)^2
#if weighted		
			
			uint num_combinations = regularizer_num_pairs; 
			
			size_t index_W_shift = 2 * n_pose_sets * (size_t)w * (size_t)h + n_pose_sets * regularizer_num_pairs; 
			
			double zeta = 1.0; 
			double zeta_normalizer 	= 1.0f / (double) regularizer_num_pairs; 
			
			for (uint i = 0; i < num_combinations; i++){
			
			DR_Robust_Optimizer_Error* cost_DR_Robust_Optimizer_Error; 
			ceres::CostFunction* costFunctionRO = 0; 
			ceres::LossFunction* lossFunctionRO = 0; 
			ceres::LossFunction* lossFunctionROScaled = 0; 
			  
			
			size_t w_index = num_segments * n_pose_sets + (size_t)i; 			// shift of the weights in the parameter array
			size_t res_shift = index_W_shift + i; 
			lossFunctionROScaled = new ceres::ScaledLoss(lossFunctionRO, conf._zeta * zeta_normalizer, ceres::TAKE_OWNERSHIP); 
			cost_DR_Robust_Optimizer_Error = new DR_Robust_Optimizer_Error(); 
			
			costFunctionRO = 
			new ceres::AutoDiffCostFunction<DR_Robust_Optimizer_Error, 1, 1>(cost_DR_Robust_Optimizer_Error); 
			
			double residualRegW;                 
			bool evalRegW = costFunctionRO->Evaluate(&(parameters[w_index]), &residualRegW, 0); 
			
			//cout << evalRegW << endl; 
			//cout << residualRegW << endl; 
			
			if ( evalRegW && (residualRegW != INVALID_RESIDUAL) ) // 
			{
			if (!residualsBlocks[res_shift])
			{
				residualsBlocks[res_shift] = problem.AddResidualBlock(costFunctionRO, lossFunctionROScaled, &weights[i]); 
			}
			}
			else
			{
			if (residualsBlocks[res_shift])
			{
				problem.RemoveResidualBlock(residualsBlocks[res_shift]); 
				residualsBlocks[res_shift] = 0;
			}
			delete costFunctionRO; 
			}   
			 
			};
			
#endif			
			// ### chained regularizer
			
			
			
#if 1			
			auto build_chaned_pose_combinations = [](std::vector<std::vector<double*> > &pose_combinations, 
								 Eigen::SparseMatrix<int, Eigen::RowMajor> &frame_connectivity,
								 uint num_segments,
								 std::vector<double*> &parameters
								 
			){
			  
			  //uint num_chainings = frame_connectivity.sum() - ( frame_connectivity.rows() - 1); 
			  // every pose in a large displecement case will be chained
			  //pose_combinations.resize(num_chainings * num_segments); 
			  
			  vector<int> num_chained_vec; //(pose_combinations.size()); 
			  
			  int cur_transformation = 0; 
			  int previous_row = -1; 
			  int current_comb = 0; 
			  
			  for (int q = 0; q < frame_connectivity.outerSize(); q++)
			    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it_c(frame_connectivity, q); it_c; ++it_c){
			
			      assert(it_c.value() == 1); 
			      size_t frame_A = it_c.row(); 
			      size_t frame_B = it_c.col(); 
			      

			      // everything starting from the second non-zero entry
			      // a chaining happens, a large displacement transformation detected
			      if (frame_A == previous_row){
				
				cout << "frame_A: " << frame_A << endl; 
				cout << "frame_B: " << frame_B << endl; 
				
				// this row -> chain to every remaining one in the row
				
				// ### perform the whole action here
				
				// -1 because of the first occurance of one has happened
				int s_correction = num_segments * (cur_transformation); 
				
				for (int segm = 0; segm < num_segments; segm++)
				  pose_combinations[current_comb + segm].push_back( parameters[ s_correction + segm ] ); 
				
				
				// the matrix is upper triangular
				// number of non-zero elements in the dense upper-triangular matrix
				
				auto shift = [](int f1, int f2, uint N){
				  
				  int num = 0; 
				  
				  for (uint i = 0; i < f1; i++)
				    num = num + (N - (i+1)); 
				    
				  num = num + f2 - (f1 + 1); 
				  
				  /*
				  cout << "f1: " << f1 << endl; 
				  cout << "f2: " << f2 << endl; 
				  cout << "N: " << N << endl; 
				  cout << "num: " << num << endl; 
				  char f; cin >> f; 
				  
				 */
				  return num; 
				  
				}; 
				
				
				int cf_cols = frame_connectivity.cols(); 
				int local_correction = 0; 
				int start_chained = frame_A; 
				int end_chained = frame_B; 
				int num_chained = 0; 
				
				for (int fr = frame_A; fr < frame_B; fr++){
				  
				  int next_frame = fr + 1; 
				  
				  //cout << "frame_A_XX: " << frame_A << endl;
				  //cout << "frame_B_XX: " << frame_B << endl; 
				  //cout << "frame_connectivity.rows(): " << frame_connectivity.rows() << endl; 
				  //cout << "fr: " << fr << endl; 
				  //cout << "fr + 1: " << fr + 1 << endl; 
				  

				    int shift_ = shift(fr, next_frame, frame_connectivity.rows()); 
				    
				    for (int segm = 0; segm < num_segments; segm++)
				      pose_combinations[current_comb + segm].push_back( parameters[ num_segments * shift_ + segm ] ); 
				     
				    num_chained++; 
				    
				}
				
				num_chained_vec.push_back(num_chained + 1); 
				current_comb = current_comb + num_segments; 

				
			      }
			      else{
				
				// the first occurence of the new row will be skipped
				previous_row = frame_A; 

			      }
			      
			      cur_transformation++; 
 
			      
			  }
			  
			  return num_chained_vec; 
			
			}; 
			
			
			
			// how many pose chainings will be there...
			
			
			
			
			std::vector<std::vector<double*> > pose_chainings(num_chainings * num_segments); 
			vector<int> num_chained_vec; 
			num_chained_vec = build_chaned_pose_combinations(pose_chainings, frame_connectivity, num_segments, parameters); 
			
			//cout << "num_chainings: " << num_chainings << endl; 
			//for (uint i = 0; i < num_chained_vec.size(); i++)
			//  cout << "num_chained_vec[" << i << "]: "<< num_chained_vec[i] << endl; 
			//cout << "THE POINT ACHIEVED" << endl; 
			//char c; cin >> c; 
			
			
			Chained_Pose_Regularizer* cost_Chained_Pose_Regularizer; 
			//ceres::CostFunction* costFunctionC = 0; 
			ceres::LossFunction* lossFunctionC = 0; 
			ceres::LossFunction* lossFunctionCScaled = 0; 
			
			
			
			
			// TODO change the total # of parameters
			
			
			size_t residual_shift = 2 * n_pose_sets * (size_t)w * (size_t)h + 
			(size_t)((n_pose_sets + 1) * regularizer_num_pairs);
			
			// size_t residual_shift = 2 * n_pose_sets * (size_t)w * (size_t)h + (size_t)(2 * regularizer_num_pairs); 
			
			double kappa = conf._kappa;		// 0.5 
			double kappa_normalizer = 1.0f / ( num_segments * num_chainings ); 
			
			for (uint k = 0; k < num_chainings; k++)
			  for (uint l = 0; l < num_segments; l++){
			    
			    uint current_pose_set = k * num_segments + l; 
			     
			    lossFunctionCScaled = new::ceres::ScaledLoss(lossFunctionC, kappa * kappa_normalizer, ceres::TAKE_OWNERSHIP); 
			    cost_Chained_Pose_Regularizer = new Chained_Pose_Regularizer(num_chained_vec[k]); 
			    
			    // num_chained_vec[k]
			    //int num_res = 6 * num_chained_vec[k]; 
			    // compute a chained pose and transfer it to the cost function

			    //costFunctionC = 
			    //new::ceres::AutoDiffCostFunction<Chained_Pose_Regularizer, 6, 6>(cost_Chained_Pose_Regularizer); 
			    
			    //costFunctionC = 
			    //new::ceres::DynamicAutoDiffCostFunction<Chained_Pose_Regularizer, 4>(cost_Chained_Pose_Regularizer);  
			    
			    
			    
			    ceres::DynamicAutoDiffCostFunction<Chained_Pose_Regularizer, 4>* costFunctionC =
				new ceres::DynamicAutoDiffCostFunction<Chained_Pose_Regularizer, 4>
				(cost_Chained_Pose_Regularizer  );
			    
			    
			    costFunctionC->SetNumResiduals(6);
			    
			    for (uint y = 0; y < num_chained_vec[k]; y++)
			      costFunctionC->AddParameterBlock(6); 
			    
			    
			    
			    
			    // ### debug
			    
			    /*
			    cout << "num_chained_vec[k]: " << num_chained_vec[k] << endl; 
			    
			    for (uint ind = 0; ind < num_chained_vec[k] ; ind++){
			      
			      cout << *pose_chainings[current_pose_set][ind] << endl; 
			      
			      
			    }
			    
			    char c; cin >> c; 
			    */
			    
			    
			    // ###
			    
			    
			    // &(pose_combination[0])
			    // &(pose_chainings[current_pose_set][0])
			    
			    
			    //vector<double> residualReg(6);    
			    
			    vector<double> residualC(6); 
			    bool evalRegC = costFunctionC->Evaluate(&(pose_chainings[current_pose_set][0]), residualC.data(), 0); 
			    
			    //cout << evalRegC << endl; 
			    size_t chained_shift = residual_shift + current_pose_set; 
			    
			    
			    if ( evalRegC && (residualC[0]!= INVALID_RESIDUAL) ) // 
			    {
			    if (!residualsBlocks[chained_shift])
			    {
				residualsBlocks[chained_shift] = problem.AddResidualBlock(costFunctionC, 
								 lossFunctionCScaled, pose_chainings[current_pose_set]); 
			    }
			    }
			    else
			    {
			    if (residualsBlocks[chained_shift])
			    {
				problem.RemoveResidualBlock(residualsBlocks[chained_shift]); 
				residualsBlocks[chained_shift] = 0;
			    }
			    delete costFunctionC; 
			    }
			    
			    
			  }
			
			
			
			
			
			
			
			
			
			
#endif			
			
			
			
			
			
			
			
			// ###
			
			
			
			int numValidResidualsBefore = problem.NumResidualBlocks(); 
			if (numValidResidualsBefore == 0){
			  
			        cout << "numValidResidualsBefore condition" << endl; 
				continue;
				
			}

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = conf._ceres_max_iterations;   				//                            
			options.num_threads = 8;
			options.num_linear_solver_threads = 8;
			//options.dogleg_type = ceres::SUBSPACE_DOGLEG; 
			//options.use_nonmonotonic_steps = true; 
			//options.line_search_type = ceres::WOLFE; 
			options.line_search_direction_type = ceres::BFGS; 
			//options.function_tolerance = 1e-20;
			//options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
			//options.linear_solver_type = ceres::SPARSE_SCHUR;		//
			//options.linear_solver_type = ceres::ITERATIVE_SCHUR;		// much much faster...
			options.linear_solver_type = ceres::CGNR;			// much much faster... pcg
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;
			
			SuccessfulStepCallback successfulStepCb;
			options.callbacks.push_back(&successfulStepCb);
			
			ceres::Solver::Summary summary;
			cout << "start solving..." << endl; 
			
			ceres::Solve(options, &problem, &summary);
			
			/*
			auto t2 = chrono::high_resolution_clock::now();
			cout << "-> non-rigid point cloud registration ready" << endl << "  -> elapsed time: " <<
			chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << " ms" << endl << endl;
			*/
			
			
			//if (debug)
				std::cout << summary.FullReport() << std::endl; 
			
			std::cout << std::endl; 
			std::cout << "initial cost: " << summary.initial_cost << std::endl; 
			std::cout << "final cost: " << summary.final_cost << std::endl; 
			std::cout << "fixed cost: " << summary.fixed_cost << std::endl; 
			std::cout << std::endl; 
			
			
			double error = summary.initial_cost / summary.final_cost; 
			
			if ( earlyBreak && ( error < (1.0/convergenceErrorRatio)) ){ 
			  break;
			}
			
			std::cout << std::endl; 
			
		}
  
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                std::cout << std::endl; 
		
  }
  
  // post-processing
  tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
  std::cout << "runtime: " << tmr << std::endl;
  
  // final poses
  for (uint i = 0; i < num_segments; i++)
    std::cout << "pose " << i << " final = " << poses_[i].transpose() << std::endl;
  
  return 0; 
  
}


// ###




// ### compute only on the large displacements, apply to all



int DynamicReconstructor::post_regularizer(){
  
  // ### for every two-frame transformation,  
  // ### group all segment poses into K clusters
  // ### try first for the 3-frame case
  
  // find median of the transformations, and assign transformations accordingly
  
  // ### outiers are parts of the closest moving pose!!!
  
  //frame_connectivity = frame_connectivity_; 
  //n_pose_sets = frame_connectivity.sum(); 
  //n_poses = num_segments * n_pose_sets; 
  
  //poses_.resize(n_poses);
  
  // ### now: only frame-to-frame transformations, 
  // ### global consistency will be guaranteed through global optimization
  
  
  
  // ### where 2-frame transformations are located, is seen in the frame connectivity matrix
  
  
  
  int cur_transformation = 0; 
  int previous_row = -1; 
  
  for (int q = 0; q < frame_connectivity.outerSize(); q++)
      for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it_c(frame_connectivity, q); it_c; ++it_c){
	
	assert(it_c.value() == 1); 
	int frame_A = it_c.row(); 
	int frame_B = it_c.col(); 
	
	if (frame_A != previous_row){
	  
	  // determine position shift
	  uint s_correction = num_segments * (cur_transformation); 
	  
	  // take the pose for every segment according to the shift...
	  
	  
	  vector<vector<int> > contributed_segments; 
	  
	  vector<double> norms; 
	 
	  vector<int> cardinalities; 
	  
	  std::map<double, int> map_poses;
	  
	  for (uint segm = 0; segm < num_segments; segm++){
	    
	    // cluster two sets and make consensus between them
	    
	    // find the first most frequent transformation...
	    // find the second most frequent transformation... 
	    // all other will be outliers...
	    
	    // cluster criterion -> norm of the transformation...
	    
	    // find the most frequent transformation -> this will be backrgound transformation, all other things will be 
	    // the moving objects! try this experiments first...

	    
	    
	    Eigen::Matrix<double, 6, 1> current_pose = poses_[s_correction + segm]; 
	    //Eigen::Vector3d current_pose = poses_[s_correction + segm].tail(3); 
	    
	    double norm = current_pose.norm(); 
	    norms.push_back(norm); 
	    
	    // search the current pose 
	    map_poses.insert(std::make_pair(norm, segm)); 
	    
	    //segm_key.insert(std::make_pair(current_value, num_segments));   
	  }
	  
	  
	  // compute a standard deviation 
	  
	  double sum = accumulate( norms.begin(), norms.end(), 0.0); 
	  double norms_average = sum/norms.size(); 
	  
	  
	  double std_dev = 0.0f; 
	  
	  
	  for (uint i = 0; i < norms.size(); i++){
	    
	    std_dev += abs(norms_average - norms[i]) * abs(norms_average - norms[i]); 
	    
	  }
	  
	  std_dev = sqrt( std_dev / norms.size() ); 
	  
	  cout << "std_dev: " << std_dev << endl; 
	  
	  
	  //char d; cin >> d; 
	  
	  
	  
	  
	  
	  vector<Eigen::Matrix<double, 6, 1>> average_poses; 
	  
	  double epsilon = norms_average; 
	  cardinalities.push_back(1); 
	  
	  double norm_previous = 1000.0f; //map_poses.begin()->first; 
	  int j = -1; 
	  
	  for (auto const& x : map_poses)
	  {
	    //std::cout << x.first  // string (key)
            //  << ':' 
            //  << x.second // string's value 
            //  << std::endl ;
	    
	    double norm_ = x.first; 
	    if ( abs(norm_ - norm_previous) < epsilon ){
	     
	      // update an average pose...
	      
	      Eigen::Matrix<double, 6, 1> current_pose; 
	      Eigen::Matrix<double, 6, 1> updated_pose; 
	      
	      int index_ = x.second; 
	      current_pose = poses_[s_correction + index_]; 
	      
	      
	      contributed_segments[j].push_back(index_); 
	      
	      updated_pose = (cardinalities[j] * average_poses[j] + current_pose ) / ( cardinalities[j] + 1 ); 
	      average_poses[j] = updated_pose; 
	      cardinalities[j]++; 
	      
	      
	      
	      
	    }
	    else{
	      
	      vector<int> vec;
	      
	      contributed_segments.push_back(vec); 
	      
	      cardinalities.push_back(1);
	      
	      Eigen::Matrix<double, 6, 1> new_pose; 
	      new_pose.setZero(); 
	      average_poses.push_back(new_pose); 
	      
	      j++; 
	    }
	    
	    norm_previous = norm_; 
	    
	  }
	  
	  
	  
	  
	  
	  // now : the pose which have the highest cardinality is the background pose
	  
	  uint highest_cardinality = 0; 
	  uint second_cardinality = 1; 
	  
	  for (uint i = 0; i < cardinalities.size(); i++){
	    if (cardinalities[i] > cardinalities[highest_cardinality]){ 
	      second_cardinality = highest_cardinality; 
	      highest_cardinality = i; 	      
	    }
	  }
	  
	  
	  // the pose which has the second cardinality is the object pose
	  
	  
	  // every thing that is close to the pose with a highest cardinality -> background and gets the average pose
	  
	  // change the poses to the average pose (background)...
	  
	  
	  
	 for (uint s_ = 0; s_ < num_segments; s_++){
	   poses_[s_correction + s_] = average_poses[second_cardinality]; 
	} 
	  
	  
	  
	 for (uint i = 0; i < contributed_segments[highest_cardinality].size(); i++){
	   
	   poses_[ s_correction + contributed_segments[highest_cardinality][i] ] = average_poses[highest_cardinality]; 
	   
	}
	  
	  
	  
	for (uint i = 0; i < cardinalities.size(); i++)
	  std::cout << cardinalities[i] << std::endl; 
	
	std::cout << "cardinalities.size(): " << cardinalities.size() << std::endl; 
	
	char f; cin >> f; 
	  
	  
	  
	  
	  /*
	  for (uint s_ = 0; s_ < num_segments; s_++){
	    
	    
	    //if ( abs ( poses_[s_correction + s_].norm() - average_poses[highest_cardinality].norm() ) < epsilon ){
	    //  poses_[s_correction + s_] = average_poses[highest_cardinality]; 
	     
	      // mark this segment as background
	     
	   //for (uint i = 0; i < contributed_segments[])  
	     
	      
	      
	    //}
	    //else if (second_cardinality != -1){
	      //poses_[s_correction + s_] = average_poses[second_cardinality]; 
	      
	      // mark this segment as foreground, i.e., moving object
	      
	      
	      
	    //}
	    
	  }
	  */
	  
	  
	  // alternatively: analyze weights! apply a threshold on the weights!!!
	  
	  
	  
	  
	  
	  // for 
	  
	  
	  
	  
	  
	  
	  // everythings else gets the average pose corresponding to the second most frequent cardinality
	  
	  
	  
	  // generate masks for the background and foreground
	  
	  
	  
	  // -> use these masks to save poses in the separate files for integration into TSDF
	  
	  
	  
	  // -> try the integration into the TSDF! 
	  
	  
	  
	  // ### first, try on the two-frame case :: additional steps will be required... -> backwarping and integration + 
	  // transformation of the meshes etc. 
	  
	  
	  
	  
	  
	  // 
	  
	  
	  
	  
	  /*
	  
	  // sort the vector
	  
	  auto sort_using_greater_than = [](double u, double v){ return u > v; }; 
	  std::sort(norms.begin(), norms.end(), sort_using_greater_than); 
	  
	  double epsilon = 0.1; 
	  
	  vector<double> poses; 
	  
	  cardinalities.push_back(1); 
	  uint j = 0; 
	  for (uint i = 1; i < norms.size(); i++){
	    
	    if ( abs(norm[i] - norm[i-1]) < epsilon ){
	      cardinalities[j]++; 
	    }
	    else{
	      cardinalities.push_back(1);
	    }
	    
	  }
	  
	  
	  for (uint i = 0; i < cardinalities.size(); i++)
	    cout << cardinalities[i] << " "; 
	  cout << endl; 
	  
	  //char t; cin >> t; 
	  */
	  
	  
	  
	  
	  
	  previous_row = frame_A; 
	  
	}
	
	
	cur_transformation++; 
	
      }
  
  
  
  
  
  
  return 0; 
  
}





int DynamicReconstructor::reconstruct_every_segment(){
  
  // segmentation transfer
  
  // compute repsojections for every point and every pose -> compute scene flow
  
  std::mt19937 rng; 
  rng.seed(std::random_device()()); 
  std::uniform_int_distribution<std::mt19937::result_type> dist255(1, 255);
  
  // generate n_segments random colors
  Eigen::MatrixXi Colors(num_segments, 3); 
  
  for (int i = 0; i < num_segments; i++){
    Colors(i, 0) = dist255(rng); 
    Colors(i, 1) = dist255(rng); 
    Colors(i, 2) = dist255(rng); 
  }
  
  
  
  cv::Mat ref_frame_segmentation(height, width, CV_8UC3, cv::Scalar(0, 0, 0)); 
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){

      cv::Vec3b &vec_ = ref_frame_segmentation.at<cv::Vec3b>(i, j); 
      
      int segm_ = segmentations[0].at<int>(i, j); 
      
      //cout << segm_ << endl; 
      
      if (segm_ != -1){
      
        vec_[0] = Colors(segm_, 0); 
        vec_[1] = Colors(segm_, 1); 
        vec_[2] = Colors(segm_, 2);       
	
      }
      
    }
  
  
  // for every segment, perform reprojection into every current frame...
  
  
  auto concat_poses = [](Eigen::Matrix<double, 6, 1> &pose23, Eigen::Matrix<double, 6, 1> &pose12){ 
    
    // if ( (v[0] == 0.0f) && (v[1] == 0.0f) && (v[2] == 0.0f) )   
    // R = Eigen::Matrix3d::Identity(3, 3);  
        
    // translation
    Eigen::Vector3d t23 = pose23.topRows<3>(); 
    Eigen::Vector3d t12 = pose12.topRows<3>(); 
    // rotation
    Eigen::Vector3d p23 = pose23.bottomRows<3>(); 
    Eigen::Vector3d p12 = pose12.bottomRows<3>(); 
    
    // rotation matrices
    Eigen::Matrix3d R23, R12, R13; 
    
    if (p23.norm() == 0.0f){
      R23 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_1 = p23.norm(); 
      Eigen::Vector3d axis_1 = p23.normalized(); 
      Eigen::AngleAxisd axisangle_1(angle_1, axis_1); 
      R23 = axisangle_1.toRotationMatrix(); 
    }
    
    if (p12.norm() == 0.0f){
      R12 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_2 = p12.norm(); 
      Eigen::Vector3d axis_2 = p12.normalized(); 
      Eigen::AngleAxisd axisangle_2(angle_2, axis_2); 
      R12 = axisangle_2.toRotationMatrix(); 
    }
    
    R13 = R23 * R12; 
    
    Eigen::AngleAxisd axisangle_result; 
    axisangle_result.fromRotationMatrix<Eigen::Matrix3d>(R13); 
    
    Eigen::Vector3d p13; 
    p13 = axisangle_result.axis() * axisangle_result.angle(); 
    
    
    
    Eigen::Vector3d t13 = t12 + t23; 
    
    Eigen::Matrix<double, 6, 1> pose13; 
    pose13.head(3) = t13; 
    pose13.tail(3) = p13; 
    
    
    
    return pose13; 
    
  }; 
  
  vector< Eigen::Matrix<double, 6, 1> > accumulated_poses; // identity at the beginning
    
    accumulated_poses.resize(num_segments); 
    for (uint i = 0; i < num_segments; i++){
      accumulated_poses[i].setZero(); 
    }
  
  
  uint zz = 0; 
  uint cur_transformation = 0; 
  int previous_row = -1; 
  
  for (int q = 0; q < frame_connectivity.outerSize(); q++)
      for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it_c(frame_connectivity, q); it_c; ++it_c){
	
	assert(it_c.value() == 1); 
	int frame_A = it_c.row(); 
	int frame_B = it_c.col(); 
	
	
	
	if (frame_A != previous_row){
	  
	  
	  cv::Mat current_segmentation(height, width, CV_8UC3, cv::Scalar(0, 0, 0)); 
	  
	  
	  // determine position shift
	  uint s_correction = num_segments * (cur_transformation);   
	
	
	// take the pose for every segment according to the shift...
	for (uint segm = 0; segm < num_segments; segm++){
	  accumulated_poses[segm] = concat_poses(poses_[s_correction + segm], accumulated_poses[segm]); 
	}
	
	
	
	
	// perform projection...
	
	for (uint i = 0; i < height; i++)
	  for (uint j = 0; j < width; j++){
	    
	    // intrinsics...
	    
	    //double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) }; 
	        
	    double fx = K(0, 0); 
	    double fy = K(1, 1); 
	    
	    double cx = K(0, 2);
	    double cy = K(1, 2); 
	    
	    int segm_ = segmentations[0].at<int>(i, j); 
	    Eigen::Matrix<double, 6, 1> current_pose = accumulated_poses[segm_]; 
	    
	    Eigen::Vector3d translation = current_pose.topRows<3>(); 
	    
	    Eigen::Vector3d v = current_pose.bottomRows<3>(); 
	    
	    
	    Eigen::Matrix3d R; 
	    
	    if (v.norm() == 0.0f){
	      R = Eigen::Matrix3d::Identity(3, 3); 
	    }
	    else{
	      double angle = v.norm(); 
	      Eigen::Vector3d axis = v.normalized(); 
	      Eigen::AngleAxisd axisangle(angle, axis); 
	      R = axisangle.toRotationMatrix(); 
	    }
	    
	    //Eigen::Matrix3d R = axisangle.toRotationMatrix(); 

	    // take the depth
	    const float* ptrDepth = (const float*)g_depths[reference_frame].data; 
	    double dRef = ptrDepth[i * width + j]; 
	    
	    double x0 = (j - cx) / fx;
	    double y0 = (i - cy) / fy;
	    
	    Eigen::Vector3d pt3Ref;
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
	    
	    pt3Ref += translation; 
	    
	    Eigen::Vector3d pt3Cur = pt3Ref.transpose() * R.inverse(); 
	    
	    //cout << "pt3Cur: " << pt3Cur << endl; 
	    
	    
	    //Eigen::Vector3d pt3Cur; 
	    
	    // project the point
	    
	    if (pt3Ref[2] > 0.0f ){
	      
	      double px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
	      double py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
		
	      if ( (px > 0.0f) && (py > 0.0f) && (px < (double)width) && (py < (double)height) ){
		
		int px_ = round(px); 
		int py_ = round(py);
		
		//cout << px_ << " " << py_ << endl; 
		
		
		cv::Vec3b &vec_ = current_segmentation.at<cv::Vec3b>(py, px); 
		
		//int segm_ = segmentations[0].at<int>(i, j); 
		
		vec_[0] = Colors(segm_, 0); 
		vec_[1] = Colors(segm_, 1); 
		vec_[2] = Colors(segm_, 2); 
		
	      }
	    }
	  }
	
	
	previous_row = frame_A; 
	
	cout << "ready to show the current_segmentation" << endl; 
	
	stringstream ss; 
	ss.str("");
	ss << "./" << frame_A << ".png" << endl; 
	
	
	// ### filter current segmentation
	
	
	//for (uint i = 0; i < 2; i++){
	
	for (uint i = 1; i < height - 1; i++)
	  for (uint j = 1; j < width - 1; j++){
	    
	    cv::Vec3b &vec_ = current_segmentation.at<cv::Vec3b>(i, j); 
	    
	    cv::Vec3b &vec_1 = current_segmentation.at<cv::Vec3b>(i + 1, j);
	    cv::Vec3b &vec_2 = current_segmentation.at<cv::Vec3b>(i - 1, j);
	    cv::Vec3b &vec_3 = current_segmentation.at<cv::Vec3b>(i, j + 1);
	    cv::Vec3b &vec_4 = current_segmentation.at<cv::Vec3b>(i, j - 1);
	    
	    double norm = sqrt( vec_[0] * vec_[0] + vec_[1] * vec_[1] + vec_[2] * vec_[2] ); 
	    
	    if (norm == 0.0f){
	      
	      
	      double norm1 = sqrt( vec_1[0] * vec_1[0] + vec_1[1] * vec_1[1] + vec_1[2] * vec_1[2] ); 
	      
	      double norm3 = sqrt( vec_3[0] * vec_3[0] + vec_3[1] * vec_3[1] + vec_3[2] * vec_3[2] ); 
	      
	      if (norm1 != 0.0f){
	      vec_[0] = vec_1[0];
	      vec_[1] = vec_1[1];
	      vec_[2] = vec_1[2];
	      }
	      else if (norm3 != 0.0f){
	      vec_[0] = vec_3[0];
	      vec_[1] = vec_3[1];
	      vec_[2] = vec_3[2];
	      }
	      
	    }   
	  }
	
	//}
	
	
	cv::imwrite(ss.str(), current_segmentation); 
	//cvWaitKey(0); 
	
	
	}
	
	
	
	
	
	cur_transformation++; 
	
      }
  
  //cout << "num_segments: " << num_segments << endl; 
  //cv::imshow("", ref_frame_segmentation); 
  //cvWaitKey(0);
  
  // a TSDF separate for every segment...
  
  
  
  
  // find correspondences,    integrate all measurements 
  
  
  
  
  
  
  
  
  
  
}








// 1 - save all segments (only for small # of segments)
// 2 - save only the final registration result
// 
int DynamicReconstructor::save_results(int save_type){
  
  // frame_colors 
  
  Eigen::MatrixXd pc_cur, pc_ref_whole; 
  cv::Mat vertexMapCur, vertexMapRef_; 
  
  RgbdProcessing* rgbdProc_ = new RgbdProcessing(); 
  
  
  for (uint i = 0; i < n_frames; i++)
  {
  
    if (i != reference_frame){
      
      rgbdProc_->computeVertexMap(K, g_depths[i], vertexMapCur); 
      cv::Mat segment1(g_depths[i].rows, g_depths[i].cols, CV_8UC1, cv::Scalar(255)); 
      extract_point_cloud(pc_cur, segment1, vertexMapCur); 
      stringstream cur_ply; 
      cur_ply.str(""); 
      cur_ply << "./RESULT/cur" << i << ".ply"; 
      //string cur_ply = "./RESULT/cur.ply"; 
      write_ply_file(cur_ply.str(), pc_cur, &frame_colors[i], {255, 0, 255}, 1); 
      
    }
  }
  
  // ### save reference as a whole
  
  rgbdProc_->computeVertexMap(K, g_depths[reference_frame], vertexMapRef_); 
  cv::Mat segment1(g_depths[reference_frame].rows, g_depths[reference_frame].cols, CV_8UC1, cv::Scalar(255)); 
  extract_point_cloud(pc_ref_whole, segment1, vertexMapRef_); 
  string ref_ply_ = "./RESULT/ref_.ply";
  write_ply_file(ref_ply_, pc_ref_whole, &frame_colors[reference_frame], {0, 255, 255}, 1); 
  
  Eigen::MatrixXd ref_transformed_ = Eigen::MatrixXd::Zero(depthRef.rows * depthRef.cols, 3);   	// maximum all points
  uint segment_shift = 0; 
  Eigen::MatrixXi ColorsWhole = Eigen::MatrixXi(depthRef.rows * depthRef.cols, 3); 
  
  // ###
  
  
  save_type = conf._save_segments; 
  
  //if ( save_type == 1 ){
  /*
  
  for (uint s = 0; s < num_segments; s++){
      
        // ### translation
        Eigen::Vector3d translation = poses_[s].topRows<3>(); 
        
        Eigen::Vector3d v = poses_[s].bottomRows<3>(); 
        double angle = v.norm(); 
        Eigen::Vector3d axis = v.normalized(); 
	
	//sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        //axis[0] = v[0] / angle;
        //axis[1] = v[1] / angle;
        //axis[2] = v[2] / angle;
        
        Eigen::AngleAxisd axisangle(angle, axis); 
        Eigen::Matrix3d R = axisangle.toRotationMatrix(); 
        Eigen::MatrixXd pc_ref, pc_ref_rotated; 
	
        //RgbdProcessing* rgbdProc_ = new RgbdProcessing(); 
        cv::Mat vertexMapRef; 
        rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef);  
        //rgbdProc_->computeVertexMap(K, depthCur, vertexMapCur); 
	
	extract_point_cloud(pc_ref, segmentations, s, vertexMapRef); 
	       
	pc_ref_rotated = pc_ref * R.inverse(); 
	pc_ref_rotated = pc_ref_rotated.rowwise()  + translation.transpose(); 
	
	
	if ( save_type == 1 ){
	  string ref_ply, ref_rot_ply; 
	  stringstream ss;
	  ss.str("");
	  ss << "./RESULT/ref" << s << " .ply";
	  ref_ply = ss.str();
	
	  ss.str(""); 
	  ss << "./RESULT/ref_rot_segm" << s << " .ply"; 
	  ref_rot_ply = ss.str(); 
	
	  // TODO add an option: no colors, use texture or a single color
	  write_ply_file(ref_ply, pc_ref, &segment_colors[s], {0, 255, 255}, 1); 
	  write_ply_file(ref_rot_ply, pc_ref_rotated, &segment_colors[s], {255, 255, 0}, 1); 
	  std::cout << "written" << std::endl; 
	}
	else if ( save_type == 2){
	  
	  //cout << pc_ref_rotated.rows() << " " << pc_ref_rotated.cols() << endl; 
	  //cout << num_elements_per_segment[s] << endl;
	  
	  ref_transformed_.block(segment_shift, 0, num_elements_per_segment[s], 3) = pc_ref_rotated; 
	  ColorsWhole.block(segment_shift, 0, num_elements_per_segment[s], 3) = segment_colors[s]; 
	  segment_shift += num_elements_per_segment[s]; 
	  
	  
	}
  }
   
  if ( save_type == 2 ){
    
    Eigen::MatrixXd ref_transformed = ref_transformed_.block(0, 0, segment_shift, 3);       
    string ref_ply_transformed = "./RESULT/ref_transformed.ply";
    write_ply_file(ref_ply_transformed, ref_transformed, &ColorsWhole, {255, 255, 0}, 1);  
    
    
  }
  
  
  */
  
  // ### if there are more current frames
  // ### change poses T13 to T23 \cdot T12
  
  
  
  auto concat = [](Eigen::Vector3d &p23, Eigen::Vector3d &p12){ 
    
    Eigen::Matrix3d R23, R12, R13; 
    
    if (p23.norm() == 0.0f){
      R23 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_1 = p23.norm(); 
      Eigen::Vector3d axis_1 = p23.normalized(); 
      Eigen::AngleAxisd axisangle_1(angle_1, axis_1); 
      R23 = axisangle_1.toRotationMatrix(); 
    }
    
    
    if (p12.norm() == 0.0f){
      R12 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_2 = p12.norm(); 
      Eigen::Vector3d axis_2 = p12.normalized(); 
      Eigen::AngleAxisd axisangle_2(angle_2, axis_2); 
      R12 = axisangle_2.toRotationMatrix(); 
    }
    
    R13 = R23 * R12; 
    
    Eigen::AngleAxisd axisangle_result; 
    axisangle_result.fromRotationMatrix<Eigen::Matrix3d>(R13); 
    
    Eigen::Vector3d return_param; 
    return_param = axisangle_result.axis() * axisangle_result.angle(); 
    return return_param; 
    
  }; 
  
  
  
  
  auto concat_poses = [](Eigen::Matrix<double, 6, 1> &pose23, Eigen::Matrix<double, 6, 1> &pose12){ 
    
    // if ( (v[0] == 0.0f) && (v[1] == 0.0f) && (v[2] == 0.0f) )   
    // R = Eigen::Matrix3d::Identity(3, 3);  
        
    // translation
    Eigen::Vector3d t23 = pose23.topRows<3>(); 
    Eigen::Vector3d t12 = pose12.topRows<3>(); 
    // rotation
    Eigen::Vector3d p23 = pose23.bottomRows<3>(); 
    Eigen::Vector3d p12 = pose12.bottomRows<3>(); 
    
    // rotation matrices
    Eigen::Matrix3d R23, R12, R13; 
    
    if (p23.norm() == 0.0f){
      R23 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_1 = p23.norm(); 
      Eigen::Vector3d axis_1 = p23.normalized(); 
      Eigen::AngleAxisd axisangle_1(angle_1, axis_1); 
      R23 = axisangle_1.toRotationMatrix(); 
    }
    
    if (p12.norm() == 0.0f){
      R12 = Eigen::Matrix3d::Identity(3, 3); 
    }
    else{
      double angle_2 = p12.norm(); 
      Eigen::Vector3d axis_2 = p12.normalized(); 
      Eigen::AngleAxisd axisangle_2(angle_2, axis_2); 
      R12 = axisangle_2.toRotationMatrix(); 
    }
    
    R13 = R23 * R12; 
    
    Eigen::AngleAxisd axisangle_result; 
    axisangle_result.fromRotationMatrix<Eigen::Matrix3d>(R13); 
    
    Eigen::Vector3d p13; 
    p13 = axisangle_result.axis() * axisangle_result.angle(); 
    
    
    
    Eigen::Vector3d t13 = t12 + t23; 
    
    Eigen::Matrix<double, 6, 1> pose13; 
    pose13.head(3) = t13; 
    pose13.tail(3) = p13; 
    
    
    
    return pose13; 
    
  }; 
  
  
  // ### save all possible combinations and concatenations...
  // ### name them tij, or t_12_23_34 (then it is clear that it is concatenated)

  // first row -> concatenation is not required
  // second row -> concatenation is required with t_12
  // starting from the second row -> save only continuously concatenated results, w/o jumps
  
  
  
  if (conf._optimization_type == 2){
    
    uint cur_transformation = 0; 
    
    vector< Eigen::Matrix<double, 6, 1> > accumulated_poses; // identity at the beginning
    
    accumulated_poses.resize(num_segments); 
    for (uint i = 0; i < num_segments; i++){
      accumulated_poses[i].setZero(); 
    }
    
    stringstream concat_name; 
    concat_name.str(""); 
    concat_name << "./RESULT/"; 
    
    int previous_row = -1; 
    
    for (int q = 0; q < frame_connectivity.outerSize(); q++)
      for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it_c(frame_connectivity, q); it_c; ++it_c){
	
	assert(it_c.value() == 1); 
	int frame_A = it_c.row(); 
	int frame_B = it_c.col(); 
	
	// rule to concatenate
	// ### the first occurance of 1 in the new row means concatenation
	
	// ### concatenation
	if (frame_A != previous_row){
	  
	  // determine position shift...
	  uint s_correction = num_segments * (cur_transformation); 
	  
	  // take the pose for every segment according to the shift...
	  for (uint segm = 0; segm < num_segments; segm++){
	    accumulated_poses[segm] = concat_poses(poses_[s_correction + segm], accumulated_poses[segm]); 
	  }
	  
	  // concat_name contains the name of the new file...
	  concat_name << frame_A << frame_B << "x"; 
	  
	  // perform transformation of the segments and save...
	  string filename = concat_name.str() + ".ply"; 
	  
	  save_transformed_reference(accumulated_poses, filename); 
	  previous_row = frame_A; 
	  
	}
	
	cur_transformation++; 
	      
	}
     
  }
  
  
  
  if (conf._optimization_type == 2){
      
      uint cur_i = 0; 
      
      for (uint i = 0; i < n_frames; i++){
	
	
	if (i != reference_frame){
	
	uint s_correction = num_segments * (cur_i);    // 
	
	// extract the required pose vector
	
	vector< Eigen::Matrix<double, 6, 1> >::const_iterator first = poses_.begin() + num_segments * cur_i; 
	vector< Eigen::Matrix<double, 6, 1> >::const_iterator last = poses_.begin() + num_segments * (cur_i + 1); 
	vector< Eigen::Matrix<double, 6, 1> > pose_subset_(first, last);
		
	stringstream ref_ply_transformed; 
	ref_ply_transformed.str(""); 
	ref_ply_transformed << "./RESULT/ref_transformed " << i << ".ply"; 
	
	save_transformed_reference(pose_subset_, ref_ply_transformed.str()); 

	cur_i++;
	
	
	}
      }
      
      
      
    }
  
  // ### weights...
  
  cout << "weights: " << endl; 
  for (uint i = 0; i < weights.size(); i++)
    cout << weights[i] << endl; 
  cout << endl; 
  
  return 0; 
  
}






int DynamicReconstructor::fetch_segment_colors(){
    
  // frame_colors
  
  // paths_rgb
  // paths_depth
  
  
  for (int fr = 0; fr < n_frames; fr++)
  {
  
  int counter_ = 0; 
  
  cv::Mat colorCur = cv::imread(paths_rgb[fr]); 
  //cv::Mat colorRef = cv::imread(path_colorRef); 
  Eigen::MatrixXi ColorsCurrent(width * height, 3); 
  //Eigen::MatrixXi ColorsReference(width * height, 3); 
  counter_ = 0; 
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      cv::Vec3b color = colorCur.at<cv::Vec3b>(i, j); 
      //cv::Vec3b colorR = colorRef.at<cv::Vec3b>(i, j); 
      ColorsCurrent(counter_, 0) = color[2]; 
      ColorsCurrent(counter_, 1) = color[1]; 
      ColorsCurrent(counter_, 2) = color[0]; 
      //ColorsReference(counter, 0) = colorR[2];
      //ColorsReference(counter, 1) = colorR[1];
      //ColorsReference(counter, 2) = colorR[0];
      counter_++; 
    }
  
  frame_colors.push_back(ColorsCurrent); 
  
  //colors_current = ColorsCurrent; 
  
  
  
  }
  
  int counter = 0; 
  
  for (uint s = 0; s < num_segments; s++){
  
  // ### pre-compute colors?.. somewhere above
	// ### add Colors to the output...
	Eigen::MatrixXi ColorsBuffer(width * height, 3); 
	
	counter = 0; 
	
	// lambda: operates on  Eigen::MatrixXi &Colors, 
	//			vector<cv::Mat> segmentations, 
	//			uint s (segment), 
	//			path_colorRef
	auto fill_colors = [&]()
	{
	  
	  cv::Mat colorRef = cv::imread(paths_rgb[reference_frame]); 
	  for (uint i = 0; i < height; i++)
	    for (uint j = 0; j < width; j++){
	       if ( segmentations[0].at<int>(i, j) == s ){ 
		cv::Vec3b color = colorRef.at<cv::Vec3b>(i, j); 
		ColorsBuffer(counter, 0) = color[2]; 
		ColorsBuffer(counter, 1) = color[1]; 
		ColorsBuffer(counter, 2) = color[0]; 
		counter++; 
	      }
	  }
	}; 
	
	fill_colors(); 
	Eigen::MatrixXi ColorsSegment = ColorsBuffer.block(0, 0, counter, 3); 
	segment_colors.push_back(ColorsSegment); 
  }  
  
  return 0; 
  
}



int DynamicReconstructor::visualize_segment_neighbours(){
  
  // ### segmentations
  
  // ### required: adjm, segmentation mask, segments mask
  
  cout << height << endl; 
  cout << width << endl; 
  cout << num_segments << endl; 
  
  //char f; cin >> f; 
  
  vector<cv::Mat> pose_pairs_visualizations; 
  
  
  for (uint i = 0; i < num_segments; i++){
    cv::Mat visualization_current_segment(height, width, CV_8UC3, cv::Scalar(0,0,0)); 
    pose_pairs_visualizations.push_back(visualization_current_segment); 
  }
  
  // ### unrelated segments, current segment, segment pairs...
  
  vector<vector<int>> adjm_lines(num_segments); 
  
  
  for (int i = 0; i < adjm.outerSize(); i++)
    for (Eigen::SparseMatrix<int, Eigen::RowMajor>::InnerIterator it(adjm, i); it; ++it){
	assert(it.value() == 1); 
	int current_segment 	= it.row(); 
	int neighbour_segment 	= it.col(); 
	adjm_lines[current_segment].push_back(neighbour_segment); 
    }
  
  
  //for (uint i = )
  
  
  for (uint i = 0; i < height; i++)
    for (uint j = 0; j < width; j++){
      
      uint segment_pp = segmentations[0].at<int>(i, j); 
      
      // color segment_pp pixel in green 
      cv::Vec3b &color = pose_pairs_visualizations[segment_pp].at<cv::Vec3b>(i, j);       
      color = {0, 255, 0}; 
      
      
      // determine, in which images the same pixel needs to be colored differently
      
      for (uint k = 0; k < adjm_lines.size(); k++){
	if ( (std::find(adjm_lines[k].begin(), adjm_lines[k].end(), segment_pp) != adjm_lines[k].end()) ){  
	  //color = {255, 0, 255}; 
	  cv::Vec3b &color2 = pose_pairs_visualizations[k].at<cv::Vec3b>(i, j);       
	  color2 = {0, 255, 255}; 
	}
	  
	  
      }
            
            
            
    }
  
  
  for (uint i = 0; i < num_segments; i++){
    
    stringstream ss; 
    ss.str(""); 
    ss << "./neighbours_vis/segment" << i << ".png"; 
    cv::imwrite(ss.str(), pose_pairs_visualizations[i]); 
    
  }
  
  
}




// ###

int DynamicReconstructor::save_transformed_reference(vector<Eigen::Matrix<double, 6, 1>> current_poses, string filename){
	
	
	//uint s_correction = num_segments * (cur_i);    // 
	
	RgbdProcessing* rgbdProc_ = new RgbdProcessing(); 
	
	uint segment_shift = 0; 
	
	Eigen::MatrixXd ref_transformed_ = Eigen::MatrixXd::Zero(depthRef.rows * depthRef.cols, 3); // maximum all points
	Eigen::MatrixXi ColorsWhole = Eigen::MatrixXi(depthRef.rows * depthRef.cols, 3); 
	
	for (uint s = 0; s < num_segments; s++){
	  
	  // translation
	  Eigen::Vector3d translation = current_poses[s].topRows<3>(); 
	  
	  // rotation
	  Eigen::Vector3d v = current_poses[s].bottomRows<3>(); 
	  Eigen::Matrix3d R; 
	  
	  if ( (v[0] == 0.0f) && (v[1] == 0.0f) && (v[2] == 0.0f) ){  
	    R = Eigen::Matrix3d::Identity(3, 3); 
	  }
	  else{
	    
	    double angle = v.norm(); 
	    Eigen::Vector3d axis = v.normalized(); 
	    Eigen::AngleAxisd axisangle(angle, axis); 
	    R = axisangle.toRotationMatrix(); 
	    
	  }
	  
	  Eigen::MatrixXd pc_ref, pc_ref_rotated; 
	  
	  cv::Mat vertexMapRef; 
	  rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef);  
	  extract_point_cloud(pc_ref, segmentations, s, vertexMapRef); 
	       
	  pc_ref_rotated = pc_ref * R.inverse(); 
	  pc_ref_rotated = pc_ref_rotated.rowwise()  + translation.transpose(); 
	  
	  ref_transformed_.block(segment_shift, 0, num_elements_per_segment[s], 3) = pc_ref_rotated; 
	  ColorsWhole.block(segment_shift, 0, num_elements_per_segment[s], 3) = segment_colors[s]; 
	  segment_shift += num_elements_per_segment[s]; 
	  
	}
	
	
	Eigen::MatrixXd ref_transformed = ref_transformed_.block(0, 0, segment_shift, 3); 
	write_ply_file(filename, ref_transformed, &ColorsWhole, {255, 255, 0}, 1); 
  
} 






#endif