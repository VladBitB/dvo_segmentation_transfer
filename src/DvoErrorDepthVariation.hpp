
#ifndef _DVOERRORDEPTHVARIATION_H
#define _DVOERRORDEPTHVARIATION_H

#define INVALID_RESIDUAL    0.0

// ### depth constancy / depth variation constraint
// ### update correspondences...



void update_correspondences(uint w, 					// width 
			    uint h, 					// height 
			    const float* depthRef, 			// 
			    const float* depthCur, 			// 
			    vector<Eigen::Matrix<double, 6, 1>> &poses, 
			    Eigen::MatrixXi &CORRESP, 
			    const double* intrinics,
			    cv::Mat segments
			    //vector<cv::Mat> segments_
			   )
		{
		  
			vector<Eigen::Matrix3d> POSES(poses.size()); 
			vector<Eigen::Vector3d> translations(poses.size()); 
			
			auto convert_to_rot = [] (Eigen::Matrix3d &POSE_X, Eigen::Vector3d &translation, Eigen::Matrix<double, 6, 1> pose_x) {
			  // Eigen::MatrixXi &CORRESP
			  // Eigen::Vector3d 
			  translation = pose_x.topRows<3>();  
			  Eigen::Vector3d rot = pose_x.bottomRows<3>(); 
			  double angle = rot.norm(); 
			
			  if (angle < 0.00001f){
			    POSE_X = Eigen::Matrix3d::Identity(); 
			  }
			  else {
			    Eigen::Vector3d axis = rot.normalized(); 
			    Eigen::AngleAxisd bb(angle, axis); 
			    POSE_X = bb.toRotationMatrix(); 
			  }
			};
			
			
			CORRESP = Eigen::MatrixXi::Zero(h, w); 
			
			for (uint i = 0; i < poses.size(); i++){
			  convert_to_rot(POSES[i], translations[i], poses[i]); 
			}
			
			//cout << POSE << endl; 
			//char t ; cin >> t; 
			
			// camera intrinsics
			double fx = intrinics[0];
			double fy = intrinics[1];
			double cx = intrinics[2];
			double cy = intrinics[3];
			
			
			/*
			for (uint s = 0; s < poses.size(); s++){
			  for (uint i = 0; i < )
			}
			*/
			
			
			/*
			auto contained_in = [&](uint row, uint col){ 
			  for (int i = 0; i < segments_.size(); i++){ 
			    if ( segments_[i].at<uchar>(row, col) == 255 ){
			       return i; 
			    }
			    
			  }
			  return -1; 
			};
			*/
			
			
			
			
			for (uint y = 0; y < h; y++)
			  for (uint x = 0; x < w; x++){
			    
			    // determine in which segment is the point...
			    
			    //int in_the_segment = contained_in(y, x); 
			    
			    int in_the_segment = segments.at<int>(y, x); 
			    
			    
			    if (in_the_segment == -1){
			      CORRESP(y, x) = -1;  
			      continue; 
			    }
			    
			    float dRef = depthRef[y*w + x]; 
			    
			    // project 2d point back into 3d using its depth
			    double x0 = (x - cx) / fx;
			    double y0 = (y - cy) / fy;
			    
			    // transform reference 3d point into current frame
			    Eigen::Vector3d pt3Ref;
			    pt3Ref[0] = x0 * dRef;
			    pt3Ref[1] = y0 * dRef;
			    pt3Ref[2] = dRef;
			    
			    Eigen::Vector3d pt3RefTransformed; 
			    pt3RefTransformed = POSES[in_the_segment] * pt3Ref + translations[in_the_segment]; 
			    
			    
			    /*
			    double pt3Ref_[3]; 
			    pt3Ref_[0] = pt3Ref[0]; 
			    pt3Ref_[1] = pt3Ref[1]; 
			    pt3Ref_[2] = pt3Ref[2]; 
			    double pt3XXX[3];
			    double rot[3];
			    rot[0] = pose[3];
			    rot[1] = pose[4];
			    rot[2] = pose[5];
			    ceres::AngleAxisRotatePoint(rot, pt3Ref_, pt3XXX); 
			    pt3XXX[0] += translation[0];
			    pt3XXX[1] += translation[1];
			    pt3XXX[2] += translation[2];
			    pt3RefTransformed[0] = pt3XXX[0]; 
			    pt3RefTransformed[1] = pt3XXX[1]; 
			    pt3RefTransformed[2] = pt3XXX[2]; 
			    */
			    
			    
			    if (pt3RefTransformed[2] > 0.001f)
			    {
			      
                              // project 3d point to 2d  
                              // pinhole camera projection 
                              float px = (fx * pt3RefTransformed[0] / pt3RefTransformed[2]) + cx;
                              float py = (fy * pt3RefTransformed[1] / pt3RefTransformed[2]) + cy;
                              

			      if ( (px >= 0.0f) && (py >= 0.0f) && (px <= (float)(w-1)) && (py <= (float)(h-1)) )
				{
				  
				  int index_cur = round(py) * w + round(px); 
				  
				  if ( depthCur[index_cur] > 0.001f )
				  {
				  CORRESP(y, x) = round(py) * w + round(px); 
				  }
				  else 
				    CORRESP(y, x) = -1;
				  
				}
				else{
				  CORRESP(y, x) = -1;  
				};
			      
			      //}
			      //else 
			      //  CORRESP(y, x) = -1;  
			      
			    }
			    else{     
			      CORRESP(y, x) = -1; 
			    }
			    
			    
			  } // y, x 
  
		}






//
// ### compute correspondences from the reference to the current image
//
// returns, which point corresponds to a (x, y) regular point of the reference!

void update_correspondences(uint w, 					// width 
			    uint h, 					// height 
			    const float* depthRef, 			// 
			    const float* depthCur, 			// 
			    Eigen::Matrix<double, 6, 1> &pose, 
			    Eigen::MatrixXi &CORRESP, 
			    const double* intrinics){ 
			
			Eigen::Matrix3d POSE; 
			
			// Eigen::MatrixXi &CORRESP
			CORRESP = Eigen::MatrixXi::Zero(h, w); 
			Eigen::Vector3d translation = pose.topRows<3>(); 
			Eigen::Vector3d rot = pose.bottomRows<3>(); 
			double angle = rot.norm();
			
			if (angle < 0.00001f){
			  POSE = Eigen::Matrix3d::Identity(); 
			}
			else {
			  Eigen::Vector3d axis = rot.normalized(); 
			  Eigen::AngleAxisd bb(angle, axis); 
			  POSE = bb.toRotationMatrix(); 
			}
			
			//cout << POSE << endl; 
			//char t ; cin >> t; 
			
			// camera intrinsics
			double fx = intrinics[0];
			double fy = intrinics[1];
			double cx = intrinics[2];
			double cy = intrinics[3];
			
			for (uint y = 0; y < h; y++)
			  for (uint x = 0; x < w; x++){
			    
			    float dRef = depthRef[y*w + x]; 
			    
			    // project 2d point back into 3d using its depth
			    double x0 = (x - cx) / fx;
			    double y0 = (y - cy) / fy;
			    
			    // transform reference 3d point into current frame
			    Eigen::Vector3d pt3Ref;
			    pt3Ref[0] = x0 * dRef;
			    pt3Ref[1] = y0 * dRef;
			    pt3Ref[2] = dRef;
			    
			    Eigen::Vector3d pt3RefTransformed; 
			    //pt3RefTransformed = POSE * pt3Ref + translation; 
			    
			    
			    
			    
			    double pt3Ref_[3]; 
			    pt3Ref_[0] = pt3Ref[0]; 
			    pt3Ref_[1] = pt3Ref[1]; 
			    pt3Ref_[2] = pt3Ref[2]; 
			    
			    
			    double pt3XXX[3];
			    
			    double rot[3];
			    rot[0] = pose[3];
			    rot[1] = pose[4];
			    rot[2] = pose[5];
			    
			    
			    ceres::AngleAxisRotatePoint(rot, pt3Ref_, pt3XXX); 
			    
			    pt3XXX[0] += translation[0];
			    pt3XXX[1] += translation[1];
			    pt3XXX[2] += translation[2];
			    
			    pt3RefTransformed[0] = pt3XXX[0]; 
			    pt3RefTransformed[1] = pt3XXX[1]; 
			    pt3RefTransformed[2] = pt3XXX[2]; 
			    
			    
			    
			    if (pt3RefTransformed[2] > 0.001f)
			    {
			      
                              // project 3d point to 2d  
                              // pinhole camera projection 
                              float px = (fx * pt3RefTransformed[0] / pt3RefTransformed[2]) + cx;
                              float py = (fy * pt3RefTransformed[1] / pt3RefTransformed[2]) + cy;
                              

			      if ( (px >= 0.0f) && (py >= 0.0f) && (px <= (float)(w-1)) && (py <= (float)(h-1)) )
				{
				  
				  int index_cur = round(py) * w + round(px); 
				  
				  if ( depthCur[index_cur] > 0.001f )
				  {
				  CORRESP(y, x) = round(py) * w + round(px); 
				  }
				  else 
				    CORRESP(y, x) = -1;
				  
				}
				else{
				  CORRESP(y, x) = -1;  
				};
			      
			      //}
			      //else 
			      //  CORRESP(y, x) = -1;  
			      
			    }
			    else{     
			      CORRESP(y, x) = -1; 
			    }
			    
			    
			  } // y, x
			  
			  
			  /*
			  uint num_minone = 0;
			  
			  for (uint i = 0; i < h; i++)
			    for (uint j = 0; j < w; j++)
			    {
			      if ( CORRESP(i, j) == -1)
				num_minone++;
			    }
			  
			  cout << "num_minone: " << num_minone << endl; 
			  */
			  
}



class DvoErrorDepthICP{
  
public: 
  
  // constructor
  DvoErrorDepthICP(		    const int w,                // width
                                    const int h,                // height
                                    const float* grayRef,       // reference brightness
                                    const float* depthRef,      // reference depth
                                    const float* grayCur,       // current brightness
                                    const float* depthCur,      // current depth
                                    const double* intrinics,    // intrinsic parameters
                                    const int x,                // x-coordinate, or column
                                    const int y,		// y-coordinate, or row
				    const int x_corr,		// x-coordinate of corresp. point
				    const int y_corr, 		// y-coordinate of corresp. point
                                    const float nx, 		// normal, x coordinate
				    const float ny, 		// normal, y coordinate
				    const float nz		// normal, z coordinate
  ) :  
		    w_(w),
                    h_(h),
                    depthRef_(depthRef),
                    depthCurrent_(depthCur),
                    grayRef_(grayRef),
                    grayCur_(grayCur),
                    intrinics_(intrinics),
                    x_(x),
                    y_(y),
                    x_corr_(x_corr), 
                    y_corr_(y_corr), 
                    nx_(nx),
                    ny_(ny),
                    nz_(nz)
  {}
  
  
  virtual ~DvoErrorDepthICP()
  {}
  
  
  template <typename T>
  bool operator()(const T* const pose, T* residuals) const
  {
		// rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
                
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);
    
		// get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);					// 
		
		T dCur; 
		if ( (y_corr_ != -1) && (x_corr_ != -1) )
		  dCur = T(depthCurrent_[y_corr_*w_ + x_corr_]); 			// 
		else 
		  dCur = T(0.0); 
		
		
		//cout << "dRef " << dRef << endl; 
		//cout << "dCur " << dCur << endl; 
		
		// check if the depth values are positive
                if ( ( dRef > T(0.0) ) && ( dCur > T(0.0) ) && (y_corr_ != -1) && (x_corr_ != -1) )
                {
		
		    //cout << "entered if..." << endl; 
		  
		    // project 2d cur and ref points back into 3d using its depth
                    T x0 = (T(x_) - cx) / fx;
                    T y0 = (T(y_) - cy) / fy;
		    
                    // transform reference 3d point into current frame
                    T pt3Ref[3];
                    pt3Ref[0] = x0 * dRef;
                    pt3Ref[1] = y0 * dRef;
                    pt3Ref[2] = dRef;
                    
                    // rotate
                    T pt3Cur[3];
                    ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
                    
		    //cout << "pt3Ref: " << pt3Ref[0] << " " << pt3Ref[1] << " " << pt3Ref[2] << endl; 
		    //cout << "pt3Cur: " << pt3Cur[0] << " " << pt3Cur[1] << " " << pt3Cur[2] << endl; 
		    
		    
		    //cout << "pt3Cur[2]: " << pt3Cur[2] << endl; 
		    
                    // translate
                    pt3Cur[0] += t[0];
                    pt3Cur[1] += t[1];
                    pt3Cur[2] += t[2];
		    
		    // 
		    
		    T x1 = (T(x_corr_) - cx) / fx; 
                    T y1 = (T(y_corr_) - cy) / fy; 
		    
		    
		    T pt3Init[3];
		    pt3Init[0] = x1 * dCur;		// wrong correspondence?.. 
                    pt3Init[1] = y1 * dCur; 		// wrong correspondence?..
                    pt3Init[2] = dCur; 
		    
		    
		    //double norm_norm = sqrt(nx_* nx_ + ny_* ny_ + nz_* nz_);
		    
		    //cout << norm_norm << endl; 
		    
		    
		    if (  ( pt3Cur[2] > T(0.0) ) && ( pt3Init[2] > T(0.0) ) )   // && (norm_norm > 0.001f)
                    {
		      
		      T diff_val[3]; 
		      
		      /*
		      diff_val[0] = pt3Cur[0] - pt3Init[0]; 
		      diff_val[1] = pt3Cur[1] - pt3Init[1]; 
		      diff_val[2] = pt3Cur[2] - pt3Init[2]; 
		      */
		      
		      diff_val[0] = pt3Init[0] - pt3Cur[0]; 
		      diff_val[1] = pt3Init[1] - pt3Cur[1]; 
		      diff_val[2] = pt3Init[2] - pt3Cur[2]; 
		      
		      
		      T norm_[3]; 
		      norm_[0] = T(nx_);
		      norm_[1] = T(ny_);
		      norm_[2] = T(nz_);
		      
		      residuals[0] = T( ceres::DotProduct(diff_val, norm_) ); 
		      
		      //cout << residuals[0] << endl; 
		      
		      //residuals[0] = diff_val[0] * norm_[0] + diff_val[1] * norm_[1] + diff_val[2] * norm_[2];
		      return true; 
		      
		      
		    } 
		
		}
		
	// invalid depth lookup
        residuals[0] = T(INVALID_RESIDUAL);
        return true;

    
  }

  
private:
	
	const int w_;
        const int h_;
        const float* depthRef_;
        const float* depthCurrent_;
        const float* grayRef_;
        const float* grayCur_;
        const double* intrinics_;
        const int x_; 
        const int y_; 
	const int x_corr_;
	const int y_corr_;
	const float nx_; 
	const float ny_; 
	const float nz_; 
  
};





class DvoErrorRegularization
{
  
public: 
      
      DvoErrorRegularization() //: weights(_weights_), index(_index_);  const double* _weights_, uint _index_
      {}
      
      
      virtual ~DvoErrorRegularization()
      {}
            
      
      template <typename T>
      bool operator()(const T* const pose1, const T* const pose2, T* residuals) const
      {
	 
	T rot1[3], t1[3], rot2[3], t2[3], diff_val[6];
	
	rot1[0] = pose1[3];
	rot1[1] = pose1[4];
	rot1[2] = pose1[5];
                
	t1[0] = pose1[0];
	t1[1] = pose1[1];
	t1[2] = pose1[2];
	
	rot2[0] = pose2[3];
	rot2[1] = pose2[4];
	rot2[2] = pose2[5];
        
	t2[0] = pose2[0]; 
	t2[1] = pose2[1]; 
	t2[2] = pose2[2]; 
	
	diff_val[0] = abs(rot1[0] - rot2[0]); 
	diff_val[1] = abs(rot1[1] - rot2[1]); 
	diff_val[2] = abs(rot1[2] - rot2[2]); 
	diff_val[3] = abs(t1[0] - t2[0]); 
	diff_val[4] = abs(t1[1] - t2[1]); 
	diff_val[5] = abs(t1[2] - t2[2]); 
	
	/*
	residuals[0] = T( diff_val[0]*diff_val[0] + diff_val[1]*diff_val[1] + diff_val[2]*diff_val[2] 
	+ diff_val[3]*diff_val[3] + diff_val[4]*diff_val[4] + diff_val[5]*diff_val[5]   ); 
	*/
	
#if 0	
	residuals[0] = T ( sqrt(sqrt(diff_val[0]*diff_val[0] + diff_val[1]*diff_val[1] + diff_val[2]*diff_val[2] 
	+ diff_val[3]*diff_val[3] + diff_val[4]*diff_val[4] + diff_val[5]*diff_val[5]) ) ); 
	
	//residuals[0] = diff_val[0] + diff_val[1] + diff_val[2] + diff_val[3] + diff_val[4] + diff_val[5]; 
	
#else	
	
	residuals[0] = diff_val[0]; 
	residuals[1] = diff_val[1]; 
	residuals[2] = diff_val[2]; 
	residuals[3] = diff_val[3]; 
	residuals[4] = diff_val[4]; 
	residuals[5] = diff_val[5]; 
	
#endif
	
	return true; 
	
      }
      
      
//private: 
  
  // vector<int> offsets_; 
  // const double* poses_; 
  
  
};


class DvoErrorRegularizationWeighted
{
  
public: 
      
      DvoErrorRegularizationWeighted() //: weights(_weights_), index(_index_);  const double* _weights_, uint _index_
      {}
      
      
      virtual ~DvoErrorRegularizationWeighted()
      {}
            
      
      template <typename T>
      bool operator()(const T* const pose1, const T* const pose2, const T* const w_ij, T* residuals) const
      {
	 
	T rot1[3], t1[3], rot2[3], t2[3], diff_val[6];
	
	rot1[0] = pose1[3];
	rot1[1] = pose1[4];
	rot1[2] = pose1[5];
                
	t1[0] = pose1[0];
	t1[1] = pose1[1];
	t1[2] = pose1[2];
	
	rot2[0] = pose2[3];
	rot2[1] = pose2[4];
	rot2[2] = pose2[5];
        
	t2[0] = pose2[0]; 
	t2[1] = pose2[1]; 
	t2[2] = pose2[2]; 
	
	diff_val[0] = rot1[0] - rot2[0]; 
	diff_val[1] = rot1[1] - rot2[1]; 
	diff_val[2] = rot1[2] - rot2[2]; 
	diff_val[3] = t1[0] - t2[0]; 
	diff_val[4] = t1[1] - t2[1]; 
	diff_val[5] = t1[2] - t2[2]; 
	
	/*
	residuals[0] = T( diff_val[0]*diff_val[0] + diff_val[1]*diff_val[1] + diff_val[2]*diff_val[2] 
	+ diff_val[3]*diff_val[3] + diff_val[4]*diff_val[4] + diff_val[5]*diff_val[5]   ); 
	*/
	
#if 0	
	residuals[0] = T ( sqrt(sqrt(diff_val[0]*diff_val[0] + diff_val[1]*diff_val[1] + diff_val[2]*diff_val[2] 
	+ diff_val[3]*diff_val[3] + diff_val[4]*diff_val[4] + diff_val[5]*diff_val[5]) ) ); 
	
	//residuals[0] = diff_val[0] + diff_val[1] + diff_val[2] + diff_val[3] + diff_val[4] + diff_val[5]; 
	
#else	

	T w_ij_ = w_ij[0] * w_ij[0]; 
	
	// if ( w_ij_ < T(0.0) )
	//  w_ij_ = T(0.0); 
	
	residuals[0] = w_ij_ * diff_val[0]; 
	residuals[1] = w_ij_ * diff_val[1]; 
	residuals[2] = w_ij_ * diff_val[2]; 
	residuals[3] = w_ij_ * diff_val[3]; 
	residuals[4] = w_ij_ * diff_val[4]; 
	residuals[5] = w_ij_ * diff_val[5]; 
	
#endif
	
	return true; 
	
      }
      
      
private: 
  
  const double* weights; 
  uint index; 
  
  
  // vector<int> offsets_; 
  // const double* poses_; 
  
  
};



class DvoErrorDepthVariation
{

    public: 
        
        // constructor
        DvoErrorDepthVariation(     const int w,                // width
                                    const int h,                // height
                                    const float* grayRef,       // reference brightness
                                    const float* depthRef,      // reference depth
                                    const float* grayCur,       // current brightness
                                    const float* depthCur,      // current depth
                                    const double* intrinics,    // intrinsic parameters
                                    const int x,                // x-coordinate, or column
                                    const int y			// y-coordinate, or row
			      ) :              
                    w_(w),
                    h_(h),
                    depthRef_(depthRef),
                    depthCurrent_(depthCur),
                    grayRef_(grayRef),
                    grayCur_(grayCur),
                    intrinics_(intrinics),
                    x_(x),
                    y_(y)
    {
    }
     
     
             // destructor
    virtual ~DvoErrorDepthVariation()
    {
    }
    
     
    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
        
                // rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
                
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);
                
                // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
                T dCur = T(depthCurrent_[y_*w_ + x_]); 
                
                // check if the depth values are positive
                if ( ( dRef > T(0.0) ) && ( dCur > T(0.0) ) )
                {
                    
                    // project current 3d point directly 

                    // project 2d point back into 3d using its depth
                    T x0 = (T(x_) - cx) / fx;
                    T y0 = (T(y_) - cy) / fy;
                
                    // transform reference 3d point into current frame
                    T pt3Ref[3];
                    pt3Ref[0] = x0 * dRef;
                    pt3Ref[1] = y0 * dRef;
                    pt3Ref[2] = dRef;
                    
                    // rotate
                    T pt3Cur[3];
                    ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
                    
                    // translate
                    pt3Cur[0] += t[0];
                    pt3Cur[1] += t[1];
                    pt3Cur[2] += t[2];
                    
                    
                    if (pt3Cur[2] > T(0.0))
                    {
                                // project 3d point to 2d  
                                // pinhole camera projection 
                                T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
                                T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                                
                        if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                        {
                            
                            typedef ceres::Grid2D<float, 1> ImageDataType;
                            
                            ImageDataType array(depthCurrent_, 0, h_, 0, w_);
                            
                            
                            // ### possibly we need to change the interpolation...
                            
                            ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                            
                            T valCur = T(0.0);
                            
                            interpolator.Evaluate(py, px, &valCur);
                            
                            //interpolate_depth<T>(py, px, valCur); 
                            
                            
                            if (valCur >= T(0.0))
                            {
                                // compute depth variation residual
                                T valRef = T(depthRef_[y_*w_ + x_]);
                                residuals[0] = valRef - valCur;
                                return true; 
                            }
                        }
                    }  
                }
        
                // invalid depth lookup
                residuals[0] = T(INVALID_RESIDUAL);
                return true;
        
    }
    
    
    private: 
        
        const int w_;
        const int h_;
        const float* depthRef_;
        const float* depthCurrent_;
        const float* grayRef_;
        const float* grayCur_;
        const double* intrinics_;
        const int x_;
        const int y_;
        
};




class DR_Robust_Optimizer_Error{
  
public: 
  
  DR_Robust_Optimizer_Error() 
  {
  }
  
  virtual ~DR_Robust_Optimizer_Error()
  {
  }
  
  template <typename T>
  bool operator()(const T* const weight_, T* residuals_) const
  {
    
    T valCur = T(1.0) - T(weight_[0] * T(weight_[0]) );   //  * T(weight_[0])
    residuals_[0] = valCur; 
    
    // invalid depth lookup
    // residuals_[0] = T(INVALID_RESIDUAL); 
    return true; 
    
  }
  
  
//private: 
  
  
  
};




class Chained_Pose_Regularizer{
  
  public:
  
    
  Chained_Pose_Regularizer(const int num_chained_poses_) : num_chained_poses(num_chained_poses_) {}
  
  virtual ~Chained_Pose_Regularizer(){}
  
  
  // always returns 6 residuals
  // variable number of poses can be given through poses
  
  template <typename T>
  bool operator()(T const* const* poses, T* residuals_) const
  {
	
	T r0[3]; 
	
	r0[0] = poses[0][3]; 
	r0[1] = poses[0][4]; 
	r0[2] = poses[0][5]; 
	
	T ROT_ACC[9]; 
	ROT_ACC[0] = T(1.0); 
	ROT_ACC[1] = T(0.0); 
	ROT_ACC[2] = T(0.0); 
	ROT_ACC[3] = T(0.0); 
	ROT_ACC[4] = T(1.0); 
	ROT_ACC[5] = T(0.0); 
	ROT_ACC[6] = T(0.0); 
	ROT_ACC[7] = T(0.0); 
	ROT_ACC[8] = T(1.0); 
	
	
	for (uint i = 1; i < num_chained_poses; i++){
	  
	  T cur_rot[3];
	  cur_rot[0] = poses[i][3];
	  cur_rot[1] = poses[i][4];
	  cur_rot[2] = poses[i][5];
	  
	  T R[9]; 
	  
	  ceres::AngleAxisToRotationMatrix(cur_rot, R); 
	  
	  
	  // ### concat matrices
	  
	  T R_PREV[9]; 
	  
	  R_PREV[0] = ROT_ACC[0]; 
	  R_PREV[1] = ROT_ACC[1]; 
	  R_PREV[2] = ROT_ACC[2]; 
	  R_PREV[3] = ROT_ACC[3]; 
	  R_PREV[4] = ROT_ACC[4]; 
	  R_PREV[5] = ROT_ACC[5]; 
	  R_PREV[6] = ROT_ACC[6]; 
	  R_PREV[7] = ROT_ACC[7]; 
	  R_PREV[8] = ROT_ACC[8]; 
	  

	  ROT_ACC[0] = R[0] * R_PREV[0] + R[1] * R_PREV[3] + R[2] * R_PREV[6]; 
  	  ROT_ACC[1] = R[0] * R_PREV[1] + R[1] * R_PREV[4] + R[2] * R_PREV[7]; 
	  ROT_ACC[2] = R[0] * R_PREV[2] + R[1] * R_PREV[5] + R[2] * R_PREV[8]; 
	  
	  ROT_ACC[3] = R[3] * R_PREV[0] + R[4] * R_PREV[3] + R[5] * R_PREV[6]; 
	  ROT_ACC[4] = R[3] * R_PREV[1] + R[4] * R_PREV[4] + R[5] * R_PREV[7]; 
	  ROT_ACC[5] = R[3] * R_PREV[2] + R[4] * R_PREV[5] + R[5] * R_PREV[8]; 
	  
	  ROT_ACC[6] = R[6] * R_PREV[0] + R[7] * R_PREV[3] + R[8] * R_PREV[6]; 
	  ROT_ACC[7] = R[6] * R_PREV[1] + R[7] * R_PREV[4] + R[8] * R_PREV[7]; 
	  ROT_ACC[8] = R[6] * R_PREV[2] + R[7] * R_PREV[5] + R[8] * R_PREV[8]; 
	   
	}
	
	T aa_acc[3]; 
	
	ceres::RotationMatrixToAngleAxis(ROT_ACC, aa_acc); 
	
	residuals_[0] =  r0[0] - aa_acc[0];
	residuals_[1] =  r0[1] - aa_acc[1];
	residuals_[2] =  r0[2] - aa_acc[2];
	
	//residuals_[0] = T(0.0); 
	//residuals_[1] = T(0.0); 
	//residuals_[2] = T(0.0); 
	
	
	// ### translation
	
	T t0[3]; 
	
	t0[0] = poses[0][0];
	t0[1] = poses[0][1];
	t0[2] = poses[0][2];
	
	for (uint i = 1; i < num_chained_poses; i++){
	  
	  t0[0] +=  poses[i][0]; 
	  t0[1] +=  poses[i][1]; 
	  t0[2] +=  poses[i][2]; 
	}
	
	
	// residuals_[0] = t0[0]; 
	// residuals_[0] = t0[1]; 
	// residuals_[0] = t0[2]; 
	
	residuals_[3] = t0[0]; 
	residuals_[4] = t0[1]; 
	residuals_[5] = t0[2]; 
      
    return true; 
    
  }
  
  
private: 
  
  const int num_chained_poses; 
  
};





/*

class Simple_Chained_Regularizer{
  
public: 
  
  Simple_Chained_Regularizer(){}; 
  
  virtual ~Simple_Chained_Regularizer(){}; 
  
  
  template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
      

    }

  
  private: 
  
  const float *acc_pose;   
    
  
}; 

*/






#endif
