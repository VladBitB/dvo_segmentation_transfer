
#ifndef HEADERFILE_H
#define HEADERFILE_H

//
// ### HEADERS
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

#include <unistd.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Geometry>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ceres/autodiff_cost_function.h>
#include <ceres/dynamic_autodiff_cost_function.h>
#include "ceres/numeric_diff_cost_function.h"
#include "ceres/cost_function_to_functor.h"
#include "ceres/rotation.h"
//#include <ceres/cubic_interpolation.h>
#include "./cubic_interpolation_.h"
#include <ceres/loss_function.h>
#include <ceres/problem.h>
#include <ceres/rotation.h>
#include <ceres/solver.h>


#include <sophus/so3.hpp>

// basic rgbd processing framework
#include "./rgbd_framework/src/tum_benchmark.h"
#include "./rgbd_framework/src/rgbd_processing.h"

#include "./ply_module_ext/ply_module.hpp"


// settings
#include "./dvo_ceres_settings/dvo_ceres_settings.hpp"

#include "./DvoErrorAutoDepth.hpp"
#include "./DvoErrorDepthVariation.hpp"
#include "./DvoErrorPhotometric.hpp"
#include "./aux.hpp"
#include "./load.hpp"
#include "./segmentation.hpp"

#include "./slic.h"

// 
// ### MACROS 
//

#define INVALID_RESIDUAL    0.0

#define NO_FILTER           0
#define AVG_FILTER          1
#define MEDIAN_FILTER       2
#define BILATERAL_FILTER    3

#define NUM_LEVELS          1
#define ALPHA               0.0f
#define BETA                1.0f
#define FILTER              AVG_FILTER              // is not currently in use
//#define FILTER            NO_FILTER

#define NO_SEGMENTS	    0
#define TEST_SEGMENTS	    1
#define SEGMENTS_ENABLED    2
#define REGULAR_SEGMENTS    4
#define SEGMENTS_SLIC	    5


#define OPTIMIZATION_TWO_FRAMES 1		// 
#define OPTIMIZATION_GLOBAL	2		// 



// 
// compute visualizations 
// 
int visualize_segmentation(vector<cv::Mat> &segmentations, vector<cv::Mat> &vizualizations, int n_segments){ 
  
  std::mt19937 rng; 
  rng.seed(std::random_device()()); 
  std::uniform_int_distribution<std::mt19937::result_type> dist255(1, 255);
  
  // generate n_segments random colors
  Eigen::MatrixXi Colors(n_segments, 3); 
  
  for (int i = 0; i < n_segments; i++){
    Colors(i, 0) = dist255(rng); 
    Colors(i, 1) = dist255(rng); 
    Colors(i, 2) = dist255(rng); 
  }
  
  /*
  Eigen::Vector3i ColorO; 
  ColorO[0] = dist255(rng); 
  ColorO[1] = dist255(rng);
  ColorO[2] = dist255(rng);
  */
  
  int n_lvl = segmentations.size(); 
  
  for (int l = 0; l < n_lvl; l++){
    
    int height	= segmentations[l].rows; 
    int width 	= segmentations[l].cols; 
    
    cv::Mat current_segmentation(height, width, CV_8UC3, cv::Scalar(0, 0, 0)); 
    
    for (int i = 0; i < height; i++)
      for (int j = 0; j < width; j++){
	
	if (segmentations[l].at<int>(i, j) != -1){
	  //cout << segmentations[l].at<int>(i, j) << endl; 
	  int segm = segmentations[l].at<int>(i, j); 
	  cv::Vec3b &color = current_segmentation.at<cv::Vec3b>(i,j); 
	  //cout << segm << endl; 
	  color[0] = Colors(segm, 0);
	  color[1] = Colors(segm, 1);
	  color[2] = Colors(segm, 2);
	}
	/*
	else{
	  cv::Vec3b &color = current_segmentation.at<cv::Vec3b>(i,j); 
	  color[0] = ColorO(0);
	  color[1] = ColorO(1);
	  color[2] = ColorO(2);
	}
	*/
      }
      vizualizations.push_back(current_segmentation);     
  }
  
  return 0; 
  
}




// 
// convert redundant segment representation to the compact representation
// 
int segmentation(std::vector<std::vector<cv::Mat> > &segment_masks, vector<cv::Mat> &segmentations){ 
  
  int num_levels 	= segment_masks[0].size(); 
  int num_segments 	= segment_masks.size(); 
    
  for (uint lvl = 0; lvl < num_levels; lvl++){
    
    vector<cv::Mat> segments_lvl(num_segments); 
    
    for (uint i = 0; i < num_segments; i++){
      segments_lvl[i] = segment_masks[i][lvl]; 			// lvl is fixed        
    }
    
    int width = segments_lvl[0].cols; 
    int height = segments_lvl[0].rows;
    
    cv::Mat current_segmentation(height, width, CV_32S, cv::Scalar(0, 0, 0)); 
    
    for (uint i = 0; i < height; i++)
      for (uint j = 0; j < width; j++){
    
	current_segmentation.at<int>(i, j) = -1; 
	for (int s = 0; s < segments_lvl.size(); s++){ 
	  if ( segments_lvl[s].at<uchar>(i, j) == 255 ){
	    current_segmentation.at<int>(i, j) = s; 
	    break; 
	  }  
	}
      }
     
    segmentations.push_back(current_segmentation); 
    
  }
  
  return 0; 
  
}






// build adjacency matrix 
  
int adjacency_matrix(Eigen::SparseMatrix<int, Eigen::RowMajor> &adjm, vector<cv::Mat> segmentations, int n_segments, const int N, cv::Mat depthRef_){ 
  
  
  // ### compare average depths and threshold the depths...
    
  int n = n_segments; 
  //adjm = Eigen::MatrixXi::Zero(n, n); 
  
  
  // ### no initialization is required, because adjm is sparse
  
  // ### later, if required: figure out what is an efficient way to compute adjacency matrix
  // ### find central point of every segment...
  // ### add 4 closest segments to the adjacency matrix...
  
  
  Eigen::MatrixXd Segment_Centers(n_segments, 2); 
  
  Eigen::VectorXd average_depths(n_segments); 
  
  vector<vector<int> > centers_x(n_segments); 
  vector<vector<int> > centers_y(n_segments); 
  
  vector<vector<double>> _depths(n_segments); 
  
  // -> use vector and then map to Eigen::Vector
  // ### how to push doppelte values...
  
  
  const float* depthref = (const float*)depthRef_.data; 
  
  for (uint i = 0; i < segmentations[0].rows; i++)
    for (uint j = 0; j < segmentations[0].cols; j++){
      
      int segm = segmentations[0].at<int>(i, j); 
      
      //cout << segm << endl; 
      
      if ( segm != -1 ){
	centers_y[segm].push_back(i);
	centers_x[segm].push_back(j); 
	
	_depths[segm].push_back(depthref[i * segmentations[0].cols + j]);
	
      }
    }
    
  for (uint i = 0; i < n_segments; i++){
    
    //Eigen::Vector y_vector(centers_y[i].size());
    //Eigen::Map<Eigen::RowVectorXi> y_vector(&(centers_y[i]), centers_y[i].size()); 
    //Eigen::Map<Eigen::RowVectorXi> x_vector(&(centers_x[i]), centers_x[i].size()); 
    
    
    Eigen::VectorXi y_vector = Eigen::VectorXi::Map(&centers_y[i][0], centers_y[i].size());	// TODO bytes or elements?..
    Eigen::VectorXi x_vector = Eigen::VectorXi::Map(&centers_x[i][0], centers_x[i].size());
    
    Eigen::VectorXd average_depth = Eigen::VectorXd::Map(&_depths[i][0], _depths[i].size()); 
    
    /*
    cout << "n_segments: " << n_segments << endl; 
    cout << centers_y[i].size() << endl; 
    cout << centers_x[i].size() << endl; 
    cout << y_vector.size() << endl; 
    cout << x_vector.size() << endl; 
    */
    
    // char j; cin >> j; 
    
    Segment_Centers(i, 0) = y_vector.mean(); 
    Segment_Centers(i, 1) = x_vector.mean(); 
    
    average_depths[i] = average_depth.mean(); 
    
    
  }
    
    // ### alternatively: compute a median point; average works in all cases for convex regions
    
    //cout << Segment_Centers << endl; 
    //char j; cin >> j; 
    
    
    
    for (int i = 0; i < n_segments; i++){
      
      int current_y = Segment_Centers(i, 0); 
      int current_x = Segment_Centers(i, 1); 
      
      // sort the centers according to the Euclidean distance from the current point
      
      std::map<double, int> euclidean_distances; 
      for (int j = 0; j < n_segments; j++){
	if (i != j){
	
	  double euclidean_distance = sqrt(  ( current_y - Segment_Centers(j, 0) ) * ( current_y - Segment_Centers(j, 0) ) + 
	  			      ( current_x - Segment_Centers(j, 1) ) * ( current_x - Segment_Centers(j, 1) )  ); 
	  
	  // perturb euclidean_distance to account for the case 
	  // if there are two identical euclidean distances in the map (otherwise will not be added)
	  std::mt19937 rng; 
	  rng.seed(std::random_device()()); 
	  std::uniform_int_distribution<std::mt19937::result_type> dist255(1, 256); 
	  double perturb = (double)j / double(n_segments * 10.0f); 
	  
	  euclidean_distance += perturb;
	  
	  //if ( fabs(  average_depths[i] - average_depths[j]  ) < 0.1f )
	    euclidean_distances.insert(std::pair<double, int>(euclidean_distance, j)); 
	    
	}
      }
      
      
      /*
      typedef map<double, int>::const_iterator mi;
      for (mi iter = euclidean_distances.begin(); iter != euclidean_distances.end(); iter++){
	cout << iter->first << " " << iter->second << endl; 
      }
      */

      // char j; cin >> j; 
      
      
      // ### fill adjm
      
      
      //for (it=mymap.begin(); it!=mymap.end(); ++it)
      //  std::cout << it->first << " => " << it->second << '\n';
      
      /*
      if (i == 62){
	
	cout << " i == 62" << endl; 
	
	typedef map<double, int>::const_iterator mi;
	for (mi iter = euclidean_distances.begin(); iter != euclidean_distances.end(); iter++){
	  cout << iter->first << " " << iter-> second << endl; 
	  
	  
	}

	char f; cin >> f; 
      }
      */
      
      
      
      // ### add N segments to the adjm
      
      int counter = 0; 
      
      typedef map<double, int>::const_iterator mi;
      for (mi iter = euclidean_distances.begin(); iter != euclidean_distances.end(); iter++){
	
	if (counter >= N)
	  break; 
	
	int neighbour = iter->second; 
	adjm.insert(i, neighbour) = 1; 
	
	counter++; 
	
      }
        
    } // loop over the segments
    
    
  return 0; 
  
}





vector<int> compute_and_save_mesh(cv::Mat &depthRef, Eigen::Matrix3d K, string mesh_file_name); 

// mesh_file_name -> name of the temporary file
// 
// 
// 
// 
int segment_and_fill_segments(string mesh_file_name, 
			      string output_dir, 
			      cv::Mat &depthRef, 
			      Eigen::Matrix3d K, 
			      int rows_, 
			      int cols_, 
			      uint &num_segments, 
			      const float kthr, 
			      const int minSegVerts, 
			      const uint num_elem_threshold, 
			      std::vector<cv::Mat> &cv_segments, 
			      cv::Mat &outliers
 			    ){
  
  //const float kthr = 50; 							// graph cut k threshold 
  //const int minSegVerts = 500;
  
  
	  vector<int> num_elements_per_segment;
	  
	  //string mesh_file_name = "./RESULT/MESH_REF.ply"; 
	  
	  // compute mesh from the first (reference) point cloud
	  // lookup maps (row, col) coordinate to a vertex in a mesh 
	  // lookup is required because not all vertices are included 
	  // into the mesh 
	  vector<int> lookup = compute_and_save_mesh(depthRef, K, mesh_file_name); 
	  
	  // segment the mesh using Felzenszwalb algorithm
	  //const float kthr = 50; 							// graph cut k threshold 
	  //const int minSegVerts = 500; 							// min number of points in a segment
	  const std::vector<int> comps = segment(mesh_file_name, kthr, minSegVerts); 
	  
	  // save a coloured ply segmentation
	  std::string coloured_file = output_dir + "/SEGMENTED.ply"; 
	  visualize_segmentation(comps, mesh_file_name, coloured_file); 
	  
	  cout << "mesh written and segmented..." << endl; 	  
	  cout << "computing segments" << endl; 
	  
	  // create segmentation masks 
	  // create a new segmentation mask associated with a number (index converter...) 
	  
	  std::map<int, uint> segm_key; 
	  for (uint i = 0; i < comps.size(); i++){ 
	    int current_value = comps[i]; 
	    if ( segm_key.insert(std::make_pair(current_value, num_segments)).second == true )
	      num_segments++; 
	  }
	  
	  
	  num_elements_per_segment.reserve(num_segments); 
	  num_elements_per_segment.assign(num_segments, 0); 
	  
	  // ### remove all segments which are smaller than 10000 pixels...
	  for (uint i = 0; i < rows_ * cols_; i++){
	    
	    int segment_id = segm_key.find(comps[lookup[i]])->second; 
	    num_elements_per_segment[segment_id]++; 

	  }
	    
	  
	  //uint num_elem_threshold = 5000; 
	  uint n_suitable_segments = 0; 
	  
	  for (uint i = 0; i < num_segments; i++){
	    //cout << num_elements_per_segment[i] << endl;  
	    if (num_elements_per_segment[i] > num_elem_threshold){
	      // set corresponding lookup values to -1
	      n_suitable_segments++; 
	    }
	  }
	  
	  cout << "n_suitable_segments: " << n_suitable_segments << endl; 
	  
	  
	  
	  
	  
	  
	  //char k; cin >> k; 
	  
	  // ###   
	  
	  std::vector<cv::Mat> cv_segments_temp; 
	  
	  
	  for (uint i = 0; i < num_segments; i++)
	    cv_segments_temp.push_back( cv::Mat(rows_, cols_, CV_8UC1, cv::Scalar(0)) ); 
	    
	  outliers = cv::Mat(rows_, cols_, CV_8UC1, cv::Scalar(0));
	  
	  
	  cout << "comps.size() " << comps.size() << endl; 
	  
	   
	  for (uint i = 0; i < rows_ * cols_; i++){
	    
	    if (lookup[i] != -1){
	      
	      uint c_segm = segm_key.find(comps[lookup[i]])->second; 
	      uint r = trunc( i / cols_); 
	      uint c = i % cols_; 
	      
	      cv_segments_temp[c_segm].at<uchar>(r, c) = 255; 
 
	    }
	    else{
	      
	      uint r = trunc( i / cols_); 
	      uint c = i % cols_; 
	      
	      outliers.at<uchar>(r, c) = 255; 
	      
	    }
	    
	    
	    
	    
	    
	    
	    
	  }
	    
	  
	   
	  
	  
	  // 
	  
	  cout << "num_segments: " << num_segments << endl; 
	  cout << "done " << endl; 
	  
	  //char c; cin >> c;

	  //num_segments = n_suitable_segments; 
	  
	  /*
	  for (uint i = 0; i < n_suitable_segments; i++){
	    cv_segments.push_back( cv::Mat(grayRefPyr[0].rows, grayRefPyr[0].cols, CV_8UC1, cv::Scalar(0)) );  
	  }
	  */
	 
	 for (uint i = 0; i < num_segments; i++){
	   
	   if (num_elements_per_segment[i] > num_elem_threshold){
	     cv_segments.push_back(cv_segments_temp[i]); 
	   }
	     // declare the segment as an outlier
	   else{
	     for (uint k = 0; k < cv_segments_temp[i].rows; k++)
	       for (uint l = 0; l < cv_segments_temp[i].cols; l++){
		 if (cv_segments_temp[i].at<uchar>(k,l) == 255){
		   outliers.at<uchar>(k,l) = 255;
		 }
	      }
	   }
	 } 
	  
	  cout << "segment reassignment accomplished..." << endl; 
	  
	  
	  num_segments = n_suitable_segments; 
	  
	  
	  for (uint z = 0; z < num_segments; z++){
	    stringstream ss0; 
	    ss0.str(""); 
	    ss0 << output_dir <<  "/mask" << z << ".png"; 
	    string rt = ss0.str(); 
	    imwrite(rt, cv_segments[z]); 
	  }
	    
	  string outliers_file = output_dir + "/outliers.png"; 
	    
	  imwrite(outliers_file, outliers); 
	  
	  
	  return 0; 
	  
}




int generate_segments(uint &num_segments, uint mode, std::vector<cv::Mat> &cv_segments, uint rows_, uint cols_){ 
  
	
	if ( mode == NO_SEGMENTS ) 
	{
	  num_segments = 1; 
	  //cout << "NO_SEGMENTS" << endl;   
	}
	else if ( mode == TEST_SEGMENTS ) 
	{
	  num_segments = 2; 
	  
	}
	
	
        std::vector<std::vector<uint> > segments(num_segments); 
	
	//uint rows_ = grayRefPyr[0].rows; 
        //uint cols_ = grayRefPyr[0].cols; 
        uint num_points = rows_ * cols_; 
	
	
	cv::Mat segment1(rows_, cols_, CV_8UC1, cv::Scalar(0));  
	cv::Mat segment2(rows_, cols_, CV_8UC1, cv::Scalar(0)); 
	
	
	if ( (mode == NO_SEGMENTS) || (mode == TEST_SEGMENTS) )  
	{
        
        std::cout << num_points << std::endl; 
        
        for (uint i = 0; i < num_segments; i++){
	  
	   // ### the whole image space is a segment
           for (uint j = (i * num_points / num_segments); j < ((i+1) * num_points / num_segments); j++) {
             segments[i].push_back(j);  
           }
              
        }
        
        
        for (uint i = 0; i < segments[0].size(); i++){
            uint r = trunc( segments[0][i] / cols_); 
            uint c = segments[0][i] % cols_; 
            segment1.at<uchar>(r, c) = 255;
            //std::cout << r << " " << c << std::endl;
        }
        
        
        
        cv_segments.push_back(segment1); 
        
        }
        
	
        if ( mode == TEST_SEGMENTS ){
	  
	  
	  for (uint i = 0; i < segments[1].size(); i++){
            uint r = trunc( segments[1][i] / cols_); 
            uint c = segments[1][i] % cols_; 
            segment2.at<uchar>(r, c) = 255;
            //std::cout << r << " " << c << std::endl;
        }
	  
	  cv_segments.push_back(segment2); 
	  
	  
	}


	
  
  return 0; 
  
  
  
}









int downsample_masks(std::vector<cv::Mat> &cv_segments, std::vector<std::vector<cv::Mat> > &segment_masks, uint numLevels){
    
    // ### 
    
    uint num_segments = segment_masks.size(); 
    std::cout << "num_segments: " << num_segments << std::endl; 
    
    //char z; std::cin >> z; 
    
    for (uint s = 0; s < num_segments; s++)
        segment_masks[s].push_back(cv_segments[s]); 
    
    
    for (uint s = 0; s < num_segments; s++)
        for (uint i = 1; i < numLevels; i++){
            segment_masks[s].push_back( downsampleGray(segment_masks[s][i - 1]) );
        }
    
    //downsampleGray(); 
    
    return 0; 
    
} 



// 
// ### FILTERS
// 


// 
// bilateral filter
//
void bilateralFilter(const cv::Mat &depthIn, cv::Mat &depthOut, float sigma)
{
		
                if (depthIn.type() != CV_32FC1)
			return;

		int kernelSize = 5;
		int kernelSizeHalf = kernelSize / 2;
		if (sigma <= 0.0)
			sigma = std::sqrt(0.3 * (float(kernelSizeHalf) - 1.0) + 0.8); 

		int w = depthIn.cols;
		int h = depthIn.rows;
		float sigma2inv = 1.0 / (sigma*sigma);
		float sigmaSpace2Inv = sigma2inv;
		float sigmaRange2Inv = sigma2inv;

		cv::Mat depthMapOut = cv::Mat::zeros(h, w, depthIn.type());

		// apply bilateral filter
		float* ptrDepthIn = (float*)depthIn.data;
		float* ptrDepthOut = (float*)depthMapOut.data;
                
                #pragma omp parallel for schedule(dynamic)
		for (int y = kernelSizeHalf; y < h - kernelSizeHalf; ++y)
		{
			for (int x = kernelSizeHalf; x < w - kernelSizeHalf; ++x)
			{
				float valIn = ptrDepthIn[y*w + x];
				if (std::isnan(valIn) || valIn == 0.0f)
					continue;

				float valOut = 0.0;
				float sumWeights = 0.0;
				for (int ky = -kernelSizeHalf; ky <= kernelSizeHalf; ++ky)
				{
					for (int kx = -kernelSizeHalf; kx <= kernelSizeHalf; ++kx)
					{
						float valKernelIn = ptrDepthIn[(y + ky)*w + x + kx];
						if (std::isnan(valKernelIn) || valKernelIn == 0.0f)
							continue;
						float colorDiff = valIn - valKernelIn;
						float rangeComponent = -0.5 * double((colorDiff * colorDiff) * sigmaRange2Inv);

						float spatialComponent = -0.5 * double((kx*kx + ky*ky) * sigmaSpace2Inv);

						float weight = std::exp(spatialComponent + rangeComponent);
						valOut += weight * valKernelIn;
						sumWeights += weight;
					}
				}
				if (sumWeights > 0.0f)
					valOut = valOut / sumWeights;
				ptrDepthOut[y*w + x] = valOut;
			}
		}
		depthOut = depthMapOut;
                
}

//
// weighted median filter
//
int weighted_average_filter(cv::Mat &dm)
{
    
    cv::Mat dm_orig = dm.clone(); 
    
    uint F_SIZE = 11;                               // dimension of the Laplace-like kernel
    
    uint w = dm.cols;
    uint h = dm.rows;
    
    for (uint i = 0 + 3; i < h - 3; i++)
        for (uint j = 0 + 3; j < w - 3; j++){
            
            if (dm_orig.at<float>(i, j) < 0.01f){ 
                
                double sum = 0.0f; 
                uint not_empty = 0; 
                
                for (uint k = 0; k < F_SIZE; k++)
                    for (uint l = 0; l < F_SIZE; l++){
                        uint i_subindex = i - 3 + k;
                        uint j_subindex = j - 3 + l;
                        
                        if (dm_orig.at<float>(i_subindex, j_subindex) > 0.01f){
                           sum = sum + dm_orig.at<float>(i_subindex, j_subindex); 
                           not_empty++; 
                        }       
                }
                
              // ### this can be improved later, but works well for now 
              if (not_empty > 2){
                dm.at<float>(i, j) = (float) ( sum / not_empty ); 
                }
            
            }
    
    }

    
   return 0; 
    
}

//
// median filter
//
int median_filter(cv::Mat &dm){ 
    
    
    auto sort_using_greater_than = [](double u, double v){ return u > v; };
    
    cv::Mat dm_orig = dm.clone(); 
    
    uint F_SIZE = 11;                               // dimension of the kernel
    
    uint w = dm.cols;
    uint h = dm.rows;
    
    for (uint i = 0 + 3; i < h - 3; i++)
        for (uint j = 0 + 3; j < w - 3; j++){
            
            vector<float> valid_values; 
            
            if (dm_orig.at<float>(i, j) < 0.01f){ 
                
                double sum = 0.0f; 
                uint not_empty = 0; 
                
                for (uint k = 0; k < F_SIZE; k++)
                    for (uint l = 0; l < F_SIZE; l++){
                        uint i_subindex = i - 3 + k;
                        uint j_subindex = j - 3 + l;
                        
                        if (dm_orig.at<float>(i_subindex, j_subindex) > 0.01f){
                            
                            valid_values.push_back( dm_orig.at<float>(i_subindex, j_subindex) ); 
                            
                           //sum = sum + dm.at<float>(i_subindex, j_subindex); 
                           //not_empty++; 
                        }       
                }
                
              // ### this can be improved later, but works well for now 
              if (valid_values.size() > 2){                                                           // (F_SIZE*F_SIZE) / 20 
                // ### sort vector of doubles
                std::sort(valid_values.begin(), valid_values.end(), sort_using_greater_than);  
                //dm.at<float>(i, j) = (float) ( sum / not_empty ); 
                
                uint index =  valid_values.size() / 2; 
               dm.at<float>(i, j) = valid_values[index];
               
                }
            }
    }
   
    return 0;
    
}

//
// ### END FILTERS
//


//
// ### other auxiliary functions
//



//
// returns the number of points in a segment defined by a mask
//
int mask_num_points(cv::Mat mask){
    
    int counter = 0; 
    
    for (int i = 0; i < mask.rows; i++)
        for (int j = 0; j < mask.cols; j++){
            if (mask.at<uchar>(i, j) == 255)
                counter++;
        }
        
    return counter; 
        
}


uint n_zero_values(cv::Mat dm){
    
    uint zero_counter = 0; 
                
    for (uint i = 0; i < dm.rows; i++)
        for (uint j = 0; j < dm.cols; j++){
            if ( dm.at<float>(i, j) < 0.01f ) 
                zero_counter++;
            }
    
    return zero_counter;
    
}




//
//  ### introduce number of points in every segment in the class
//
int extract_point_cloud(Eigen::MatrixXd &point_cloud, vector<cv::Mat> &segmentations_, uint s, cv::Mat vertexMap)
{   
    
    int width = segmentations_[0].cols; 
    int height = segmentations_[0].rows; 
    
    uint NUM_POINTS_ = 0; 
    for (int i = 0; i < height; i++)
      for (int j = 0; j < width; j++){
	if ( segmentations_[0].at<int>(i, j) == s )
	  NUM_POINTS_++; 
      }
    
    //uint NUM_POINTS_ = mask_num_points(mask); 
    
    
    //std::cout << NUM_POINTS_ << std::endl;
    //char f; std::cin >> f;
    
    
    point_cloud = Eigen::MatrixXd::Zero(NUM_POINTS_, 3); 
    
    // ### save the vertex map
    
    int counter = 0; 
    
    for (uint y = 0; y < vertexMap.rows; y++)
        for (uint x = 0; x < vertexMap.cols; x++){
            
            cv::Vec3f coordinate = vertexMap.at<cv::Vec3f>(y, x);
            
            if (segmentations_[0].at<int>(y, x) == s){
                point_cloud(counter, 0) = coordinate[0]; 
                point_cloud(counter, 1) = coordinate[1]; 
                point_cloud(counter, 2) = coordinate[2]; 
                
                counter++; 
            } 
        }   
        
        
        return NUM_POINTS_; 
        
}





//
// extracts a masked point cloud
//
int extract_point_cloud(Eigen::MatrixXd &point_cloud, cv::Mat mask, cv::Mat vertexMap)
{
        
    uint NUM_POINTS_ = mask_num_points(mask); 
    
    //std::cout << NUM_POINTS_ << std::endl;
    //char f; std::cin >> f;
    
    
    point_cloud = Eigen::MatrixXd::Zero(NUM_POINTS_, 3); 
    
    // ### save the vertex map
    
    int counter = 0; 
    
    for (uint y = 0; y < vertexMap.rows; y++)
        for (uint x = 0; x < vertexMap.cols; x++){
            
            cv::Vec3f coordinate = vertexMap.at<cv::Vec3f>(y, x);
            
            if (mask.at<uchar>(y, x) == 255){
                point_cloud(counter, 0) = coordinate[0]; 
                point_cloud(counter, 1) = coordinate[1]; 
                point_cloud(counter, 2) = coordinate[2]; 
                
                counter++; 
            }   
        }   
}


// 
// ### compute correspondences
// 
int compute_correspondences(vector<int> &corresp, Eigen::Matrix3d POSE_, Eigen::Vector3d translation, Eigen::Matrix3d intrinsics_, 
                            const cv::Mat &depthRef, const cv::Mat &depthCur){ 
    
    uint width = depthRef.cols; 
    uint height = depthRef.rows; 
    
    uint counter = 0; 
    
    corresp.reserve(width * height); 
    
    
    for (uint i = 0; i < height; i++)
        for (uint j = 0; j < width; j++){ 
        
        counter = i * width + j; 
        
        corresp[counter] = -1; 
    
        // camera intrinsics
        double fx = intrinsics_(0,0);
        double fy = intrinsics_(1,1);
        double cx = intrinsics_(0,2);
        double cy = intrinsics_(1,2);
        
        double dRef = depthRef.at<float>(i, j);
        double dCur = depthCur.at<float>(i, j); 
        
        // check if the depth values are positive
        if ( ( dRef > 0.0f ) && ( dCur > 0.0f ) )
        {
            
                    // project 2d point back into 3d using its depth
                    double x0 = (j - cx) / fx;
                    double y0 = (i - cy) / fy;
                
                    // transform reference 3d point into current frame
                    Eigen::Vector3d pt3Ref;
                    pt3Ref[0] = x0 * dRef;
                    pt3Ref[1] = y0 * dRef;
                    pt3Ref[2] = dRef;
                    
                    
                    Eigen::Vector3d pt3Cur = POSE_ * pt3Ref - translation; 
                    
                    
                    if (pt3Cur[2] > 0.0f)
                    {
                                // project 3d point to 2d  
                                // pinhole camera projection 
                                double px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
                                double py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                                
                        if (px >= 0.0f && py >= 0.0f && px <= (width-1) && py <= (height-1))
                        {
                            
                            // px and py are the coordinates, find the nearst depth value
                            uint corresp_point = (uint)py * width + (uint)px; 
                            corresp[counter] = corresp_point; 

                        }
                    } 
                    
                
        }
        
        
    }
    
    
    
    return 0; 
    
}



struct mesh{
    
    std::vector<cv::Vec3i> vertices_; 
    std::vector<cv::Vec3i> normals_; 
    std::vector<cv::Vec3b> colors_; 
    std::vector<cv::Vec3i> faceVertices_; 
    
public: 
    
    std::vector<cv::Vec3i>& vertices() { return vertices_;} 
    std::vector<cv::Vec3i>& normals()  { return normals_; }; 
    std::vector<cv::Vec3b>& colors()   { return colors_;}; 
    std::vector<cv::Vec3i>& faceVertices() {return faceVertices_;}; 
    
}; 






//
// compute a mesh from a point cloud
//

vector<int> convertVertexMapToMesh_(const cv::Mat &vertexMap, 
			     std::vector<cv::Vec3i> &mFaceIndices, 
			     Eigen::MatrixXd &Vertices) 
{
	
        double depthThreshold = 10.95;		// 5.0
        double edgeThreshold = 0.05;		// 0.05
        int w = vertexMap.cols;
        int h = vertexMap.rows;
	
	//for (int i = 0; i < w*h; i++)
	//  corresp_points[i] = -1; 
	
        vector<int> valid_vertices; 
	
	vector<int> lookup(w*h); 
	lookup.assign(w*h, -1); 
        
        for (int y = 0; y < h-1; ++y){
            for (int x = 0; x < w-1; ++x){
		
		
                cv::Vec3f vert = vertexMap.at<cv::Vec3f>(y, x); 
		
                // get points
		
                uint p00_index = y * w + x; 
                uint p10_index = y * w + x+1; 
                uint p01_index = (y+1) * w + x; 
                uint p11_index = (y+1) * w + x+1; 
                
                cv::Vec3f p00 = vertexMap.at<cv::Vec3f>(y, x);
                cv::Vec3f p10 = vertexMap.at<cv::Vec3f>(y, x+1);
                cv::Vec3f p01 = vertexMap.at<cv::Vec3f>(y+1, x);
                cv::Vec3f p11 = vertexMap.at<cv::Vec3f>(y+1, x+1);
                
                // check depth
                if (std::isnan(p00[2]) || std::isnan(p10[2]) || std::isnan(p01[2]) || std::isnan(p11[2]))
		{

                    continue;
		}
                
                if (p00[2] > depthThreshold || p10[2] > depthThreshold || p01[2] > depthThreshold || p11[2] > depthThreshold)
		{

                    continue;
		}
                
                if (p00[2] <= 0.01f || p10[2] <= 0.01f || p01[2] <= 0.01f || p11[2] <= 0.01f)
		{

                    continue;
		}
		     
                // TODO check occlusions: angle between vector (camera->p0) and vector (p0->p1) must be below threshold
		
                //check edge length
                if (norm(p00 - p01) > edgeThreshold || norm(p00 - p10) > edgeThreshold || norm(p10 - p01) > edgeThreshold ||
                       norm(p11 - p01) > edgeThreshold || norm(p11 - p10) > edgeThreshold)
		{
		  
                    continue; 
		}
                		
                
                if (lookup[p00_index] == -1){
		      valid_vertices.push_back(p00_index);
		      lookup[p00_index] = valid_vertices.size() - 1; 
		      
		    }
		    if (lookup[p01_index] == -1){
		      valid_vertices.push_back(p01_index);
		      lookup[p01_index] = valid_vertices.size() - 1; 
		    }
		    if (lookup[p11_index] == -1){
		      valid_vertices.push_back(p11_index);
		      lookup[p11_index] = valid_vertices.size() - 1; 
		    }
		    if (lookup[p10_index] == -1){
		      valid_vertices.push_back(p10_index);  
		      lookup[p10_index] = valid_vertices.size() - 1; 
		}
                
                
                // insert triangles (prefer smaller diagonal to form triangles)
                                
                cv::Vec3i face0;
                cv::Vec3i face1;
                if ( norm(p11-p00) < norm(p01-p10) )
                {
		    
		    //face0 = cv::Vec3i(p00_index, p01_index, p11_index);
                    //face1 = cv::Vec3i(p11_index, p10_index, p00_index);
		    
		    face0 = cv::Vec3i(lookup[p00_index], lookup[p01_index], lookup[p11_index]);
		    face1 = cv::Vec3i(lookup[p11_index], lookup[p10_index], lookup[p00_index]);
		      
                }
                else
                {
		  
                    //face0 = cv::Vec3i(p00_index, p01_index, p10_index);
                    //face1 = cv::Vec3i(p10_index, p01_index, p11_index);
		    
		    face0 = cv::Vec3i(lookup[p00_index], lookup[p01_index], lookup[p10_index]);
                    face1 = cv::Vec3i(lookup[p10_index], lookup[p01_index], lookup[p11_index]);
		    
                }
                
                
                mFaceIndices.push_back(face0);
                mFaceIndices.push_back(face1);
                
                
                
            }
        }
        
        
        cout << valid_vertices.size() << endl; 
        
        uint z_ = valid_vertices.size(); 
        
        Vertices = Eigen::MatrixXd(z_, 3); 
        
        for (int i = 0; i < z_; i++){
            
            uint r = trunc( valid_vertices[i] / w); 
            uint c = valid_vertices[i] % w; 
            
            cv::Vec3f vert_ = vertexMap.at<cv::Vec3f>(r, c); 
            
            Vertices(i, 0) = vert_[0]; 
            Vertices(i, 1) = vert_[1]; 
            Vertices(i, 2) = vert_[2]; 
            
            
        }
        
        
        return lookup; 
        
        //char f; cin >> f; 
}   







//
// compute a mesh from a point cloud
//

void convertVertexMapToMeshNonZeroes(const cv::Mat &vertexMap, std::vector<cv::Vec3i> &mFaceIndices, Eigen::MatrixXd &Vertices)
{
        
        double depthThreshold = 5.0;
        double edgeThreshold = 0.05;
        int w = vertexMap.cols;
        int h = vertexMap.rows;

        vector<int> valid_vertices; 
        
        uint corrective_index = 0; 
        
        
        for (int y = 0; y < h-1; ++y){
            for (int x = 0; x < w-1; ++x){
 
                //typedef Vec3f Eigen::Vector3f;
                
                cv::Vec3f vert = vertexMap.at<cv::Vec3f>(y, x); 
                
                
                
                
                if ( norm(vert) < 0.01f) {
                  
                  corrective_index++; 
                    
                    
                }
                else
                {
                    
                    
                // get points
                    
                uint p00_index = y * w + x - corrective_index; 
                uint p10_index = y * w + x+1 - corrective_index;
                uint p01_index = (y+1) * w + x - corrective_index; 
                uint p11_index = (y+1) * w + x+1 - corrective_index; 
                
                cv::Vec3f p00 = vertexMap.at<cv::Vec3f>(y, x);
                cv::Vec3f p10 = vertexMap.at<cv::Vec3f>(y, x+1);
                cv::Vec3f p01 = vertexMap.at<cv::Vec3f>(y+1, x);
                cv::Vec3f p11 = vertexMap.at<cv::Vec3f>(y+1, x+1);
                
                // check depth
                if (std::isnan(p00[2]) || std::isnan(p10[2]) || std::isnan(p01[2]) || std::isnan(p11[2])){
		    corrective_index++; 
                    continue;
		}
                
                if (p00[2] > depthThreshold || p10[2] > depthThreshold || p01[2] > depthThreshold || p11[2] > depthThreshold){
		    corrective_index++; 
                    continue;
		}
                
                if (p00[2] <= 0.01f || p10[2] <= 0.01f || p01[2] <= 0.01f || p11[2] <= 0.01f){
		    corrective_index++; 
                    continue;
		}
		    
		    
                // TODO check occlusions: angle between vector (camera->p0) and vector (p0->p1) must be below threshold

                //check edge length
                //if ((p00 - p01).norm() > edgeThreshold || (p00 - p10).norm() > edgeThreshold || (p10 - p01).norm() > edgeThreshold ||
                //       (p11 - p01).norm() > edgeThreshold || (p11 - p10).norm() > edgeThreshold)
                if (norm(p00 - p01) > edgeThreshold || norm(p00 - p10) > edgeThreshold || norm(p10 - p01) > edgeThreshold ||
                       norm(p11 - p01) > edgeThreshold || norm(p11 - p10) > edgeThreshold)
		{
		    corrective_index++; 
                    continue; 
		}
                
                
                valid_vertices.push_back(y * w + x); 
                
                // insert triangles (prefer smaller diagonal to form triangles)
                cv::Vec3i face0;
                cv::Vec3i face1;
                if ( norm(p11-p00) < norm(p01-p10) )
                {
                    face0 = cv::Vec3i(p00_index, p01_index, p11_index);
                    face1 = cv::Vec3i(p11_index, p10_index, p00_index);
                }
                else
                {
                    face0 = cv::Vec3i(p00_index, p01_index, p10_index);
                    face1 = cv::Vec3i(p10_index, p01_index, p11_index);
                }
                mFaceIndices.push_back(face0);
                mFaceIndices.push_back(face1);

		
                }
                
                
                
            }
        }
        
        
        
        cout << valid_vertices.size() << endl; 
        
        
        uint z_ = valid_vertices.size(); 
        
        Vertices = Eigen::MatrixXd(z_, 3); 
        
        for (int i = 0; i < z_; i++){
            
            uint r = trunc( valid_vertices[i] / w); 
            uint c = valid_vertices[i] % w; 
            
            cv::Vec3f vert_ = vertexMap.at<cv::Vec3f>(r, c); 
            
            Vertices(i, 0) = vert_[0]; 
            Vertices(i, 1) = vert_[1]; 
            Vertices(i, 2) = vert_[2]; 
            
            
            
        }
        
        //char f; cin >> f; 

}   





//
// compute a mesh from a point cloud
//

void convertVertexMapToMesh(const cv::Mat &vertexMap, std::vector<cv::Vec3i> &mFaceIndices)
{
    
        double depthThreshold = 5.0;
        double edgeThreshold = 0.05;
        int w = vertexMap.cols;
        int h = vertexMap.rows;

        for (int y = 0; y < h-1; ++y){
            for (int x = 0; x < w-1; ++x){
 
                //typedef Vec3f Eigen::Vector3f;

                // get points
                
                uint p00_index = y * w + x; 
                uint p10_index = y * w + x+1;
                uint p01_index = (y+1) * w + x; 
                uint p11_index = (y+1) * w + x+1; 
                
                cv::Vec3f p00 = vertexMap.at<cv::Vec3f>(y, x);
                cv::Vec3f p10 = vertexMap.at<cv::Vec3f>(y, x+1);
                cv::Vec3f p01 = vertexMap.at<cv::Vec3f>(y+1, x);
                cv::Vec3f p11 = vertexMap.at<cv::Vec3f>(y+1, x+1);
                
                // check depth
                if (std::isnan(p00[2]) || std::isnan(p10[2]) || std::isnan(p01[2]) || std::isnan(p11[2]))
                    continue;
                
                if (p00[2] > depthThreshold || p10[2] > depthThreshold || p01[2] > depthThreshold || p11[2] > depthThreshold)
                    continue;
                
                if (p00[2] <= 0.0f || p10[2] <= 0.0f || p01[2] <= 0.0f || p11[2] <= 0.0f)
                    continue;
                
                
                // TODO check occlusions: angle between vector (camera->p0) and vector (p0->p1) must be below threshold

                //check edge length
                //if ((p00 - p01).norm() > edgeThreshold || (p00 - p10).norm() > edgeThreshold || (p10 - p01).norm() > edgeThreshold ||
                //       (p11 - p01).norm() > edgeThreshold || (p11 - p10).norm() > edgeThreshold)
                if (norm(p00 - p01) > edgeThreshold || norm(p00 - p10) > edgeThreshold || norm(p10 - p01) > edgeThreshold ||
                       norm(p11 - p01) > edgeThreshold || norm(p11 - p10) > edgeThreshold)
                    continue;
                
                
                // insert triangles (prefer smaller diagonal to form triangles)
                cv::Vec3i face0;
                cv::Vec3i face1;
                if ( norm(p11-p00) < norm(p01-p10) )
                {
                    face0 = cv::Vec3i(p00_index, p01_index, p11_index);
                    face1 = cv::Vec3i(p11_index, p10_index, p00_index);
                }
                else
                {
                    face0 = cv::Vec3i(p00_index, p01_index, p10_index);
                    face1 = cv::Vec3i(p10_index, p01_index, p11_index);
                }
                mFaceIndices.push_back(face0);
                mFaceIndices.push_back(face1);
            }
        }

}   









///*

void convertRgbdToMesh(const cv::Mat &vertexMap, const cv::Mat &color, const cv::Mat &normals, mesh &mesh) //const
{
        double depthThreshold = 5.0;
        double normalThreshold = 0.0;
        double edgeThreshold = 0.05;

        int w = vertexMap.cols;
        int h = vertexMap.rows;

        const float* ptrVert = (const float*)vertexMap.data;

        std::vector<cv::Vec3i> &mVerts = mesh.vertices();
        std::vector<cv::Vec3i> &mNormals = mesh.normals();
        bool hasNormals = !mNormals.empty();
        std::vector<cv::Vec3b> &mColors = mesh.colors();
        bool hasColors = !color.empty();
        std::vector<cv::Vec3i> &mFaceIndices = mesh.faceVertices();

        for (int y = 0; y < h-1; ++y)
        {
            for (int x = 0; x < w-1; ++x)
            {
                int idx00 = 3*(y*w + x);
                int idx10 = 3*(y*w + x+1);
                int idx01 = 3*((y+1)*w + x);
                int idx11 = 3*((y+1)*w + x+1);

                // get points
                cv::Vec3f p00(ptrVert[idx00], ptrVert[idx00+1], ptrVert[idx00+2]);
                cv::Vec3f p10(ptrVert[idx10], ptrVert[idx10+1], ptrVert[idx10+2]);
                cv::Vec3f p01(ptrVert[idx01], ptrVert[idx01+1], ptrVert[idx01+2]);
                cv::Vec3f p11(ptrVert[idx11], ptrVert[idx11+1], ptrVert[idx11+2]);
                // check depth
                if (std::isnan(p00[2]) || std::isnan(p10[2]) || std::isnan(p01[2]) || std::isnan(p11[2]))
                    continue;
                if (p00[2] > depthThreshold || p10[2] > depthThreshold || p01[2] > depthThreshold || p11[2] > depthThreshold)
                    continue;
                if (p00[2] <= 0.0f || p10[2] <= 0.0f || p01[2] <= 0.0f || p11[2] <= 0.0f)
                    continue;

                // TODO check occlusions: angle between vector (camera->p0) and vector (p0->p1) must be below threshold

                //check edge length
                //if ((p00 - p01).norm() > edgeThreshold || (p00 - p10).norm() > edgeThreshold || (p10 - p01).norm() > edgeThreshold ||
                //       (p11 - p01).norm() > edgeThreshold || (p11 - p10).norm() > edgeThreshold)
                if (norm(p00 - p01) > edgeThreshold || norm(p00 - p10) > edgeThreshold || norm(p10 - p01) > edgeThreshold ||
                       norm(p11 - p01) > edgeThreshold || norm(p11 - p10) > edgeThreshold)
                    continue;

                if (hasNormals)
                {
                    // normals
                    const float* ptrNormals = (float*)normals.data;
                    cv::Vec3f n00(ptrNormals[idx00], ptrNormals[idx00+1], ptrNormals[idx00+2]);
                    cv::Vec3f n10(ptrNormals[idx10], ptrNormals[idx10+1], ptrNormals[idx10+2]);
                    cv::Vec3f n01(ptrNormals[idx01], ptrNormals[idx01+1], ptrNormals[idx01+2]);
                    cv::Vec3f n11(ptrNormals[idx11], ptrNormals[idx11+1], ptrNormals[idx11+2]);
#if 1
                    // check normals
                    if (std::abs(n00[2]) < normalThreshold || std::abs(n10[2]) < normalThreshold ||
                            std::abs(n01[2]) < normalThreshold || std::abs(n11[2]) < normalThreshold)
                        continue;
#endif

                    // insert vertex normals
                    mNormals.push_back(n00);
                    mNormals.push_back(n10);
                    mNormals.push_back(n01);
                    mNormals.push_back(n11);
                }

                // insert vertices
                int v00 = mVerts.size();
                mVerts.push_back(p00);
                int v10 = mVerts.size();
                mVerts.push_back(p10);
                int v01 = mVerts.size();
                mVerts.push_back(p01);
                int v11 = mVerts.size();
                mVerts.push_back(p11);

                if (hasColors)
                {
                    // insert vertex colors
                    const unsigned char* ptrColor = (unsigned char*)color.data;
                    cv::Vec3b c00(ptrColor[idx00+2], ptrColor[idx00+1], ptrColor[idx00]);
                    mColors.push_back(c00);
                    cv::Vec3b c10(ptrColor[idx10+2], ptrColor[idx10+1], ptrColor[idx10]);
                    mColors.push_back(c10);
                    cv::Vec3b c01(ptrColor[idx01+2], ptrColor[idx01+1], ptrColor[idx01]);
                    mColors.push_back(c01);
                    cv::Vec3b c11(ptrColor[idx11+2], ptrColor[idx11+1], ptrColor[idx11]);
                    mColors.push_back(c11);
                }

                // insert triangles (prefer smaller diagonal to form triangles)
                cv::Vec3i face0;
                cv::Vec3i face1;
                if ( norm(p11-p00) < norm(p01-p10) )
                {
                    face0 = cv::Vec3i(v00, v01, v11);
                    face1 = cv::Vec3i(v11, v10, v00);
                }
                else
                {
                    face0 = cv::Vec3i(v00, v01, v10);
                    face1 = cv::Vec3i(v10, v01, v11);
                }
                mFaceIndices.push_back(face0);
                mFaceIndices.push_back(face1);
            }
        }

        // compute vertex normals
        //computeVertexNormals(mesh);

        // unify vertices
        //unifyVertices(mesh);
    }

//*/




int ply_mesh_write(const cv::Mat &vertexMap, std::vector<cv::Vec3i> mFaceIndices){
    
    

    
}




//
// ### compute mesh 
//


vector<int> compute_and_save_mesh(cv::Mat &depthRef, Eigen::Matrix3d K, string mesh_file_name){
  
	RgbdProcessing* rgbdProcX = new RgbdProcessing();
        cv::Mat vertexMapRefX; 
        rgbdProcX->computeVertexMap(K, depthRef, vertexMapRefX); 
        
        std::vector<cv::Vec3i> mFaceIndices; 
        //convertVertexMapToMesh(vertexMapRefX, mFaceIndices); 
        
        Eigen::MatrixXd Vertices; 
        //convertVertexMapToMeshNonZeroes(vertexMapRefX, mFaceIndices, Vertices); 
        vector<int> lookup = convertVertexMapToMesh_(vertexMapRefX, mFaceIndices, Vertices); 
	
        Eigen::MatrixXi FACES(mFaceIndices.size(), 3);
        
        for (uint f = 0; f < mFaceIndices.size(); f++){
            cv::Vec3i face_ = mFaceIndices[f];
            FACES(f, 0) = face_[0]; 
            FACES(f, 1) = face_[1]; 
            FACES(f, 2) = face_[2]; 
        }
        
        //cout << "FACES(mFaceIndices.size() - 1, 2): " << FACES(mFaceIndices.size() - 1, 2) << endl; 
        
        Eigen::MatrixXd VERT(vertexMapRefX.cols * vertexMapRefX.rows, 3); 
        
        uint index_ = 0; 
        for (uint r = 0; r < vertexMapRefX.rows; r++)
            for (uint c = 0; c < vertexMapRefX.cols; c++){
                index_ = r * vertexMapRefX.cols + c; 
                cv::Vec3f coordinate = vertexMapRefX.at<cv::Vec3f>(r, c);
                VERT(index_, 0) =  coordinate[0]; 
                VERT(index_, 1) =  coordinate[1]; 
                VERT(index_, 2) =  coordinate[2]; 
        }
        
        write_ply_file(mesh_file_name, Vertices, nullptr, {0, 255, 255}, 2, FACES); 
        //write_ply_file("MESH.ply", VERT, nullptr, {0, 255, 255}, 2, FACES); 
        cout << "MESH WRITTEN..." << endl; 
	
	return lookup; 
    
}









#endif
